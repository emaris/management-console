
// @ts-nocheck


// listens for the scripts to load and the UI to be ready. Shows a loading layer until that happens.
// how? 
// 1. registers an observer for new DOM node with one well-known selector.
// 2. then adds a class to the body that triggers a transition defined in index.html
// 3. then listen for the end of that transition to lift the loading layer and shows the UI beneath.
// but:
// 4. wait no longer than a given timeout (maxWaitTimeout), after which lift the loading layer anyway (typically to show a loading error).



const busyMessageEvent = 'spin-msg' // busy guard message update event.
const readyTimeout = 6000 // timeout for ready selector.
const readySelectors = '.ant-layout, .covering, .nosuchroute' // any marks UI as ready.

const loadingDiv = document.querySelector('#loading')
const messageDiv = document.querySelector('#message')


// READY STATE HANDLING -------------------------------------------------------------------------------------------------------------------

const loadingObserver = new MutationObserver(mutations => {

  if (mutations.map(({ addedNodes: [node] }) => node).some(node => node?.matches?.(readySelectors) || node?.querySelector?.(readySelectors))) {

    clearTimeout(maxWaitTimeout) // stop timeout, we're actually done.
    triggerExitTransition()

  }

})


// register loader for dom changes from body down recursively.
loadingObserver.observe(document.body, { childList: true, subtree: true })

// guard timeoutt 
const maxWaitTimeout = setTimeout(() => {

  triggerExitTransition()

}, readyTimeout)


const triggerExitTransition = () => {

  // free mem
  loadingObserver.disconnect()

  // wait a little bit before stepping in.
  setTimeout(() => {

    document.body.classList.add('ready') // add class that kicks off transition animation that we listen for below.

  }, 200)
}


//  MESSAGE UPDATE HANDLING -------------------------------------------------------------------------------------------------------------------------

// puts a loading message upfront while scripts load.
// also, initial loading sequence is hardcoded on a white background, so we keep this dark layer also after scripts are loaded.
// but then we need to listen for messages below and copy them on the layer, which we do here.

const updateMessage = ({ detail: msg }) => messageDiv.innerText = msg

// shows initial messsage until scripts are loaded and busy guard rendered.
updateMessage({
  detail: (navigator.language || navigator.userLanguage).startsWith('fr') ? 'Chargement du système...' : 'Loading system...'
})

// register for message changes from busy guard below.
addEventListener(busyMessageEvent, updateMessage)

// CLEANUP ----------------------------------------------------------------------------------------

loadingDiv.addEventListener('transitionend', function handler({ target }) {

  if (target !== loadingDiv) return

  loadingDiv.removeEventListener('transitionend', handler)

  loadingDiv.remove()    

  root.style.willChange = 'unset' // free gpu memory (gpu layer is no more needed)

  removeEventListener(busyMessageEvent, updateMessage)

})

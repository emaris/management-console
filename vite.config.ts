import react from '@vitejs/plugin-react'
import { readdirSync } from 'fs'
import { defineConfig } from 'vite'
import checker from 'vite-plugin-checker'
import antdViteImportPlugin from 'antd-vite-import-plugin';

const libs = `${__dirname}/src/lib/`
const proxy = { target: 'https://localhost:8443', changeOrigin: true, secure: false, headers: { "Connection": "keep-alive" } }
const wsproxy = { target: proxy.target, ws: true, changeOrigin: true, secure: false }

const autochunked = ['date-fns', 'react-quill', 'quill', 'xlsx', 'react-color']

export default defineConfig(({ mode }) => {

  const config = {

    base: "/legacymc",

    server: {

      port: 3000,

      proxy: {
        // '/config.json': proxy,
        '/domain': proxy,
        '/admin': proxy,
        '/testmail': proxy,
        '/oauth2': proxy,
        '/export': proxy,
        '/event': wsproxy
      },

    },

    resolve: {
      preserveSymlinks: true,

      alias: [
      
        ...readdirSync(libs).map(name => 
          
            ({ find: name, replacement: `${libs}/${name}` })),

        { find: /^#(.*)$/, replacement: `${__dirname}/src/$1` },

        // react-pdf depends on this node lib, which doewsn't appear to be required, so we exclude it. 
        // this can be explicitly polyfilled (crypto-browserify), but this brings along a larger chain of deps
        // which add to the bundle and complicate further the picture.
        { find: "crypto", replacement: "rollup-plugin-node-polyfills/polyfills/empty"}

      ]
    }

    ,

    build: {

      // addresses the mixed use of `require` and `import` in some antd3 modules.
      // vite seems to deal with require, but in mixed mode it needs this.
      commonjsOptions: {
        transformMixedEsModules: true
      },


      sourcemap: true,

      rollupOptions: {

        output: {

          manualChunks: id => {

            if (autochunked.some(c => id.includes(c)))
              return

            if (id.includes('nats.ws'))
              return 'natsws'

            if (id.includes('node_modules'))
              return 'vendor'

          }

        }
      }

    },

    plugins: [

      react(),

      checker({
        overlay: {
          initialIsOpen: false,
          position: 'br'
        },
        typescript: true,
        eslint: {
          lintCommand: 'eslint "src/**/*{.ts,.tsx}"'
        }
      })
    ]

    ,

    define: {
      global: 'window',
      "process.browser": true,
    }
  }

  if (mode === 'production') {
  
    // address multiple issues with some "star" antd3 imports that create incompatibility with vite.
    config.plugins.push(antdViteImportPlugin())

  }

  return config

})

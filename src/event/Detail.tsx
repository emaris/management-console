
import { Button } from "apprise-frontend/components/Button";
import { IdProperty } from "apprise-frontend/components/IdProperty";
import { NoSuchRoute } from "apprise-frontend/components/NoSuchRoute";
import { RouteGuard } from "apprise-frontend/components/RouteGuard";
import { SideList } from "apprise-frontend/components/SiderList";
import { Paragraph, Text } from "apprise-frontend/components/Typography";
import { useFormState } from "apprise-frontend/form/hooks";
import { iamPlural } from "apprise-frontend/iam/constants";
import { any, specialise } from "apprise-frontend/iam/model";
import { icns } from "apprise-frontend/icons";
import { PredefinedLabel } from 'apprise-frontend/model/constants';
import { useLocale } from "apprise-frontend/model/hooks";
import { LifecycleSummary } from "apprise-frontend/model/lifecycle";
import { PushGuard } from 'apprise-frontend/push/PushGuard';
import { Page } from "apprise-frontend/scaffold/Page";
import { Titlebar } from "apprise-frontend/scaffold/PageHeader";
import { Sidebar } from "apprise-frontend/scaffold/Sidebar";
import { Tab } from "apprise-frontend/scaffold/Tab";
import { Topbar } from "apprise-frontend/scaffold/Topbar";
import { TagList } from "apprise-frontend/tag/Label";
import { useUsers } from "apprise-frontend/user/api";
import { through } from "apprise-frontend/utils/common";
import { parentIn } from "apprise-frontend/utils/routes";
import { useLiveAssetGuard } from '#campaign/LiveGuard';
import { useDashboards } from 'emaris-frontend/dashboard/api';
import { useEvents } from 'emaris-frontend/event/api';
import { eventPlural, eventRoute, eventSingular, eventType } from 'emaris-frontend/event/constants';
import { EventLabel } from 'emaris-frontend/event/Label';
import { Event, EventDto } from 'emaris-frontend/event/model';
import { useEventPermissions } from 'emaris-frontend/event/Permissions';
import * as React from "react";
import { useTranslation } from "react-i18next";
import { useHistory, useLocation, useParams } from "react-router";
import { EventGeneralForm } from './GeneralForm';



export const EventDetail = () => {

    const { pathname } = useLocation()
    const { id } = useParams<{ id: string }>()

    const events = useEvents()

    const current = events.lookup(id)

    if (!current)
        return <NoSuchRoute backTo={parentIn(pathname)} />

    return <PushGuard>
        <InnerEventDetail key={current.id} isNew={false} detail={current} />
    </PushGuard>

 
}



export const NewEventDetail = () => {
    const events = useEvents()

    const detail = events.next()

    return <InnerEventDetail isNew={true} detail={detail.model} />

}

const InnerEventDetail = (props: { isNew: boolean, detail: Event }) => {

    const { l } = useLocale()
    const { t } = useTranslation()

    const history = useHistory()
    const { pathname, search } = useLocation()

    const { isNew, detail } = props

    const singular = t(eventSingular)

    const events = useEvents()
    const { actions: { manage } } = events

    const { logged } = useUsers()

    const [Permissions, showPermissions] = useEventPermissions()

    const formstate = useFormState<Event>(detail);

    const { edited, initial, reset, dirty } = formstate


    const dashboard = useDashboards()

    const { liveGuarded, liveGuard, usage } = useLiveAssetGuard({
        id: edited.id,
        disabled: dirty,
        link: c => `${dashboard.given(c).routeToCampaign()}/${eventType}`,
        singular
    })



    const name = l(edited.name)

    const plural = t(eventPlural).toLowerCase()

    const manageIt = specialise(events.actions.manage, detail.id);


    const isProtected = edited.predefined || edited.guarded

    // -------------- effetcs

    React.useEffect(() => { if (isProtected) history.push(`${pathname}`) },
        //eslint-disable-next-line
        [isProtected]
    )

    // -------------- error reporting

    const report = { profile: events.validateEvent(edited, initial) }

    const totalErrors = Object.keys(report).reduce((total, key) => total += report[key].errors(), 0)

    // -------------- actions

    const onSave = (event: EventDto) => events.save(event)
        // reset to saved as initial state (or will appear dirty)
        .then(through(saved => reset(saved, false)))
        .then(saved => { events.resetNext(); return saved })
        .then(saved => history.push(`${events.routeTo(saved)}${search}`))

    const onRemove = () => events.remove(detail.id, () => {

        history.push(parentIn(pathname))

    }, usage.isInUse(edited.id))

    const onClone = () => {
        events.setNext({ model: events.clone(edited) })
        history.push(`${parentIn(pathname)}/new`)
    }

    const onAddEvent = () => Promise.resolve(events.resetNext())

    // -------------- action buttons

    const removeBtn = <Button
        icn={icns.remove}
        enabled={logged.can(manageIt) && edited.lifecycle.state === 'inactive'}
        disabled={dirty || isNew || edited.predefined}
        onClick={onRemove}>
        {t("common.buttons.remove")}
    </Button>

    const saveBtn = <Button
        icn={icns.save}
        enabled={dirty}
        disabled={totalErrors > 0}
        dot={totalErrors > 0}
        type="primary"
        onClick={() => onSave(edited)}>
        {t("common.buttons.save")}
    </Button>

    const cloneBtn = <Button
        enabled={logged.can(manage)}
        disabled={isNew || totalErrors > 0}
        icn={icns.clone}
        onClick={onClone}>
        {t("common.buttons.clone")}
    </Button>

    const rightsBtn = <Button
        icn={icns.permissions}
        disabled={isNew || dirty}
        enabledOnReadOnly
        onClick={showPermissions}>
        {t(iamPlural)}
    </Button>


    const revertBtn = <Button
        icn={icns.revert}
        enabled={dirty}
        type="danger"
        onClick={() => reset()}>
        {t("common.buttons.revert")}
    </Button>

    const addEventBtn = <Button
        type="primary"
        icn={icns.add}
        enabledOnReadOnly
        enabled={logged.can(manage)}
        onClick={onAddEvent}
        linkTo={`${eventRoute}/new`} >
        {t("common.buttons.add_one", { singular })}
    </Button>


    const readonly = liveGuarded || !logged.can(manageIt)

    return <Page readOnly={readonly}>

        <Sidebar>
            {saveBtn}
            {revertBtn}
            {cloneBtn}
            {removeBtn}
            {rightsBtn}

            <br />

            <IdProperty id={edited.id} />

            <br />

            <LifecycleSummary {...edited.lifecycle} />

            <br />

            <div className="sidebar-property">
                <Paragraph>
                    {totalErrors > 0 ?

                        <Text type="danger">{t("common.validation.total_error_count", { totalErrors })}</Text>
                        :
                        <Text smaller className="emphasis">{t("common.validation.zero_error_count")}</Text>

                    }
                </Paragraph>
            </div>

            {(logged.can(manage) && !isNew) &&
                <>
                    <div style={{ marginTop: "25px" }}>{addEventBtn}</div>
                </>
            }
            <SideList data={events.all()}
                filterGroup={eventType} filterBy={events.stringify} filterPlaceholder={t("common.components.table.filter_placeholder", { plural })}
                renderData={e => l(e.name)}
                render={e => <EventLabel selected={e.id === edited.id} event={e} />} />


        </Sidebar>

        <Topbar>

            <Titlebar title={name || (name === undefined ? "<new>" : "")}>
                {edited.predefined && <PredefinedLabel />}
                <TagList taglist={edited.tags} />
            </Titlebar>

            {liveGuard}

            {saveBtn}
            {revertBtn}
            {cloneBtn}
            {removeBtn}
            {rightsBtn}

            <Tab default id="general" icon={icns.form} name={t("common.labels.general")} badge={report.profile.errors() > 0} />

        </Topbar>

        <Permissions resourceCentric resourceRange={[edited]} filter={{ resources: [edited.id, any] }} />

        <EventGeneralForm {...formstate} report={report.profile} />

        <RouteGuard when={dirty} onOk={() => events.resetNext()} />

    </Page>

}
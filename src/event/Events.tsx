import * as React from 'react'
import { Route, Switch } from "react-router"
import { EventDetail, NewEventDetail } from './Detail'
import { EventList } from './List'
import { Placeholder } from 'apprise-frontend/components/Placeholder'
import { EventLoader } from 'emaris-frontend/event/Loader'
import { eventRoute } from 'emaris-frontend/event/constants'

export const Events = () => <EventLoader placeholder={Placeholder.page}>
            
            <Switch>
                        
                    <Route exact path={eventRoute} component={EventList} />
                    <Route path={`${eventRoute}/new`} component={NewEventDetail} />
                    <Route path={`${eventRoute}/:id`} component={EventDetail} />
                        
            </Switch>
            </EventLoader>

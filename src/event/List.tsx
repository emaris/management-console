import { buildinfo } from 'apprise-frontend/buildinfo'
import { Button } from 'apprise-frontend/components/Button'
import { ListState, useListState } from 'apprise-frontend/components/hooks'
import { Column, VirtualTable } from 'apprise-frontend/components/VirtualTable'
import { iamPlural } from 'apprise-frontend/iam/constants'
import { specialise } from 'apprise-frontend/iam/model'
import { icns } from 'apprise-frontend/icons'
import { useLocale } from 'apprise-frontend/model/hooks'
import { Page } from 'apprise-frontend/scaffold/Page'
import { Titlebar } from 'apprise-frontend/scaffold/PageHeader'
import { Sidebar } from 'apprise-frontend/scaffold/Sidebar'
import { Topbar } from 'apprise-frontend/scaffold/Topbar'
import { useTags } from 'apprise-frontend/tag/api'
import { TagList } from 'apprise-frontend/tag/Label'
import { useUsers } from 'apprise-frontend/user/api'
import { useBaseFilter } from 'apprise-frontend/utils/filter'
import { useCampaignUsage } from '#campaign/Usage'
import { useEvents } from 'emaris-frontend/event/api'
import { eventPlural, eventSingular, eventType } from 'emaris-frontend/event/constants'
import { EventLabel, EventTypeLabel } from 'emaris-frontend/event/Label'
import { Event } from 'emaris-frontend/event/model'
import { useEventPermissions } from 'emaris-frontend/event/Permissions'
import { useTranslation } from 'react-i18next'
import { useHistory, useLocation } from 'react-router'

export const eventGroup = eventType

export const EventList = () => {

    const { t } = useTranslation()
    const { l } = useLocale()

    const history = useHistory()
    const { pathname } = useLocation()

    const [singular, plural] = [t(eventSingular), t(eventPlural)]

    const liststate: ListState<Event> = useListState<Event>()

    const events = useEvents()
    const users = useUsers()
    const tags = useTags()

    const { actions: { manage } } = events

    const { logged } = users

    const [Permissions, showPermissions] = useEventPermissions()
    
    const usage = useCampaignUsage()


    // -------------- authz privileges
     const canManage = (r: Event) => logged.can(specialise(events.actions.manage, r.id))
 
    // -------------- actions
    const onRemove = ({id}:Event) => events.remove(
        id,
        () => { 
            
            liststate.setSelected(liststate.selected.filter(l => l.id !== id))
        },
        usage.isInUse(id)
    )

    const onClone = (event: Event) => {
        events.setNext({model: events.clone(event) })
        history.push(`${pathname}/new`)
    }

    const onAddEvent = () => Promise.resolve(events.resetNext())


    // -------------- buttons

    const addBtn =  <Button 
                        type="primary" 
                        icn={icns.add} 
                        enabled={logged.can(manage)} 
                        onClick={onAddEvent}
                        linkTo={`${pathname}/new`}>
                            {t("common.buttons.add_one", { singular })}
                    </Button>

    const openBtn = (event: Event) =>   <Button 
                                            key={1} 
                                            icn={icns.open} 
                                            linkTo={events.routeTo(event)}>
                                                {t("common.buttons.open")}
                                        </Button>

    const cloneBtn = (event:Event)  =>  
        <Button 
            key={3} 
            icn={icns.clone} 
            onClick={()=>onClone(event)}>
                {t("common.buttons.clone")}
        </Button>
    
    const removeBtn = (event: Event) =>
        <Button
            key={2}
            icn={icns.remove}
            enabled={logged.can(manage) && event.lifecycle.state==='inactive'}
            disabled={event.predefined || event.guarded}
            onClick={() => onRemove(event)
            } >{t("common.buttons.remove")}
        </Button>

    const removeAllBtn =    <Button 
                                key={3} 
                                type="danger" 
                                disabled={liststate.selected.length < 1}
                                onClick={() => events.removeAll(liststate.selected.map(l => l.id), () => liststate.setSelected([]))} >
                                    <span style={{fontVariantCaps:"all-small-caps"}}>(DEV) </span>{t("common.labels.remove_all", { count: liststate.selected.length })}
                            </Button>

    const rightsBtn =   <Button 
                            enabled={liststate.selected.length > 0} 
                            icn={icns.permissions} 
                            style={{ marginTop: 30 }} 
                            onClick={showPermissions}>
                                {t(iamPlural)}
                        </Button>

    
    const unfilteredData = events.all() 

    const {BaseFilter,baseFilteredData} = useBaseFilter({
        data: unfilteredData,
        readonlyUnless: canManage,
        group: eventGroup
    })

    const data = baseFilteredData

    return <Page>

        <Sidebar>
            {addBtn}
            {rightsBtn}

            {buildinfo.development &&
                <>
                    <br />
                    {removeAllBtn}
                </>
            }
        </Sidebar>

        <Topbar>
            <Titlebar title={ plural } />

            {addBtn}
            {rightsBtn}

        </Topbar>
        <VirtualTable data={data} total={unfilteredData.length} rowKey="id" state={liststate} selectable={buildinfo.development}
           
            filterGroup={eventGroup} filterBy={events.stringify} filterPlaceholder={t("common.components.table.filter_placeholder", { plural: plural.toLowerCase() })}
            
            sortBy={[['type', 'asc'], ['name', 'asc']]}
            actions={event => [openBtn(event), removeBtn(event), cloneBtn(event)]}
            
            filters={[BaseFilter]}
            onDoubleClick={e => history.push(events.routeTo(e))} >

            <Column<Event> flexGrow={1} title={t("common.fields.name_multi.name")} dataKey="name" dataGetter={e => l(e.name)}
                cellRenderer={({ rowData: e }) => <EventLabel event={e} readonly={!canManage(e)} />} />

            <Column<Event> flexGrow={1} width={150} title={t("common.fields.type.name")} dataKey="type" dataGetter={e => e.type} cellRenderer={cell => <EventTypeLabel type={cell.rowData?.type} />} />

            {tags.allTagsOf(eventType).length > 0 &&
                <Column sortable={false} flexGrow={2} title={t("common.fields.tags.name")} dataKey="tags" dataGetter={(e: Event) => <TagList taglist={e.tags} />} />
            }

        </VirtualTable>

        <Permissions resourceRange={liststate.selected} />

    </Page>

}

import { NoteBox } from 'apprise-frontend/form/NoteBox'
import { SliderBox } from 'apprise-frontend/form/SliderBox'
import { useTags } from 'apprise-frontend/tag/api'
import { TagRefBox } from 'apprise-frontend/tag/TagRefBox'
import { useUsers } from 'apprise-frontend/user/api'
import * as React from 'react'
import { useTranslation } from 'react-i18next'
import { Form } from 'apprise-frontend/form/Form'
import { FormState } from 'apprise-frontend/form/hooks'
import { MultiBox } from 'apprise-frontend/form/MultiBox'
import { SelectBox } from 'apprise-frontend/form/SelectBox'
import { Switch } from 'apprise-frontend/form/Switch'
import { Module } from 'apprise-frontend/module'
import { TagBoxset } from 'apprise-frontend/tag/TagBoxset'
import { EventValidation } from 'emaris-frontend/event/validation'
import { useEvents } from 'emaris-frontend/event/api'
import { EventTypeLabel } from 'emaris-frontend/event/Label'
import { Event } from 'emaris-frontend/event/model'
import { eventType } from 'emaris-frontend/event/constants'


type Props = FormState<Event> & {

    report: ReturnType<EventValidation['validateEvent']>
}


export const EventGeneralForm = (props: Props) => {

    const { t } = useTranslation()

    const { edited, change, initial, report } = props;

    const {logged} = useUsers()
    const { registeredModules } = useEvents()
    const tags = useTags()

    const regModules = registeredModules()


    const tagCategories = edited.type ? tags.allCategoriesOf(edited.type) : []

    const isProtected = edited.guarded

    const maxCardinality = 11

    const cardinalityValue = edited.managed ? 
                                [edited.cardinality.initial, edited.cardinality.max || maxCardinality] as [number, number] 
                                : 
                                edited.cardinality.max || maxCardinality

    const cardinalityMarkers = {
        1 : "1",
        2 : "2",
        3 : "3",
        4 : "4",
        5 : "5",
        6 : "6",
        7 : "7",
        8 : "8",
        9 : "9",
        10 : "10",
        11 : t("common.labels.any"),
    }

    const cardinalityChange = (value:[number,number]) => {
            change((t, v) => {t.cardinality = {initial:v[0], max: v===maxCardinality ? undefined : v[1]}})(value)
    }

    return <Form state={props} sidebar>

        <Switch disabled={isProtected} label={t("common.fields.active.name")} onChange={change((u, v) => u.lifecycle.state = v ? "active" : "inactive")} validation={report.active}>
            {edited.lifecycle.state === 'active'}
        </Switch>

        <SelectBox label={t("common.fields.type.name")} validation={report.type} onChange={change((t, v) => t.type = v)}
            disabled={!!edited.id} renderDisabled={() => <EventTypeLabel light type={edited.type} />}
            getkey={(c: Module) => c.type}
            getlbl={(c: Module) => <EventTypeLabel light type={c.type} />}
            selectedKey={edited.type}>
            {regModules}
        </SelectBox>

        <MultiBox id="event-name" label={t("common.fields.name_multi.name")} validation={report.name} onChange={change((t, v) => t.name = v)}>
            {edited.name}
        </MultiBox>

        <MultiBox id="event-desc" autoSize label={t("common.fields.description_multi.name")} validation={report.description} onChange={change((t, v) => t.description = v)}>
            {edited.description}
        </MultiBox>

        <Switch disabled={isProtected} label={t("event.fields.relatable.name")} onChange={
                change((u, v) => u.properties.relatable = v ? v : initial.properties?.relatable)
            }
            validation={report.relatable}>
            {edited.properties.relatable}
        </Switch>


        <Switch disabled={isProtected} label={t("event.fields.managed.name")} onChange={
                change((u, v) => {u.managed = v; u.cardinality = {initial: 1, max: 1}})
            }
            validation={report.managed}>
            {edited.managed}
        </Switch>

        {edited.type === eventType &&

            <SelectBox label={t("event.fields.managed_type.name")} validation={report.managedTypes} onChange={change((t, v) => t.managedTypes = v)}
                getkey={(c: Module) => c.type}
                getlbl={(c: Module) => <EventTypeLabel light type={c.type} />}
                selectedKey={edited.managedTypes || []}>
                {regModules.filter(m => m.type !== eventType)}
            </SelectBox>

        }

        <SliderBox 
            defaultValue={cardinalityValue} 
            disabled={isProtected || edited.predefinedProperties.includes('cardinality')}
            range={edited.managed} 
            label={t("event.fields.cardinality.name")} 
            validation={report.cardinality} 
            min={1} 
            max={maxCardinality}
            tooltipVisible={false}
            marks={cardinalityMarkers}
            showValues={false}
            onChange={value=>
                edited.managed ? 
                    cardinalityChange(value as [number, number])
                    :
                    cardinalityChange([1, value as number])
            } 
        />

        {edited.type && tagCategories.length > 0 &&
            <TagRefBox disabled={!edited.managed || isProtected} mode="multi" label={t("event.fields.managed_expr.name")} validation={report.managedExpression} expression={edited.managedExpression} type={edited.type} onChange={change((t, v) => t.managedExpression = v)} />
        }

        <TagBoxset edited={edited.tags} type={eventType} disabled={isProtected} validation={report} onChange={change((t, v) => t.tags = v)} />


        <NoteBox label={t("common.fields.note.name")} validation={report.note} autoSize={{ minRows: 4, maxRows: 6 }} onChange={change((t, v) => t.properties.note = t.properties.note ? { ...t.properties.note, [logged.tenant]: v } : { [logged.tenant]: v })} >
            {edited.properties.note}
        </NoteBox>

    </Form>

}

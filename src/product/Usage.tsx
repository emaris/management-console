import { FormState } from "apprise-frontend/form/hooks"
import { useLocale } from "apprise-frontend/model/hooks"
import { ContentHeader } from "apprise-frontend/scaffold/ContentHeader"
import { campaignIcon } from "emaris-frontend/campaign/constants"
import { campaignmodule } from "emaris-frontend/campaign/module"
import { useCampaignUsage } from "emaris-frontend/campaign/UsageList"
import { useProducts } from "emaris-frontend/product/api"
import { productType } from "emaris-frontend/product/constants"
import { Product } from "emaris-frontend/product/model"
import { requirementIcon, requirementPlural, requirementSingular } from "emaris-frontend/requirement/constants"
import * as React from "react"
import { useTranslation } from "react-i18next"
import { ProductRequirementList } from "./RequirementList"

export type Props = FormState<Product>

export const ProductUsage = (props:Props) => {

    const {t} = useTranslation()
    const {l} = useLocale()

    const products = useProducts()
   

    const {edited} = props

    const [campaignUsageList,campaignList] = useCampaignUsage({type:productType,target:edited.id,height:250})

    const name = l(edited.name)
    const reqcount =  products.requirementsFor(edited).length  //edited.requirements?.length ?? 0
    const requirements = t( reqcount!==1 ? requirementPlural: requirementSingular)
    const campcount = campaignList?.length ?? -1
    const campaigns = t( campcount!==1 ? campaignmodule.namePlural: campaignmodule.nameSingular)

    return <>

            <ContentHeader  title={<span>{requirementIcon}&nbsp;&nbsp; <span>{reqcount}</span> {requirements}</span>}
                    subtitle={t("product.feedback.based_on",{name,reqcount,requirements:requirements.toLowerCase()})}/>
            
            <ProductRequirementList height={250} product={edited}  />
           
            <ContentHeader  title={<span>{campaignIcon}&nbsp;&nbsp; <span>{campcount===-1? '...' : campcount}</span> {campaigns}</span>} 
                    subtitle={t("campaign.feedback.used_in",{name,campcount:campcount===-1? '...' : campcount})} />

            {campaignUsageList}


          </>
}
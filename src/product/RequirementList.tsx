
import { Column, VirtualTable } from "apprise-frontend/components/VirtualTable";
import { icns } from "apprise-frontend/icons";
import { useLocale } from "apprise-frontend/model/hooks";
import { TagList } from "apprise-frontend/tag/Label";
import { ReportMethods } from "apprise-frontend/utils/validation";
import { useProducts } from 'emaris-frontend/product/api';
import { Product } from 'emaris-frontend/product/model';
import { useRequirements } from 'emaris-frontend/requirement/api';
import { requirementRoute } from "emaris-frontend/requirement/constants";
import { RequirementLabel } from "emaris-frontend/requirement/Label";
import { Requirement } from "emaris-frontend/requirement/model";
import * as React from "react";
import { useTranslation } from "react-i18next";
import { useHistory } from "react-router";


type Props = {

    product: Product
    report?: ReportMethods
    height?: number
}


export const ProductRequirementList = (props: Props) => {

    const history = useHistory()
    const { t } = useTranslation()
    const { l } = useLocale()

    const { height, product, report = {} } = props

    const invalid = ({ rowData: r }) => report[r.id]?.status === "error" && icns.error(report[r.id].msg)

    const products = useProducts()
   
    const assets : (Product | Requirement)[] = [...useProducts().all(), ...useRequirements().all()]
    
    // eslint-disable-next-line
    const data = React.useMemo(() => products.dependenciesOf(product).map( ({asset}) => assets.find( a => a.id === asset)! ).filter( asset => !!asset), [product])

    return <VirtualTable<Requirement | Product> height={height} selectable={false} filtered={data.length > 10}
        data={data} rowKey="id"
        onDoubleClick={t => history.push(`${requirementRoute}/${t.id}`)} >

        <Column title={t("common.fields.name_multi.name")} dataKey="name" decorations={[invalid]}
            dataGetter={(r: Requirement) => l(r.name)} cellRenderer={r => <RequirementLabel lineage requirement={r.rowData} />} />

        <Column flexGrow={1} title={t("common.fields.tags.name")} dataKey="tags" dataGetter={(r: Requirement) => <TagList taglist={r.tags} />} />

    </VirtualTable>

}
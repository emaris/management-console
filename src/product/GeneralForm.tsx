import { NoteBox } from 'apprise-frontend/form/NoteBox'
import { useLocale } from 'apprise-frontend/model/hooks'
import { useSystem } from 'apprise-frontend/system/api'
import { useTags } from 'apprise-frontend/tag/api'
import { TagRefBox } from 'apprise-frontend/tag/TagRefBox'
import { AudienceList } from 'apprise-frontend/tenant/AudienceList'
import { tenantType } from 'apprise-frontend/tenant/constants'
import { useUsers } from 'apprise-frontend/user/api'
import { userType } from 'apprise-frontend/user/constants'
import { useProducts } from 'emaris-frontend/product/api'
import { ProductLabel } from 'emaris-frontend/product/Label'
import { Form } from 'apprise-frontend/form/Form'
import { FormState } from 'apprise-frontend/form/hooks'
import { MultiBox } from 'apprise-frontend/form/MultiBox'
import { Switch } from 'apprise-frontend/form/Switch'
import { contextCategory, systemType } from 'apprise-frontend/system/constants'
import { ContextAwareSelectBox } from 'apprise-frontend/system/ContextAwareSelectBox'
import { TagBoxset } from 'apprise-frontend/tag/TagBoxset'
import { Product } from 'emaris-frontend/product/model'
import { ProductValidation } from 'emaris-frontend/product/validation'
import { partition } from "lodash"
import * as React from 'react'
import { useTranslation } from 'react-i18next'
import { productType } from '../lib/emaris-frontend/product/constants'



type Props = FormState<Product> & {

    report: ReturnType<ProductValidation['validateProduct']>
}
    


export const ProductGeneralForm = (props:Props) =>  {
    
    const {t} = useTranslation()
    const {l} = useLocale()

    const {edited,change,report} = props;

    const {lookupTag,lookupCategory} = useTags()
    const {logged} = useUsers()
    const { contextOf } = useSystem()

    const products = useProducts()

    const [facets,other] = partition(edited.tags,t=>lookupTag(t).type===systemType)

    const context = lookupCategory(contextCategory)

    const otherProducts = products.all().filter(r => r.id !== edited.id)

    return <Form state={props} sidebar> 

            <Switch label={t("common.fields.active.name")}  onChange={change((u,v) => u.lifecycle.state=v?"active":"inactive")} validation={report.active}>
                    {edited.lifecycle.state==='active'}
            </Switch>

            <MultiBox id="product-name" label={t("common.fields.name_multi.name")} validation={report.name} onChange={change( (t,v)=> t.name=v) }>
                {edited.name}
            </MultiBox>

            <MultiBox id="product-title" label={t("common.fields.title.name")} validation={report.title} onChange={change( (t,v)=> t.description=v) }>
                {edited.description}
            </MultiBox>  

            <ContextAwareSelectBox mode="multiple" label={t("common.fields.lineage.name")} validation={report.lineage}
                currentContext={contextOf(edited)}
                 
                options={otherProducts}
                onChange={change((t, vs) => t.lineage = vs ? vs.map(v => v.id) : vs)}
                renderOption={p => <ProductLabel noLink product={p} />}
                lblTxt={ p => l(p.name) }
                optionId={p => p.id}>

                {products.all().filter(p => edited.lineage?.includes(p.id))}

            </ContextAwareSelectBox>

            <TagRefBox mode="multi" label={t("common.fields.audience.name")}   validation={report.audience} expression={edited.audience} type={tenantType} onChange={ change( (t,v) => t.audience=v)} />

            <AudienceList label={t("common.fields.audience_list.name")}  validation={report.audienceList} onChange={change((t,v) => t.audienceList = v)}>{edited.audienceList}</AudienceList>

            <TagRefBox mode="multi" label={t("common.fields.user_profile.name")} validation={report.userProfile} expression={edited.userProfile} type={userType} onChange={change((t, v) => t.userProfile = v)} />

           <TagBoxset edited={other}  type={productType} validation={report} onChange={change( (t,v) =>t.tags=[...facets,...v])}  />

            <TagBoxset edited={facets}  categories={[context]} validation={report} onChange={change( (t,v) =>t.tags=[...v,...other])}  />

            <Switch label={t("common.fields.editable.name")}  onChange={change((u,v) => u.properties.editable=v)} validation={report.editable}>
                {edited.properties.editable !== undefined ? edited.properties.editable : true}
            </Switch>

            <Switch label={t("common.fields.versionable.name")}  onChange={change((u,v) => u.properties.versionable=v)} validation={report.versionable}>
                {edited.properties.versionable !== undefined ? edited.properties.versionable : true}
            </Switch>

            <Switch label={t("common.fields.assessed.name")}  onChange={change((u,v) => u.properties.assessed=v)} validation={report.assessed}>
                {edited.properties.assessed !== undefined ? edited.properties.assessed : true}
            </Switch>
            
            <NoteBox label={t("common.fields.note.name")} validation={report.note} autoSize={{minRows:4,maxRows:6}} onChange={change( (t,v)=> t.properties.note = t.properties.note ? {...t.properties.note, [logged.tenant]: v} : {[logged.tenant]: v} ) } >
                {edited.properties.note} 
            </NoteBox> 

    </Form>

}
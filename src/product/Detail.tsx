import { Button } from "apprise-frontend/components/Button";
import { IdProperty } from "apprise-frontend/components/IdProperty";
import { NoSuchRoute } from "apprise-frontend/components/NoSuchRoute";
import { RouteGuard } from "apprise-frontend/components/RouteGuard";
import { SideList } from "apprise-frontend/components/SiderList";
import { Paragraph, Text } from "apprise-frontend/components/Typography";
import { useFormState } from "apprise-frontend/form/hooks";
import { iamPlural } from "apprise-frontend/iam/constants";
import { any, specialise } from "apprise-frontend/iam/model";
import { icns } from "apprise-frontend/icons";
import { LayoutDesigner } from "apprise-frontend/layout/Designer";
import { Layout, LayoutConfig } from "apprise-frontend/layout/model";
import { PredefinedLabel } from 'apprise-frontend/model/constants';
import { useLocale } from "apprise-frontend/model/hooks";
import { LifecycleSummary } from "apprise-frontend/model/lifecycle";
import { PushGuard } from 'apprise-frontend/push/PushGuard';
import { Page } from "apprise-frontend/scaffold/Page";
import { Titlebar } from "apprise-frontend/scaffold/PageHeader";
import { Sidebar } from "apprise-frontend/scaffold/Sidebar";
import { Tab } from "apprise-frontend/scaffold/Tab";
import { Topbar } from "apprise-frontend/scaffold/Topbar";
import { BytestreamedContext, useBytestreamedHelper } from "apprise-frontend/stream/BytestreamedHelper";
import { DocumentLocalisedStream, localisedStreamsOf } from "apprise-frontend/stream/model";
import { useSystem } from "apprise-frontend/system/api";
import { TagList } from "apprise-frontend/tag/Label";
import { useUsers } from "apprise-frontend/user/api";
import { through } from "apprise-frontend/utils/common";
import { paramsInQuery, parentIn } from "apprise-frontend/utils/routes";
import { useLiveAssetGuard } from '#campaign/LiveGuard';
import { CampaignLoader } from "emaris-frontend/campaign/Loader";
import { useDashboards } from 'emaris-frontend/dashboard/api';
import { useProducts } from "emaris-frontend/product/api";
import { productPlural, productRoute, productSingular, productType } from "emaris-frontend/product/constants";
import { ProductDetailLoader } from 'emaris-frontend/product/DetailLoader';
import { ProductLabel } from "emaris-frontend/product/Label";
import { Product, ProductDto } from "emaris-frontend/product/model";
import { useProductPermissions } from "emaris-frontend/product/ProductPermissions";
import * as React from "react";
import { useTranslation } from "react-i18next";
import { useHistory, useLocation, useParams } from "react-router";
import { ProductGeneralForm } from "./GeneralForm";
import { productGroup } from './List';
import { ProductUsage } from "./Usage";

export const ProductDetail = () => {

    const { id } = useParams<{ id: string }>()
    const { pathname } = useLocation()
    const products = useProducts()

    const current = products.lookup(id)

    if (!current)
        return <NoSuchRoute backTo={parentIn(pathname)} />

    return <PushGuard>
        <ProductDetailLoader key={current.id} id={current.id}>
            {loaded =>

                <BytestreamedContext target={loaded.id}>
                    <InnerProductDetail isNew={false} detail={loaded} />
                </BytestreamedContext>
            }
        </ProductDetailLoader>
    </PushGuard >

}

export const NewProductDetail = () => {

    const products = useProducts()

    const detail = products.next()

    return <BytestreamedContext><InnerProductDetail isNew={true} detail={detail.model} /></BytestreamedContext>

}


const InnerProductDetail = (props: { isNew: boolean, detail: Product }) => {

    const { l } = useLocale()
    const { t } = useTranslation()
    const history = useHistory()
    const { pathname, search } = useLocation()



    const { contextOf } = useSystem()
    const { logged } = useUsers()
    const products = useProducts()

    const helper = useBytestreamedHelper()

    const singular = t(productSingular)

    const { isNew, detail } = props

    const { actions: { edit } } = products

    const [Permissions, showPermissions] = useProductPermissions()

    const formstate = useFormState<Product>(detail);

    const dashboard = useDashboards()

    const { edited, change, initial, reset, dirty } = formstate

    const { liveGuarded, liveGuard, usage } = useLiveAssetGuard({
        id: edited.id,
        disabled: dirty,
        link: c => dashboard.given(c).routeToAssetWith(productType, edited.id),
        singular
    })

    const { tab } = paramsInQuery(search)

    const name = l(edited.name)
    const plural = t(productPlural).toLowerCase()

    const canEditIt = logged.can(specialise(edit, detail.id))

    const isProtected = edited.predefined || liveGuarded

    // -------------- effetcs
    React.useEffect(() =>
        products.resetNext(),
        //eslint-disable-next-line
        [])


    // -------------- error reporting

    const report = { profile: products.validateProduct(edited) }

    const totalErrors = Object.keys(report).reduce((total, key) => total += report[key].errors(), 0)

    // -------------- actions

    const currentDocumentStreams = () => Object.values(edited.documents ?? {}).flatMap(ds => ds.flatMap(localisedStreamsOf))

    const updateDocumentStreams = (product: Product, localisedStreams: DocumentLocalisedStream[]) => {

        const docs = Object.values(product.documents ?? {}).flatMap(ds => ds).flatMap(d => d)

        localisedStreams.forEach(localisedStream => {

            const lang = localisedStream.lang
            const match = docs.find(d => `${d.id}-${lang}` === localisedStream.id)

            if (match)
                match[lang] = localisedStream.stream

        })

    }

    const onSave = (product: ProductDto) => helper.upload(...currentDocumentStreams())

        .then(through(streams => updateDocumentStreams(product, streams)))
        .then(() => products.save(product))
        // reset to saved as initial state (or will appear dirty)
        .then(through(saved => reset(saved, false)))
        .then(through(helper.reset))
        .then(saved => { products.resetNext(); return saved })
        .then(saved => history.push(`${products.routeTo(saved)}${search}`))

    const onRevert = () => reset(initial, helper.reset)

    const onRemove = () => products.remove(detail.id, () => {

        history.push(parentIn(pathname))

    }, usage.isInUse(edited.id))

    const onClone = () => {
        products.setNext({ model: products.clone(edited) })
        history.push(`${parentIn(pathname)}/new`)
    }

    const onBranch = () => {
        products.setNext({ model: products.branch({ ...edited, documents: {} }) })
        history.push(`${parentIn(pathname)}/new`)
    }

    const onAddProduct = () => Promise.resolve(products.resetNext())



    // -------------- action buttons

    const removeBtn = <Button
        icn={icns.remove}
        enabled={canEditIt && edited.lifecycle.state === 'inactive'}
        disabled={dirty || isNew || edited.predefined}
        onClick={onRemove}>
        {t("common.buttons.remove")}
    </Button>


    const saveBtn = <Button
        type="primary"
        icn={icns.save}
        enabled={dirty}
        disabled={totalErrors > 0}
        dot={totalErrors > 0}
        onClick={() => onSave(edited)}>
        {t("common.buttons.save")}
    </Button>

    const revertBtn = <Button
        icn={icns.revert}
        enabled={dirty}
        type="danger"
        onClick={onRevert}>
        {t("common.buttons.revert")}
    </Button>

    const cloneBtn = <Button
        enabled={logged.can(edit)}
        disabled={isNew || totalErrors > 0}
        icn={icns.clone}
        onClick={onClone}>
        {t("common.buttons.clone")}
    </Button>

    const branchBtn = <Button
        enabled={logged.can(edit)}
        disabled={isNew || totalErrors > 0}
        icn={icns.branch}
        onClick={onBranch}>
        {t("campaign.buttons.branch")}
    </Button>

    const rightsBtn = <Button
        icn={icns.permissions}
        enabledOnReadOnly
        disabled={isNew || dirty}
        onClick={showPermissions}>
        {t(iamPlural)}
    </Button>

    const addProductBtn = <Button
        type="primary"
        icn={icns.add}
        enabled={logged.can(edit)}
        enabledOnReadOnly
        onClick={onAddProduct}
        linkTo={`${productRoute}/new`} >
        {t("common.buttons.add_one", { singular })}
    </Button>

    const layout: Layout = {
        ...edited.properties.layout ?? {},
        documents: edited.documents ?? {}
    }

    const layoutConfig: LayoutConfig = {
        type: productType,
        mode: 'design',
        importsData: true,
        canUnlock: logged.hasNoTenant(),
        domainContext: contextOf(edited)
    }

    const tabcompo = (() => {

        switch (tab) {

            case "usage": return <ProductUsage {...formstate} />
            case "layout": return <CampaignLoader>
                <LayoutDesigner

                    for={edited.id}

                    layoutConfig={layoutConfig}

                    domainContext={contextOf(edited)}

                    layout={layout}

                    onChange={change((t, layout: Layout) => {

                        //eslint-disable-next-line
                        const { documents = {}, ...rest } = layout

                        t.documents = documents

                        t.properties.layout = { ...rest }

                    }, true)}  // doesn't clean on layout changes to stabilise layout data for memoisations.
                />
            </CampaignLoader>

            default: return <ProductGeneralForm report={report.profile} {...formstate} />
        }
    })()


    const title = tab === "usage" ? t("common.labels.usage_one", { singular: l(edited.name) }) : name || (name === undefined ? `<${t('common.labels.new')}>` : "")

    const readonly = isProtected || !canEditIt


    return <Page readOnly={readonly}>

        <Sidebar>
            {saveBtn}
            {revertBtn}
            {cloneBtn}
            {branchBtn}
            {removeBtn}
            {rightsBtn}

            <br />

            <IdProperty id={detail.id} />

            <br />

            <LifecycleSummary {...edited.lifecycle} />

            <br />
            <div className="sidebar-property">
                <Paragraph>
                    {totalErrors > 0 ?

                        <Text type="danger">{t("common.validation.total_error_count", { totalErrors })}</Text>
                        :
                        <Text smaller className="emphasis">{t("common.validation.zero_error_count")}</Text>

                    }
                </Paragraph>
            </div>

            {(logged.can(edit) && !isNew) &&
                <>
                    <div style={{ marginTop: "25px" }}>{addProductBtn}</div>
                </>
            }
            <SideList data={products.all()}
                filterGroup={productGroup} filterBy={products.stringify} filterPlaceholder={t("common.components.table.filter_placeholder", { plural })}
                renderData={p => l(p.name)}
                render={(p: Product) => <ProductLabel noMemo lineage selected={p.id === edited.id} product={p} />} />




        </Sidebar>

        <Topbar>

            <Titlebar title={title} >
                {edited.predefined && <PredefinedLabel />}
                <TagList taglist={edited.tags} />
            </Titlebar>

            <Tab default id="info" icon={icns.form} name={t("common.labels.general")} badge={report.profile.errors() > 0} />
            <Tab disabled={isNew} id="layout" icon={icns.layout} name={t("common.labels.layout")} />
            {/* <Tab disabled={isNew} default id="usage" icon={icns.usage} name={t("common.labels.usage")} /> */}

            {liveGuard}

            {saveBtn}
            {revertBtn}
            {cloneBtn}
            {branchBtn}
            {removeBtn}
            {rightsBtn}

        </Topbar>

        {tabcompo}

        <RouteGuard when={dirty} onOk={() => products.resetNext()} />

        <Permissions resourceCentric resourceRange={[edited]} filter={{ resources: [edited.id, any] }} />

    </Page>

}
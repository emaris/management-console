
import { useCampaignUsage } from "#campaign/Usage"
import { buildinfo } from "apprise-frontend/buildinfo"
import { Button } from "apprise-frontend/components/Button"
import { useListState } from "apprise-frontend/components/hooks"
import { Label } from "apprise-frontend/components/Label"
import { Column, VirtualTable } from "apprise-frontend/components/VirtualTable"
import { specialise } from "apprise-frontend/iam/model"
import { icns } from "apprise-frontend/icons"
import { useLocale } from "apprise-frontend/model/hooks"
import { Page } from "apprise-frontend/scaffold/Page"
import { Titlebar } from "apprise-frontend/scaffold/PageHeader"
import { Sidebar } from "apprise-frontend/scaffold/Sidebar"
import { Topbar } from "apprise-frontend/scaffold/Topbar"
import { contextCategory } from 'apprise-frontend/system/constants'
import { useContextFilter } from 'apprise-frontend/system/filter'
import { useTags } from "apprise-frontend/tag/api"
import { useTagFilter, useTagHolderFilter } from 'apprise-frontend/tag/filter'
import { TagList } from "apprise-frontend/tag/Label"
import { tagRefsIn } from "apprise-frontend/tag/model"
import { tenantPlural } from "apprise-frontend/tenant/constants"
import { useUsers } from "apprise-frontend/user/api"
import { userPlural } from 'apprise-frontend/user/constants'
import { useBaseFilter } from 'apprise-frontend/utils/filter'
import { useProducts } from "emaris-frontend/product/api"
import { productPlural, productSingular, productType } from "emaris-frontend/product/constants"
import { ProductLabel } from "emaris-frontend/product/Label"
import { Product } from "emaris-frontend/product/model"
import { useProductPermissions } from "emaris-frontend/product/ProductPermissions"
import * as React from "react"
import { useTranslation } from "react-i18next"
import { useHistory, useLocation } from "react-router"


export const productGroup = productType

export const ProductList =  () => {

    const {l} = useLocale()
    const {t} = useTranslation()
    const history = useHistory()
    const {pathname} = useLocation()

    const tags = useTags()
    const {logged} = useUsers()
    const products = useProducts()

    const [singular,plural] = [t(productSingular),t(productPlural)]

    const {actions:{edit} } = products

    const [Permissions,showPermissions] = useProductPermissions()

    const usage = useCampaignUsage()

     const liststate = useListState<Product>();

    // -------------- authz privileges
    const canManage = React.useCallback((p:Product) => logged.can(specialise(products.actions.manage,p.id)), [logged, products])


    // -------------- actions
    const onRemove = ({id}:Product) => products.remove(
        id, 
        () => { 
            
            liststate.setSelected(liststate.selected.filter(l => l.id !== id))
        }
        , usage.isInUse(id)
    ) 

    const onClone = (product: Product) => {

        products.fetchOne(product.id).then( loaded => {

            products.setNext({ model: products.clone(loaded) })
            history.push(`${pathname}/new`)    
        })
        
    }

    const onRemoveAll = () => products.removeAll(liststate.selected.map(l=>l.id), ()=>liststate.setSelected([]))

    const onAddProduct = () => Promise.resolve(products.resetNext())


    // -------------- buttons

    const addBtn = <Button 
                        type="primary" 
                        icn={icns.add} 
                        enabled={logged.can(edit)} 
                        onClick={onAddProduct} 
                        linkTo={`${pathname}/new`} >
                            {t("common.buttons.add_one",{singular})}
                    </Button>

    const openBtn = (p:Product) => <Button 
                                        key={1} 
                                        icn={icns.open} 
                                        linkTo={ products.routeTo(p) }>
                                            {t("common.buttons.open")}
                                    </Button>
    
    const cloneBtn = (product:Product)  =>  
        <Button 
            key={3} 
            icn={icns.clone} 
            onClick={()=>onClone(product)}>
                {t("common.buttons.clone")}
        </Button>

    const rightsBtn =   <Button 
                            enabled={liststate.selected.length>0} 
                            icn={icns.permissions} 
                            style={{marginTop:30}} 
                            onClick={showPermissions}>
                                {t("common.buttons.permissions")}
                        </Button>

    const removeBtn =  (product:Product) => 
        <Button 
            key={2} 
            icn={icns.remove} 
            enabled={logged.can(edit) && product.lifecycle.state==='inactive'} 
            disabled={product.predefined}
            onClick={ () => onRemove(product) }>
                {t("common.buttons.remove")}
        </Button>


    const removeAllBtn = <Button 
                            type="danger" 
                            disabled={liststate.selected.length < 1} 
                            enabled={logged.can(edit)}
                            onClick={onRemoveAll}>
                                <span style={{fontVariantCaps:"all-small-caps"}}>(DEV)</span> {t("common.labels.remove_all", {count: liststate.selected.length})}
                        </Button>
   



    // filters

    const unfilteredData = products.all()

    // eslint-disable-next-line
    const sortedData = React.useMemo(()=> [...unfilteredData].sort(products.comparator),[unfilteredData])

    const {ContextSelector,contextFilteredData} = useContextFilter({
        data:sortedData,
        group:productGroup
    })

    const {BaseFilter,baseFilteredData} = useBaseFilter({
        data: contextFilteredData,
        readonlyUnless: canManage,
        group:productGroup
    })

    const {TagFilter,tagFilteredData} = useTagFilter({
        tagged:unfilteredData,
        filtered: baseFilteredData,
        excludeCategories:[contextCategory],
        key: `tags`,
        group:productGroup
    })

    const {TagFilter:AudienceFilter,tagFilteredData:audienceFilteredData} = useTagHolderFilter({
        tagged:unfilteredData,
        filtered: tagFilteredData,
        tagsOf: t=>t.audience.terms.flatMap(t => t.tags),
        placeholder:t("common.fields.audience.name"),
        key: `audience`,
        group:productGroup
    })

    const {TagFilter:UserProfileFilter,tagFilteredData: userProfileFilteredData} = useTagHolderFilter ({
        filtered:audienceFilteredData, 
        tagged: unfilteredData,
        tagsOf: t=>t.userProfile?.terms.flatMap(t => t.tags),
        placeholder:t("common.fields.user_profile.name"),
        key: `userprofile`,
        group:productGroup
    })


    const data = userProfileFilteredData

    return <Page>

        <Sidebar>
            {addBtn}
            {rightsBtn}

            {buildinfo.development &&
                <>
                    <br />
                    {removeAllBtn}
                </>
            }

        </Sidebar>

        <Topbar>

            <Titlebar title={plural} />                   

            {addBtn}
            {rightsBtn}

        </Topbar>

        <VirtualTable data={data} total={sortedData.length} state={liststate} 
            
            filterGroup={productGroup} filterBy={products.stringify} filterPlaceholder={t("common.components.table.filter_placeholder",{plural:plural.toLowerCase()})}
            filters={[ UserProfileFilter, AudienceFilter, TagFilter, BaseFilter, ContextSelector]}
            
            actions= { p => [openBtn(p), removeBtn(p), cloneBtn(p),ContextSelector]}
            onDoubleClick={t => history.push(products.routeTo(t))} >
         
             <Column flexGrow={2} title={t("common.fields.name_multi.name")} dataKey="name" 
                    dataGetter={(t:Product)=>l(t.name)}
                    cellRenderer={t=><ProductLabel tipTitle product={t.rowData} readonly={!canManage(t.rowData)} />} />

            <Column sortable={false} title={t("common.fields.audience.name")} dataKey="audience" dataGetter={(p:Product)=> p.audience?.terms?.length>0 ? <TagList taglist={tagRefsIn(p.audience)} /> : <Label title={t(tenantPlural)} />} />

            
            <Column sortable={false} title={t("common.fields.user_profile.name")} dataKey="userprofile" dataGetter={(r: Product) => r.userProfile?.terms?.length > 0 ? <TagList taglist={tagRefsIn(r.userProfile)} /> : <Label title={t(userPlural) } />} />

            {tags.allTagsOf(productType).length > 0 &&
              <Column sortable={false} flexGrow={3} title={t("common.fields.tags.name")} dataKey="tags" dataGetter={(p:Product)=><TagList truncateEllipsisLink={products.routeTo(p)} taglist={p.tags} />} />
            }

        </VirtualTable> 

        <Permissions resourceRange={liststate.selected} />

    </Page>

}


import Title from "antd/lib/typography/Title";
import { Button } from "apprise-frontend/components/Button";
import { icns } from 'apprise-frontend/icons';
import { Page } from "apprise-frontend/scaffold/Page";
import { Sidebar } from "apprise-frontend/scaffold/Sidebar";
import { givenChildren } from 'apprise-frontend/utils/children';
import { campaignIcon, campaignRoute } from "emaris-frontend/campaign/constants";
import { mclogo } from 'emaris-frontend/constants';
import { dashboardIcon, dashboardRoute } from "emaris-frontend/dashboard/constants";
import { eventIcon, eventRoute } from "emaris-frontend/event/constants";
import { productIcon, productRoute } from "emaris-frontend/product/constants";
import { requirementIcon, requirementRoute } from "emaris-frontend/requirement/constants";
import * as React from "react";
import { useTranslation } from "react-i18next";
import "./styles.scss";




export const Home = () => {

  const { t } = useTranslation()

  return <Page className="apppage" headerClassName="appheader">

    <Sidebar />

    <Title className="app-title">
      {mclogo}
      <div className="app-name">{t("mc_home.title")}</div>
      <div className="app-subtitle">{t("mc_home.subtitle")}</div>
    </Title>

    <div className="app-blurb">
      <Blurb color='teal' icon={dashboardIcon} className="dashboard" title={t("mc_home.dashboard_title")}>
        <ul>
          <li>{icns.dot}{t("mc_home.dashboard_description_1")}</li>
          <li>{icns.dot}{t("mc_home.dashboard_description_2")}</li>
          <li>{icns.dot}{t("mc_home.dashboard_description_3")}</li>
          <li>{icns.dot}{t("mc_home.dashboard_description_4")}</li>
        </ul>
        <Button linkTo={dashboardRoute}>{t("mc_home.dashboard_button_1")}</Button>
      </Blurb>
      <Blurb color='darkgoldenrod' icon={campaignIcon} title={t("mc_home.workbench_title")}>
        <ul>
          <li>{icns.dot}{t("mc_home.workbench_description_1")}</li>
          <li>{icns.dot}{t("mc_home.workbench_description_2")}</li>
          <li>{icns.dot}{t("mc_home.workbench_description_3")}</li>
        </ul>
        <Button linkTo={campaignRoute}>{t("mc_home.workbench_button_1")}</Button>
        <Button linkTo={`${campaignRoute}/new`} >{t("mc_home.workbench_button_2")}</Button>
      </Blurb>

      <Blurb color='lightseagreen' icon={requirementIcon} title={t("mc_home.requirements_title")} >
        <ul>
          <li>{icns.dot}{t("mc_home.requirements_description_1")}</li>
          <li>{icns.dot}{t("mc_home.requirements_description_2")}</li>
          <li>{icns.dot}{t("mc_home.requirements_description_3")}</li>
          <li>{icns.dot}{t("mc_home.requirements_description_4")}</li>
          <li>{icns.dot}{t("mc_home.requirements_description_5")}</li>
        </ul>
        <Button linkTo={requirementRoute} type="primary">{t("mc_home.requirements_button_1")}</Button>
        <Button linkTo={`${requirementRoute}/new`} type="primary">{t("mc_home.requirements_button_2")}</Button>
      </Blurb>

      <Blurb color='goldenrod' icon={productIcon} title={t("mc_home.products_title")}>
        <ul>
          <li>{icns.dot}{t("mc_home.products_description_1")}</li>
          <li>{icns.dot}{t("mc_home.products_description_2")}</li>
          <li>{icns.dot}{t("mc_home.products_description_3")}</li>
          <li>{icns.dot}{t("mc_home.products_description_4")}</li>
        </ul>
        <Button linkTo={productRoute}>{t("mc_home.products_button_1")}</Button>
        <Button linkTo={`${productRoute}/new`}>{t("mc_home.products_button_2")}</Button>
      </Blurb>

      <Blurb color='darkturquoise' icon={eventIcon} title={t("mc_home.events_title")}>
        <ul>
          <li>{icns.dot}{t("mc_home.events_description_1")}</li>
          <li>{icns.dot}{t("mc_home.events_description_2")}</li>
          <li>{icns.dot}{t("mc_home.events_description_3")}</li>
        </ul>
        <Button linkTo={eventRoute}>{t("mc_home.events_button_1")}</Button>
        <Button linkTo={`${eventRoute}/new`}>{t("mc_home.events_button_2")}</Button>
      </Blurb>
    </div>

  </Page>

}

type HomeEntryProps = React.PropsWithChildren<{
  className?: string
  icon: JSX.Element
  color?: string
  title: string
  buttons?: JSX.Element[]
}>

const Blurb = (props: HomeEntryProps) => {

  const { className, icon, color, title, children } = props

  const { btns, other } = givenChildren(children).byTypes([["btns", Button]]);

  return <div className={`entry ${className ?? ''}`}>
    <div className="innerEntry">
      <div className="logo">
        <div style={{ color }} className="icon-halo">
          {icon}
        </div>
      </div>
      <div className="contents">
        <div className="title">{title}</div>
        <div className="text">
          {/* <div className="subtitle">{subtitle}</div> */}
          {other}
        </div>
        {btns && <div className="buttons">{btns.map(b => React.cloneElement(b, { className: "button", size: 'large', style: { background:color} }))} </div>}
      </div>
    </div>
  </div >


}


import * as React from "react"
import { Icon } from "antd"

import { AiOutlineHome } from "react-icons/ai"

export const homeRoute = "/"
export const homeIcon = <Icon component={AiOutlineHome} />
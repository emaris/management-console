
import { requirementRoute } from "emaris-frontend/requirement/constants"
import { RequirementLoader } from "emaris-frontend/requirement/Loader"
import * as React from "react"
import { Route, Switch } from "react-router"
import { NewRequirementDetail, RequirementDetail } from "./Detail"
import { RequirementList } from "./List"
import { Placeholder } from "apprise-frontend/components/Placeholder"


export const Requirements =  () =>{
     
    return  <RequirementLoader placeholder={Placeholder.page}>
                <Switch>
                    <Route exact path={requirementRoute} component={RequirementList} />
                    <Route path={`${requirementRoute}/new`} component={NewRequirementDetail} />
                    <Route path={`${requirementRoute}/:id`} component={RequirementDetail} />
                </Switch>
            </RequirementLoader>

}
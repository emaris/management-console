
import { Button } from "apprise-frontend/components/Button";
import { IdProperty } from "apprise-frontend/components/IdProperty";
import { NoSuchRoute } from "apprise-frontend/components/NoSuchRoute";
import { RouteGuard } from "apprise-frontend/components/RouteGuard";
import { SideList } from "apprise-frontend/components/SiderList";
import { Paragraph, Text } from "apprise-frontend/components/Typography";
import { useFormState } from "apprise-frontend/form/hooks";
import { iamPlural } from "apprise-frontend/iam/constants";
import { any, specialise } from "apprise-frontend/iam/model";
import { icns } from "apprise-frontend/icons";
import { LayoutDesigner } from "apprise-frontend/layout/Designer";
import { Layout, LayoutConfig } from "apprise-frontend/layout/model";
import { PredefinedLabel } from 'apprise-frontend/model/constants';
import { useLocale } from "apprise-frontend/model/hooks";
import { LifecycleSummary } from "apprise-frontend/model/lifecycle";
import { PushGuard } from 'apprise-frontend/push/PushGuard';
import { Page } from "apprise-frontend/scaffold/Page";
import { Subtitle, Titlebar } from "apprise-frontend/scaffold/PageHeader";
import { Sidebar } from "apprise-frontend/scaffold/Sidebar";
import { Tab } from "apprise-frontend/scaffold/Tab";
import { Topbar } from "apprise-frontend/scaffold/Topbar";
import { BytestreamedContext, useBytestreamedHelper } from "apprise-frontend/stream/BytestreamedHelper";
import { DocumentLocalisedStream, localisedStreamsOf } from "apprise-frontend/stream/model";
import { useSystem } from "apprise-frontend/system/api";
import { TagList } from "apprise-frontend/tag/Label";
import { useUsers } from "apprise-frontend/user/api";
import { through } from "apprise-frontend/utils/common";
import { paramsInQuery, parentIn } from "apprise-frontend/utils/routes";
import { CampaignLoader } from "emaris-frontend/campaign/Loader";
import { useDashboards } from 'emaris-frontend/dashboard/api';
import { useRequirements } from "emaris-frontend/requirement/api";
import { requirementPlural, requirementRoute, requirementSingular, requirementType } from "emaris-frontend/requirement/constants";
import { RequirementDetailLoader } from 'emaris-frontend/requirement/DetailLoader';
import { RequirementLabel } from "emaris-frontend/requirement/Label";
import { Requirement, RequirementDto } from "emaris-frontend/requirement/model";
import { useRequirementPermissions } from "emaris-frontend/requirement/RequirementPermissions";
import * as React from "react";
import { useTranslation } from "react-i18next";
import { useHistory, useLocation, useParams } from "react-router";
import { RequirementGeneralForm } from "./GeneralForm";
import { requirementGroup } from './List';
import { RequirementUsage } from "./Usage";
import { useLiveAssetGuard } from "#campaign/LiveGuard";



export const RequirementDetail = () => {

    const { id } = useParams<{ id: string }>()
    const { pathname } = useLocation()

    const requirements = useRequirements()

    const current = requirements.lookup(id)

    if (!current)
        return <NoSuchRoute backTo={parentIn(pathname)} />

    return <PushGuard>
        <RequirementDetailLoader key={current.id} id={current.id} >{
        
            loaded =>

            <BytestreamedContext target={loaded.id}>
                <InnerRequirementDetail isNew={false} detail={loaded} />
            </BytestreamedContext>
            
        }</RequirementDetailLoader>
    </PushGuard>

}

export const NewRequirementDetail = () => {

    const requirements = useRequirements()

    const detail = requirements.next()

    return <BytestreamedContext><InnerRequirementDetail isNew={true} detail={detail.model} /></BytestreamedContext>

}


const InnerRequirementDetail = (props: { isNew: boolean, detail: Requirement }) => {

    const { t } = useTranslation()
    const { l } = useLocale()
    const history = useHistory()
    const { pathname, search } = useLocation()

    const { contextOf } = useSystem()
    const { logged } = useUsers()
    const requirements = useRequirements()
    const helper = useBytestreamedHelper()
    const [Permissions, showPermissions] = useRequirementPermissions()


    const singular = t(requirementSingular)

    const { isNew, detail } = props

    const { actions: { edit } } = requirements

    const formstate = useFormState<Requirement>(detail);

    const { edited, initial, change, reset, dirty } = formstate

    const dashboard = useDashboards()

    const { liveGuarded, liveGuard, usage } = useLiveAssetGuard({
        id: edited.id,
        disabled: dirty,
        link: c => dashboard.given(c).routeToAssetWith(requirementType, edited.id),
        singular
    })

    const { tab } = paramsInQuery(search)

    const plural = t(requirementPlural).toLowerCase()
    const name = l(edited.name)

    const editIt = specialise(edit, detail.id)
    const canEditIt = logged.can(editIt)

    const isProtected = edited.predefined || liveGuarded



    // -------------- effetcs
    React.useEffect(() =>
        requirements.resetNext(),
        //eslint-disable-next-line
        [])

    // -------------- error reporting

    const report = {
        profile: requirements.validateRequirement(edited)
    }

    const totalErrors = Object.keys(report).reduce((total, key) => total += report[key].errors(), 0)


    // -------------- actions


    const currentDocumentStreams = () => Object.values(edited.documents ?? {}).flatMap(ds => ds.flatMap(localisedStreamsOf))

    const updateDocumentStreams = (requirement: Requirement, localisedStreams: DocumentLocalisedStream[]) => {

        const docs = Object.values(requirement.documents ?? {}).flatMap(ds => ds).flatMap(d => d)

        localisedStreams.forEach(localisedStream => {

            const lang = localisedStream.lang
            const match = docs.find(d => `${d.id}-${lang}` === localisedStream.id)

            if (match)
                match[lang] = localisedStream.stream

        })

    }

    const onSave = (requirement: RequirementDto) => helper.upload(...currentDocumentStreams())

        .then(through(streams => updateDocumentStreams(requirement, streams)))
        .then(() => requirements.save(requirement))
        .then(through(saved => reset(saved, false)))
        .then(through(helper.reset))
        .then(saved => { requirements.resetNext(); return saved })
        .then(saved => history.push(`${requirements.routeTo(saved)}${search}`))

    const onRevert = () => reset(initial, helper.reset)

    const onRemove = () => requirements.remove(detail.id, () => {

        history.push(parentIn(pathname))

    }, usage.isInUse(edited.id))

    const onClone = () => {
        requirements.setNext({ model: requirements.clone(edited) })
        history.push(`${parentIn(pathname)}/new`)
    }

    const onBranch = () => {
        requirements.setNext({ model: requirements.branch(edited) })
        history.push(`${parentIn(pathname)}/new`)
    }

    const onAddRequirement = () => Promise.resolve(requirements.resetNext())

    //console.log("documents",edited.documents,"descriptors",helper.all())

    // -------------- action buttons

    const removeBtn = <Button
        icn={icns.remove}
        enabled={canEditIt && edited.lifecycle.state === 'inactive'}
        disabled={dirty || isNew || edited.predefined}
        onClick={onRemove}>
        {t("common.buttons.remove")}
    </ Button>

    const saveBtn = <Button
        type="primary"
        icn={icns.save}
        enabled={dirty}
        disabled={totalErrors > 0}
        dot={totalErrors > 0}
        onClick={() => onSave(edited)}>
        {t("common.buttons.save")}
    </Button>

    const revertBtn = <Button
        icn={icns.revert}
        enabled={dirty}
        type="danger"
        onClick={onRevert}>
        {t("common.buttons.revert")}
    </Button>

    const cloneBtn = <Button
        enabled={logged.can(edit)}
        disabled={isNew || totalErrors > 0}
        icn={icns.clone}
        onClick={onClone}>
        {t("common.buttons.clone")}
    </Button>

    const branchBtn = <Button
        enabled={logged.can(edit)}
        disabled={isNew || totalErrors > 0}
        icn={icns.branch}
        onClick={onBranch}>
        {t("campaign.buttons.branch")}
    </Button>

    const rightsBtn = <Button
        icn={icns.permissions}
        enabledOnReadOnly
        disabled={isNew || dirty}
        onClick={showPermissions}>
        {t(iamPlural)}
    </Button>

    const addRequirementBtn = <Button
        enabledOnReadOnly
        type="primary"
        icn={icns.add}
        enabled={logged.can(edit)}
        onClick={onAddRequirement}
        linkTo={`${requirementRoute}/new`} >
        {t("common.buttons.add_one", { singular })}
    </Button>


    const layoutConfig: LayoutConfig = {
        type: requirementType,
        mode: 'design',
        exportsData: true,
        canUnlock: logged.hasNoTenant(),
        domainContext: contextOf(edited)
    }

    const tabcompo = (() => {

        switch (tab) {

            case "usage": return <RequirementUsage {...formstate} />
            case "layout": return <CampaignLoader>
                <LayoutDesigner
                    for={edited.id}
                    domainContext={contextOf(edited)}
                    layoutConfig={layoutConfig}
                    layout={{ ...edited.properties.layout ?? {}, documents: edited.documents ?? {} }}
                    onChange={change((t, layout: Layout) => {

                        const { documents = {}, ...rest } = layout

                        t.documents = documents

                        t.properties.layout = { ...rest }
                    })} />
            </CampaignLoader>

            default: return <RequirementGeneralForm isNew={isNew} report={report.profile} {...formstate} />
        }
    })()


    const readonly = isProtected || !logged.can(editIt)


    return <Page readOnly={readonly}>

        <Sidebar>

            {saveBtn}
            {revertBtn}
            {cloneBtn}
            {branchBtn}
            {removeBtn}
            {rightsBtn}

            <br />

            <IdProperty id={detail.id} />

            <br />

            <LifecycleSummary {...edited.lifecycle} />

            <br />

            <div className="sidebar-property">
                <Paragraph>
                    {totalErrors > 0 ?

                        <Text type="danger">{t("common.validation.total_error_count", { totalErrors })}</Text>
                        :
                        <Text smaller className="emphasis">{t("common.validation.zero_error_count")}</Text>

                    }
                </Paragraph>
            </div>

            {(logged.can(edit) && !isNew) &&
                <>
                    <div style={{ marginTop: "25px" }}>{addRequirementBtn}</div>
                </>
            }
            <SideList<Requirement> data={requirements.all()}
                filterGroup={requirementGroup} filterBy={requirements.stringify} filterPlaceholder={t("common.components.table.filter_placeholder", { plural })}
                renderData={r => l(r.name)}
                render={r => <RequirementLabel noMemo selected={r.id === edited.id} lineage requirement={r} />} />


        </Sidebar>

        <Topbar>

            <Titlebar title={name || (name === undefined ? `<${t('common.labels.new')}>` : "")}>
                <Subtitle>{l(edited.description)}</Subtitle>
                {edited.predefined && <PredefinedLabel />}
                <TagList taglist={edited.tags} />
            </Titlebar>

            <Tab default id="info" icon={icns.form} name={t("common.labels.general")} badge={report.profile.errors() > 0} />
            <Tab disabled={isNew} id="layout" icon={icns.layout} name={t("common.labels.layout")} />
            {/* <Tab disabled={isNew} default id="usage" icon={icns.usage} name={t("common.labels.usage")} /> */}

            {liveGuard}

            {saveBtn}
            {revertBtn}
            {cloneBtn}
            {branchBtn}
            {removeBtn}
            {rightsBtn}

        </Topbar>

        {tabcompo}

        <RouteGuard when={dirty} onOk={() => requirements.resetNext()} />

        <Permissions resourceCentric resourceRange={[edited]} filter={{ resources: [edited.id, any] }} />

    </Page>

}


import { buildinfo } from "apprise-frontend/buildinfo"
import { Button } from "apprise-frontend/components/Button"
import { useListState } from "apprise-frontend/components/hooks"
import { Label } from "apprise-frontend/components/Label"
import { Column, VirtualTable } from "apprise-frontend/components/VirtualTable"
import { iamPlural } from "apprise-frontend/iam/constants"
import { specialise } from "apprise-frontend/iam/model"
import { icns } from "apprise-frontend/icons"
import { useLocale } from "apprise-frontend/model/hooks"
import { Page } from "apprise-frontend/scaffold/Page"
import { Titlebar } from "apprise-frontend/scaffold/PageHeader"
import { Sidebar } from "apprise-frontend/scaffold/Sidebar"
import { Topbar } from "apprise-frontend/scaffold/Topbar"
import { contextCategory } from 'apprise-frontend/system/constants'
import { useContextFilter } from 'apprise-frontend/system/filter'
import { useTags } from "apprise-frontend/tag/api"
import { useTagFilter, useTagHolderFilter } from 'apprise-frontend/tag/filter'
import { TagList } from "apprise-frontend/tag/Label"
import { tagRefsIn } from "apprise-frontend/tag/model"
import { tenantPlural } from "apprise-frontend/tenant/constants"
import { useUsers } from "apprise-frontend/user/api"
import { userPlural } from 'apprise-frontend/user/constants'
import { useBaseFilter } from 'apprise-frontend/utils/filter'
import { useCampaignUsage } from '../campaign/Usage'
import { useRequirements } from "emaris-frontend/requirement/api"
import { requirementPlural, requirementSingular, requirementType } from "emaris-frontend/requirement/constants"
import { RequirementLabel } from "emaris-frontend/requirement/Label"
import { Requirement } from "emaris-frontend/requirement/model"
import { useRequirementPermissions } from "emaris-frontend/requirement/RequirementPermissions"
import * as React from "react"
import { useTranslation } from "react-i18next"
import { useHistory, useLocation } from "react-router"


export const requirementGroup = requirementType

export const RequirementList = () => {

    const { l } = useLocale()
    const { t } = useTranslation()
    const history = useHistory()
    const { pathname } = useLocation()

    const tags = useTags()
    const { logged } = useUsers()
    const requirements = useRequirements()

    const [singular, plural] = [t(requirementSingular), t(requirementPlural)]

    const { actions: { edit } } = requirements

    const [Permissions, showPermissions] = useRequirementPermissions()

    const usage = useCampaignUsage()

    const liststate = useListState<Requirement>()


    // -------------- authz privileges
    const canManage = (r: Requirement) => logged.can(specialise(requirements.actions.manage, r.id))


    // -------------- actions
    const onRemove = ({ id }: Requirement) => requirements.remove(
        id,
        () => { 
            
            liststate.setSelected(liststate.selected.filter(l => l.id !== id))
        }
        , usage.isInUse(id)
    )

    const onClone = (requirement: Requirement) => {

        requirements.fetchOne(requirement.id).then( loaded => {

            requirements.setNext({ model: requirements.clone(loaded) })
            history.push(`${pathname}/new`)    
        })
    }

    const onRemoveAll = () => requirements.removeAll(liststate.selected.map(l => l.id), () => liststate.setSelected([]))

    const onAddRequirement = () => Promise.resolve(requirements.resetNext())

    // -------------- buttons

    const addBtn = <Button
        type="primary"
        icn={icns.add}
        enabled={logged.can(edit)}
        onClick={onAddRequirement}
        linkTo={`${pathname}/new`}>
        {t("common.buttons.add_one", { singular })}
    </Button>

    const openBtn = (r: Requirement) => <Button
        key={1}
        icn={icns.open}
        linkTo={requirements.routeTo(r)}>
        {t("common.buttons.open")}
    </Button>


    const cloneBtn = (requirement: Requirement) =>
        <Button
            key={3}
            icn={icns.clone}
            onClick={() => onClone(requirement)} >
            {t("common.buttons.clone")}
        </Button>

    const rightsBtn = <Button
        enabled={liststate.selected.length > 0}
        icn={icns.permissions}
        style={{ marginTop: 30 }}
        onClick={showPermissions}>
        {t(iamPlural)}
    </Button>

    const removeBtn = (requirement: Requirement) =>
        <Button
            key={2}
            icn={icns.remove}
            enabled={logged.can(edit) && requirement.lifecycle.state==='inactive' }
            disabled={requirement.predefined || usage.isInUse(requirement)}
            onClick={() => onRemove(requirement)} >
            {t("common.buttons.remove")}
        </Button>

    const removeAllBtn = <Button
        type="danger"
        disabled={liststate.selected.length < 1}
        enabled={logged.can(edit)}
        onClick={onRemoveAll} >
        <span style={{ fontVariantCaps: "all-small-caps" }}>(DEV)</span> {t("common.labels.remove_all", { count: liststate.selected.length })}
    </Button>


    // filters

    const unfilteredData = requirements.all()

   // eslint-disable-next-line
    const sortedData = React.useMemo(()=> [...unfilteredData].sort(requirements.comparator),[unfilteredData])
   
    const {ContextSelector,contextFilteredData} = useContextFilter({
        data:sortedData,
        group: requirementGroup
    })

    const {BaseFilter,baseFilteredData} = useBaseFilter({
        data: contextFilteredData,
        readonlyUnless: canManage,
        group: requirementGroup
    })


    const {TagFilter,tagFilteredData} = useTagFilter({
        filtered:baseFilteredData, 
        tagged: unfilteredData,
        excludeCategories:[contextCategory],
        key: `tags`,
        group: requirementGroup
    })

    const {TagFilter:AudienceFilter,tagFilteredData: audienceFilteredData} = useTagHolderFilter ({
        filtered:tagFilteredData, 
        tagged: unfilteredData,
        tagsOf: t=>t.audience.terms.flatMap(t => t.tags),
        placeholder:t("common.fields.audience.name"),
        key: `audience`,
        group: requirementGroup
    })

    const {TagFilter:UserProfileFilter,tagFilteredData: userProfileFilteredData} = useTagHolderFilter ({
        filtered:audienceFilteredData, 
        tagged: unfilteredData,
        tagsOf: t=>t.userProfile?.terms.flatMap(t => t.tags),
        placeholder:t("common.fields.user_profile.name"),
        key: `profile`,
        group: requirementGroup
    })

    const data = userProfileFilteredData

    return <Page>

        <Sidebar>
            {addBtn}
            {rightsBtn}

            {buildinfo.development &&
                <>
                    <br />
                    {removeAllBtn}
                </>
            }

        </Sidebar>

        <Topbar>

            <Titlebar title={ plural } />

            {addBtn}
            {rightsBtn}

        </Topbar>


        <VirtualTable state={liststate} data={data} total={unfilteredData.length}
            filterGroup={requirementGroup}
            filterBy={requirements.stringify} filterPlaceholder={t("common.components.table.filter_placeholder", { plural: plural.toLowerCase() })}
            filters={[ UserProfileFilter, AudienceFilter, TagFilter, BaseFilter, ContextSelector]}
            rowKey="id"
            actions={r => [openBtn(r), removeBtn(r), cloneBtn(r)]}
            onDoubleClick={r => history.push(requirements.routeTo(r))} >


            <Column flexGrow={2} title={t("common.fields.name_multi.name")} dataKey="name"
                dataGetter={(r: Requirement) => l(r.name)} cellRenderer={r => <RequirementLabel tipTitle lineage requirement={r.rowData} readonly={!canManage(r.rowData)} />} />

            <Column sortable={false} title={t("common.fields.audience.name")} dataKey="audience" dataGetter={(r: Requirement) => r.audience?.terms?.length > 0 ? <TagList taglist={tagRefsIn(r.audience)} /> : <Label title={t(tenantPlural)} />} />

            <Column sortable={false} title={t("common.fields.user_profile.name")} dataKey="userprofile" dataGetter={(r: Requirement) => r.userProfile?.terms?.length > 0 ? <TagList taglist={tagRefsIn(r.userProfile)} /> : <Label title={t(userPlural)} />} />

            {tags.allTagsOf(requirementType).length > 0 &&
                <Column sortable={false} flexGrow={2} title={t("common.fields.tags.name")} dataKey="tags" dataGetter={(r: Requirement) => <TagList truncateEllipsisLink={requirements.routeTo(r)} taglist={r.tags} />} />
            }

        </VirtualTable>

        <Permissions resourceRange={liststate.selected} />

    </Page>

}
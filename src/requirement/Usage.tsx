import { FormState } from "apprise-frontend/form/hooks"
import { useLocale } from "apprise-frontend/model/hooks"
import { ContentHeader } from "apprise-frontend/scaffold/ContentHeader"
import { campaignIcon } from "emaris-frontend/campaign/constants"
import { campaignmodule } from "emaris-frontend/campaign/module"
import { useCampaignUsage } from "emaris-frontend/campaign/UsageList"
import { requirementType } from "emaris-frontend/requirement/constants"
import { Requirement } from "emaris-frontend/requirement/model"
import * as React from "react"
import { useTranslation } from "react-i18next"

export type Props = FormState<Requirement>

export const RequirementUsage = (props:Props) => {

    const {t} = useTranslation()
    const {l} = useLocale()
   

    const {edited} = props

    const [campaignUsageList,campaignList] = useCampaignUsage({type:requirementType,target:edited.id})

    const name = l(edited.name)
    const campcount = campaignList?.length ?? -1
    const campaigns = t( campcount!==1 ? campaignmodule.namePlural: campaignmodule.nameSingular)

    return <> 

            <ContentHeader  title={<span>{campaignIcon}&nbsp;&nbsp; <span>{campcount===-1? '...' : campcount}</span> {campaigns}</span>} 
                            subtitle={t("campaign.feedback.used_in",{name,campcount:campcount===-1? '...' : campcount})} />

            {campaignUsageList}

          </>


}
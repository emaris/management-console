
import { state } from "#app";
import { App } from "apprise-frontend/App";
import { LogoutItem } from 'apprise-frontend/call/Logout';
import { NoSuchRoute } from "apprise-frontend/components/NoSuchRoute";
import { layoutmodule } from "apprise-frontend/layout/module";
import { PushEvents } from 'apprise-frontend/push/PushEvents';
import { ExternalLink } from "apprise-frontend/scaffold/ExternalLink";
import { Scaffold } from "apprise-frontend/scaffold/Scaffold";
import { Section } from "apprise-frontend/scaffold/Section";
import { Slider } from "apprise-frontend/scaffold/Slider";
import { bytestreamModule } from "apprise-frontend/stream/module";
import { tagmodule } from "apprise-frontend/tag/module";
import { tenants } from 'apprise-frontend/tenant/api';
import { tenantPlural, tenantRoute } from "apprise-frontend/tenant/constants";
import { tenantmodule } from "apprise-frontend/tenant/module";
import { useUsers } from "apprise-frontend/user/api";
import { usermodule } from "apprise-frontend/user/module";
import { UserProfile, userprofileId, UserProfileItem } from "apprise-frontend/user/Profile";
import { messagePlural, messageType } from "apprise-messages/constants";
import { messagemodule } from "apprise-messages/module";
import { Campaigns } from "#campaign/Campaigns";
import { CampaignUsage } from '#campaign/Usage';
import { campaigns } from 'emaris-frontend/campaign/api';
import { campaignIcon, campaignPlural, campaignRoute } from "emaris-frontend/campaign/constants";
import { CampaignPreloader } from 'emaris-frontend/campaign/Loader';
import { campaignmodule, compliancemodule, timelinessmodule } from "emaris-frontend/campaign/module";
import { submissionModule } from "emaris-frontend/campaign/submission/module";
import { AcIcon, acRoute, iotcLogo, iotcRoute, mcbanner, McIcon } from "emaris-frontend/constants";
import { dashboardIcon, dashboardName, dashboardRoute } from "emaris-frontend/dashboard/constants";
import { Dashboard } from "emaris-frontend/dashboard/Dashboard";
import { summaryName, summaryRoute } from "emaris-frontend/dashboard/ViewRouter";
import { events } from 'emaris-frontend/event/api';
import { eventIcon, eventPlural, eventRoute } from "emaris-frontend/event/constants";
import { eventmodule } from "emaris-frontend/event/module";
import { products } from 'emaris-frontend/product/api';
import { productIcon, productPlural, productRoute } from "emaris-frontend/product/constants";
import { productmodule } from "emaris-frontend/product/module";
import { requirements } from 'emaris-frontend/requirement/api';
import { requirementIcon, requirementPlural, requirementRoute } from "emaris-frontend/requirement/constants";
import { requirementmodule } from "emaris-frontend/requirement/module";
import { Events } from '#event/Events';
import { homeIcon, homeRoute } from "#home/constants";
import { Home } from "#home/Home";
import { mockery } from '#mockery';
import { Products } from "#product/Products";
import { PropsWithChildren } from 'react';
import { useTranslation } from "react-i18next";
import { Requirements } from "#requirement/Requirements";
import "./variables.css";



const modules = [usermodule, tagmodule, bytestreamModule, eventmodule, tenantmodule, messagemodule, requirementmodule, productmodule, campaignmodule, layoutmodule, submissionModule, /*mailmodule,*/ compliancemodule, timelinessmodule]



const ManagementConsole = () => {

    const { t } = useTranslation()

    return <EntryCheck>
        <PushEvents>
            <CampaignPreloader />
            <CampaignUsage>
                <Scaffold title={t("mc_home.title")} shortTitle={t("mc_home.short_title")} icon={<McIcon color='white' />} banner={mcbanner} >
                    <Section title={t("mc_home.name")} icon={homeIcon} path={homeRoute} exact
                        crumbs={{

                            [homeRoute]: { name: "" },
                            [requirementRoute]: { name: t(requirementPlural) },
                            [`${requirementRoute}/new`]: { name: t("common.labels.new") },
                            [`${requirementRoute}/*`]: { resolver: requirements.breadcrumbResolver },
                            [productRoute]: { name: t(productPlural) },
                            [`${productRoute}/new`]: { name: t("common.labels.new") },
                            [`${productRoute}/*`]: { resolver: products.breadcrumbResolver },
                            [campaignRoute]: { name: t(campaignPlural) },
                            [`${campaignRoute}/new`]: { name: t("common.labels.new") },
                            [`${campaignRoute}/*`]: { resolver: campaigns.breadcrumbResolver },
                            [eventRoute]: { name: t(eventPlural) },
                            [`${eventRoute}/new`]: { name: t("common.labels.new") },
                            [`${eventRoute}/*`]: { resolver: events.breadcrumbResolver },
                            [`${campaignRoute}/*${requirementRoute}`]: { name: t(requirementPlural) },
                            [`${campaignRoute}/*${productRoute}`]: { name: t(productPlural) },
                            [`${campaignRoute}/*${tenantRoute}`]: { name: t(tenantPlural) },
                            [`${campaignRoute}/*${eventRoute}`]: { name: t(eventPlural) },

                            [dashboardRoute]: { name: t(dashboardName) },
                            [`${dashboardRoute}/*/${summaryRoute}`]: { name: t(summaryName) },
                            [`${dashboardRoute}/*`]: { resolver: campaigns.breadcrumbResolver },
                            [`${dashboardRoute}/*${requirementRoute}`]: { name: t(requirementPlural) },
                            [`${dashboardRoute}/*${requirementRoute}/*`]: { resolver: requirements.breadcrumbResolver },
                            [`${dashboardRoute}/*${requirementRoute}/*/${summaryRoute}`]: { name: t(summaryName) },
                            [`${dashboardRoute}/*${requirementRoute}/*/${messageType}`]: { name: t(messagePlural) },
                            [`${dashboardRoute}/*/*/*${requirementRoute}`]: { name: t(requirementPlural) },
                            [`${dashboardRoute}/*/*/*${requirementRoute}/*`]: { resolver: requirements.breadcrumbResolver },
                            [`${dashboardRoute}/*${productRoute}`]: { name: t(productPlural) },
                            [`${dashboardRoute}/*${productRoute}/*`]: { resolver: products.breadcrumbResolver },
                            [`${dashboardRoute}/*/*/*${productRoute}`]: { name: t(productPlural) },
                            [`${dashboardRoute}/*/*/*${productRoute}/*`]: { resolver: products.breadcrumbResolver },
                            [`${dashboardRoute}/*${productRoute}/*/${summaryRoute}`]: { name: t(summaryName) },
                            [`${dashboardRoute}/*${productRoute}/*/${messageType}`]: { name: t(messagePlural) },
                            [`${dashboardRoute}/*${tenantRoute}`]: { name: t(tenantPlural) },
                            [`${dashboardRoute}/*${tenantRoute}/*`]: { resolver: tenants.breadcrumbResolver },
                            [`${dashboardRoute}/*/*/*${tenantRoute}`]: { omit: true },
                            [`${dashboardRoute}/*/*/*${tenantRoute}/*`]: { resolver: tenants.breadcrumbResolver },
                            [`${dashboardRoute}/*${tenantRoute}/*/${summaryRoute}`]: { name: t(summaryName) },
                            [`${dashboardRoute}/*${tenantRoute}/*/${messageType}`]: { name: t(messagePlural) },
                            [`${dashboardRoute}/*/${messageType}`]: { name: t(messagePlural) },
                            [`${dashboardRoute}/*${eventRoute}`]: { name: t("dashboard.calendar.name") },
                            [`${dashboardRoute}/*/*/*${eventRoute}`]: { name: t("dashboard.calendar.name") },
                            [`${dashboardRoute}/*/*/*/*/*/latest`]: { name: t("submission.labels.latest") },
                            [`${dashboardRoute}/*/*/*/*/*/*`]: { name: t("submission.labels.old_version") }
                            ,

                        }}>
                        <Home />
                    </Section>

                    <Section title={t("dashboard.name")} icon={dashboardIcon} path={dashboardRoute}>
                        <Dashboard />
                    </Section>

                    <Section title={t(campaignPlural)} icon={campaignIcon} path={campaignRoute}>
                        <Campaigns />
                    </Section>

                    <Section title={t(requirementPlural)} icon={requirementIcon} path={requirementRoute}>
                        <Requirements />
                    </Section>

                    <Section title={t(productPlural)} icon={productIcon} path={productRoute}>
                        <Products />
                    </Section>

                    <Section title={t(eventPlural)} icon={eventIcon} path={eventRoute}>
                        <Events />
                    </Section>

                    {/* <Section title={t(userPlural)} icon={userIcon} path={userRoute}
                    crumbs={{ [userRoute]: { name: t(userPlural) } }} >
                    <Users />
                </Section>

                <Section title={t(tenantPlural)} icon={tenantIcon} path={tenantRoute}
                    crumbs={{
                        [tenantRoute]: { name: t(tenantPlural) },
                        [`${tenantRoute}/*`]: { resolver: tenants.breadcrumbResolver }
                    }}>
                    <Tenants />
                </Section> */}

                    <Slider id={userprofileId}>
                        <UserProfile />
                    </Slider>

                    {UserProfileItem()}
                    {LogoutItem()}

                    <ExternalLink title={t("links.iotc")} icon={iotcLogo} href={iotcRoute} />
                    <ExternalLink title={t("links.ac")} icon={<AcIcon />} href={acRoute} />

                </Scaffold>
            </CampaignUsage>
        </PushEvents>
    </EntryCheck>

}


const EntryCheck = (props: PropsWithChildren<any>) => {

    const { logged } = useUsers()

    return logged.hasNoTenant() ? props.children : <NoSuchRoute />

}


export const ManagementConsoleInit = () =>

        <App initState={state} modules={modules} mockery={mockery}>
            <ManagementConsole />
        </App>


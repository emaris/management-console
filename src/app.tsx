import { EmarisState, initialEmariState } from 'emaris-frontend/state/model';
import { ChangeFunction } from 'apprise-frontend/state/model';


export type State = ChangeFunction<State> & EmarisState

export const state : State = { ...initialEmariState }






import { ManagementConsoleInit } from '#index';
import ReactDOM from 'react-dom';


ReactDOM.render(

    <ManagementConsoleInit />

    , document.getElementById('root'))

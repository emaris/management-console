
import { Placeholder } from "apprise-frontend/components/Placeholder"
import { useAsyncRender } from "apprise-frontend/utils/hooks"
import { useRequirementInstances } from "emaris-frontend/campaign/requirement/api"
import { RequirementLoader } from "emaris-frontend/requirement/Loader"
import * as React from "react"
import { useCurrentCampaign } from "../../lib/emaris-frontend/campaign/hooks"


type Props = React.PropsWithChildren<{}>


export const RequirementInstanceLoader = (props:Props) => {

    const campaign = useCurrentCampaign()

    const {fetchAll,areReady} = useRequirementInstances(campaign)

    const [render] = useAsyncRender({when:areReady(),task:fetchAll,content:props.children,placeholder:Placeholder.list})

return <RequirementLoader placeholder={Placeholder.list}>
          {render}
      </RequirementLoader>
  
  }
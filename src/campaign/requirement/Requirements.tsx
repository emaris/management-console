import { NoSuchRoute } from "apprise-frontend/components/NoSuchRoute";
import { campaignRoute } from "emaris-frontend/campaign/constants";
import { CampaignValidation } from "emaris-frontend/campaign/validation";
import { requirementType } from "emaris-frontend/requirement/constants";
import * as React from "react";
import { Route, Switch } from "react-router-dom";
import { RequirementInstanceList } from "./List";

type Props = {

    report: ReturnType<CampaignValidation['validateRequirements']>
}


export const RequirementInstances = (props:Props)=> {

    return  <Switch>
                <Route exact path={`${campaignRoute}/:name/${requirementType}`} render={()=><RequirementInstanceList {...props} />} />
                <NoSuchRoute /> 
            </Switch>
           

}

import { Button } from "apprise-frontend/components/Button"
import { Drawer } from "apprise-frontend/components/Drawer"
import { useConfig } from "apprise-frontend/config/state"
import { Form } from "apprise-frontend/form/Form"
import { FormState, useFormState } from "apprise-frontend/form/hooks"
import { NoteBox } from "apprise-frontend/form/NoteBox"
import { SimpleBox } from "apprise-frontend/form/SimpleBox"
import { Switch } from "apprise-frontend/form/Switch"
import { VSelectBox } from "apprise-frontend/form/VSelectBox"
import { icns } from "apprise-frontend/icons"
import { LayoutProvider } from 'apprise-frontend/layout/LayoutProvider'
import { instanceFrom } from 'apprise-frontend/layout/model'
import { paramIcon } from 'apprise-frontend/layout/parameters/constants'
import { useLocale } from "apprise-frontend/model/hooks"
import { Tab } from 'apprise-frontend/scaffold/Tab'
import { Topbar } from "apprise-frontend/scaffold/Topbar"
import { BytestreamedContext } from 'apprise-frontend/stream/BytestreamedHelper'
import { TagBoxset } from "apprise-frontend/tag/TagBoxset"
import { TagRefBox } from "apprise-frontend/tag/TagRefBox"
import { AudienceList } from "apprise-frontend/tenant/AudienceList"
import { tenantIcon, tenantType } from "apprise-frontend/tenant/constants"
import { useUsers } from "apprise-frontend/user/api"
import { userType } from 'apprise-frontend/user/constants'
import { through } from "apprise-frontend/utils/common"
import { paramsInQuery } from 'apprise-frontend/utils/routes'
import { AssetOverlayForm } from '#campaign/AssetOverlayForm'
import { EventList } from "#campaign/event/EventList"
import { useEventInstances } from "emaris-frontend/campaign/event/api"
import { EventInstance } from 'emaris-frontend/campaign/event/model'
import { useRequirementInstances } from "emaris-frontend/campaign/requirement/api"
import { RequirementInstanceLabel } from "emaris-frontend/campaign/requirement/Label"
import { RequirementInstance } from "emaris-frontend/campaign/requirement/model"
import { submissionType } from "emaris-frontend/campaign/submission/constants"
import { useDashboards } from "emaris-frontend/dashboard/api"
import { predefinedRequirementParameterId } from 'emaris-frontend/layout/parameters/constants'
import { useRequirements } from "emaris-frontend/requirement/api"
import { requirementType } from "emaris-frontend/requirement/constants"
import { RequirementDetailLoader } from 'emaris-frontend/requirement/DetailLoader'
import { RequirementLabel } from "emaris-frontend/requirement/Label"
import { Requirement } from "emaris-frontend/requirement/model"
import * as React from "react"
import { useTranslation } from "react-i18next"
import { useHistory, useLocation } from "react-router-dom"
import { useCurrentCampaign } from "../../lib/emaris-frontend/campaign/hooks"



type Props = {

    detail: RequirementInstance
    onClose: () => void

    width?: number
}


export const RequirementInstanceDetail = (props: Props) => {

    const { detail } = props

    return <RequirementDetailLoader id={detail.source}>{

        () => <InnerRequirementInstanceDetail {...props} />

    }</RequirementDetailLoader>
}

const InnerRequirementInstanceDetail = (props: Props) => {

    const { t } = useTranslation()
    const { l } = useLocale()
    const history = useHistory()
    const { search } = useLocation()

    const campaign = useCurrentCampaign()

    const config = useConfig()
    const dashboard = useDashboards()

    const { width = 650, detail, onClose } = props

    const requirements = useRequirementInstances(campaign)
    const events = useEventInstances(campaign)

    const sources = useRequirements()
    // existing events about this detail, including temporals.
    const initialRelatedEvents = events.allAboutWithTemporals(detail.source)

    type FormData = { instance: RequirementInstance, relatedEvents: EventInstance[] }

    const formstate = useFormState<FormData>({ instance: detail, relatedEvents: initialRelatedEvents })

    const { edited,  initial, dirty, change, reset, set, softReset } = formstate

    // resyncs on external changes (push events)
    // eslint-disable-next-line
    React.useEffect(() => set({ ...edited, relatedEvents: initialRelatedEvents }), [events.all()])
    
    const { instance, relatedEvents } = formstate.edited

    const onScreenEvents = dirty ? relatedEvents : [...relatedEvents].sort(events.temporalAwareDateComparatorOver(relatedEvents))

    const source = sources.safeLookup(edited.instance.source)!

    const liveBtn = <Button enabledOnReadOnly iconLeft icn={icns.edit} onClick={() => history.push(dashboard.given(campaign).routeToAsset(edited.instance))}>{t("campaign.labels.campaign_view.live")}</Button>

    const onSave = () => events.replaceAllAbout(onScreenEvents, detail.source)
        .then(through(() => requirements.save(instance, initial.instance)))
        .then(relatedEvents => softReset({ ...edited, relatedEvents }))

    const revert = <Button icn={icns.revert} enabled={dirty} onClick={() => reset()}>{t("common.buttons.revert")}</Button>
    const saveact = <Button icn={icns.save} enabled={dirty} type="primary" onClick={onSave}>{t("common.buttons.save")}</Button>

    const report = requirements.validateInstance(instance)

    const { tab } = paramsInQuery(search)

    const tabcompo = (() => {

        switch (tab) {

            case 'overlay': return (
                
                <BytestreamedContext>
                    <LayoutProvider for={edited.instance.id} layoutConfig={{ type: instance.instanceType }} layout={instanceFrom(source.properties.layout)} onChange={() => {/* no real change intended here, only overlays. */ }}>
                        <AssetOverlayForm
                            instance={edited.instance}
                            excludes={p => predefinedRequirementParameterId.includes(p.id)}
                            onChange={change((t, v) => t.instance = v)} />
                    </LayoutProvider>
                </BytestreamedContext>

            )

            default: return <GeneralForm report={report} {...formstate} />
        }
    })()

    return <Drawer readonly={!edited.instance.source} renderAfterTransition routeId={requirements.detailParam()} visible width={width} warnOnClose={dirty} title={t("common.buttons.edit_one", { singular: l(source?.name) })} icon={tenantIcon} onClose={onClose}>

        <Topbar offset={63}>

            <Tab default id={"info"} icon={icns.form} name={t("common.labels.general")} badge={report.errors() > 0} />
            <Tab id={'overlay'} icon={paramIcon} name={t('layout.parameters.plural')} />

            {saveact}
            {revert}
            {config.get().routedTypes?.includes(submissionType) && liveBtn}

        </Topbar>


        {tabcompo}

    </Drawer>
}

type FormProps = FormState<{ instance: RequirementInstance, relatedEvents: EventInstance[] }> & {

    report: any
}

const GeneralForm = (props: FormProps) => {


    const { edited, change, report, dirty, initial } = props

    const { instance, relatedEvents } = edited

    const { t } = useTranslation()

    const { l } = useLocale()

    const { logged } = useUsers();

    const campaign = useCurrentCampaign()

    const sources = useRequirements()

    const source = sources.safeLookup(edited.instance.source)!

    const requirements = useRequirementInstances(campaign)

    const events = useEventInstances(campaign)

    const onScreenEvents = dirty ? relatedEvents : [...relatedEvents].sort(events.temporalAwareDateComparatorOver(relatedEvents))


    // bizrule: an instance can be "re-targeted", associated with a later version of its source,
    // provided that version is selected for this campaign. 
    // this is typically required when the campaign is cloned from an older one whilst requirements have been superseeded.

    const isViableDescendant = (r: Requirement) => r.lineage?.includes(initial.instance.source) && !requirements.all().map(i => i.source).includes(r.id)
   
    const descendants = sources.all().filter(r => r.id === initial.instance.source || isViableDescendant(r))


    return <Form state={props} >

        <EventList instance={instance} events={onScreenEvents} onChange={change((e, newevents) => { e.relatedEvents = newevents })} />

        <TagRefBox mode="multi" label={t("common.fields.audience.name")} validation={report.audience} expression={edited.instance.audience} type={tenantType} onChange={change((t, v) => t.instance.audience = v)} />

        <AudienceList label={t("common.fields.audience_list.name")}  validation={report.audienceList} onChange={change((t,v) => t.instance.audienceList = v)}>{edited.instance.audienceList}</AudienceList>

        <TagRefBox mode="multi" label={t("common.fields.user_profile.name")} validation={report.userProfile} expression={edited.instance.userProfile} type={userType} onChange={change((t, v) => t.instance.userProfile = v)} />

        <TagBoxset edited={instance.tags} type={requirementType} validation={report} onChange={change((t, v) => t.instance.tags = v)} />

        <NoteBox enabledOnReadOnly label={t("common.fields.note.name")} validation={report.note} autoSize={{ minRows: 4, maxRows: 6 }} onChange={change((t, v) => t.instance.properties.note = t.instance.properties.note ? { ...t.instance.properties.note, [logged.tenant]: v } : { [logged.tenant]: v })} >
            {instance.properties.note}
        </NoteBox>


        {descendants.length > 1 ?

            <VSelectBox label={t("campaign.fields.source.name")}
                validation={report.source_editable}
                lblTxt={r => l(r.name)}
                options={descendants}
                onChange={change((t, v) => t.instance.source = v.id)}
                renderOption={r => <RequirementLabel noLink requirement={r} />}
                optionId={r => r.id}>

                {[source]}

            </VSelectBox>

            :

            <SimpleBox label={t("campaign.fields.source.name")} validation={report.source_readonly} >
                <RequirementLabel requirement={instance.source} />
            </SimpleBox>

        }

        {instance.lineage &&

            <SimpleBox label={t("common.fields.lineage.name")} validation={report.lineage} >
                <RequirementInstanceLabel showCampaign instance={instance.lineage} />
            </SimpleBox>

        }

        <Switch label={t("common.fields.editable.name")} onChange={change((u,v) => u.instance.properties.editable=v)} validation={report.editable}>
            {edited.instance.properties.editable !== undefined ? edited.instance.properties.editable : true}
        </Switch>

        <Switch label={t("common.fields.versionable.name")} onChange={change((u,v) => u.instance.properties.versionable=v)} validation={report.versionable}>
            {edited.instance.properties.versionable !== undefined ? edited.instance.properties.versionable : true}
        </Switch>

        {
        campaign.properties.complianceScale &&
            <Switch label={t("common.fields.assessed.name")} onChange={change((u,v) => u.instance.properties.assessed=v)} validation={report.assessed}>
                {edited.instance.properties.assessed !== undefined ? edited.instance.properties.assessed : true}
            </Switch>
        }


    </Form>


}

import { useListState } from "apprise-frontend/components/hooks";
import { Label } from "apprise-frontend/components/Label";
import { NoSuchRoute } from "apprise-frontend/components/NoSuchRoute";
import { Column, VirtualTable } from "apprise-frontend/components/VirtualTable";
import { useLocale } from "apprise-frontend/model/hooks";
import { useTags } from "apprise-frontend/tag/api";
import { useTagFilter, useTagHolderFilter } from 'apprise-frontend/tag/filter';
import { TagList } from "apprise-frontend/tag/Label";
import { tagRefsIn } from "apprise-frontend/tag/model";
import { tenantPlural } from "apprise-frontend/tenant/constants";
import { userPlural } from 'apprise-frontend/user/constants';
import { useEventInstances } from "emaris-frontend/campaign/event/api";
import { useRequirementInstances } from "emaris-frontend/campaign/requirement/api";
import { RequirementInstanceLabel } from "emaris-frontend/campaign/requirement/Label";
import { RequirementInstance } from "emaris-frontend/campaign/requirement/model";
import { eventIcon, eventPlural } from "emaris-frontend/event/constants";
import { useRequirements } from "emaris-frontend/requirement/api";
import { requirementIcon, requirementPlural, requirementType } from "emaris-frontend/requirement/constants";
import { RequirementLabel } from "emaris-frontend/requirement/Label";
import { Requirement } from "emaris-frontend/requirement/model";
import { Button, ButtonGroup } from 'apprise-frontend/components/Button';
import { icns } from 'apprise-frontend/icons';
import { CampaignValidation } from 'emaris-frontend/campaign/validation';
import * as React from "react";
import { useTranslation } from "react-i18next";
import { useHistory } from "react-router-dom";
import { useCurrentCampaign } from '../../lib/emaris-frontend/campaign/hooks';
import { usePicker } from "../Picker";
import '../styles.scss';
import { RequirementInstanceDetail } from "./Detail";

type Props = {

    report: ReturnType<CampaignValidation['validateRequirements']>
}

export const RequirementInstanceList = (props: Props) => {

    const campaign = useCurrentCampaign()

    const instances = useRequirementInstances(campaign)

    const instanceId = instances.detailInRoute()

    const detail = instances.lookup(instanceId)

    return instanceId && !detail ? <NoSuchRoute /> : <InnerList {...props} detail={detail!} />
}

const InnerList = (props: Props & { detail: RequirementInstance }) => {


    const { t } = useTranslation()
    const { l } = useLocale()

    const history = useHistory()

    const tags = useTags()
    const requirements = useRequirements()

    const campaign = useCurrentCampaign()
    const eventInstances = useEventInstances(campaign)

    const readOnly = campaign.guarded

    const { report, detail } = props

    const { Picker, pickerVisible, setPickerVisible } = usePicker()

    const instances = useRequirementInstances(campaign)

    const liststate = useListState<RequirementInstance>()

    const { selected, resetSelected, removeOne } = liststate

    const plural = t(requirementPlural)

    const editBtn = (p: RequirementInstance) => <Button
        key="edit"
        onClick={() => history.push(instances.route(p))} >
        {t("common.buttons.open")}
    </Button>
    
    const removeBtn = (p: RequirementInstance) => <Button
        key="remove"
        disabled={readOnly}
        onClick={() => instances.remove(p, removeOne)}>
        {t("common.buttons.remove")}
    </Button>

    const addBtn = <Button
        icn={icns.add}
        type="primary"
        disabled={readOnly}
        onClick={() => setPickerVisible(true)}>
        {t("common.buttons.add_many", { plural })}
    </Button>

    const removeSelectedBtn = <Button
        type="danger"
        disabled={readOnly}
        enabled={selected.length >= 1}
        onClick={() => instances.removeMany(selected, resetSelected)}>
        {t("common.buttons.remove_many", { count: selected.length })}
    </Button>

    const unfilteredData = instances.all()

   // eslint-disable-next-line
    const sortedData = React.useMemo(() => instances.allSorted(), [unfilteredData])

    const requirementinstanceGroup = `${campaign.id}-${requirementType}`

    const { TagFilter, tagFilteredData } = useTagFilter({
        filtered: sortedData,
        group: requirementinstanceGroup,
        key: `tags`
    })

    const { TagFilter: AudienceFilter, tagFilteredData: audienceFilteredData } = useTagHolderFilter({
        filtered: tagFilteredData,
        tagged: unfilteredData,
        tagsOf: t => t.audience?.terms.flatMap(t => t.tags),
        placeholder: t("common.fields.audience.name"),
        group: requirementinstanceGroup,
        key: `audience`
    })

    const {TagFilter:UserProfileFilter,tagFilteredData: userProfileFilteredData} = useTagHolderFilter ({
        filtered:audienceFilteredData, 
        tagged: unfilteredData,
        tagsOf: t=>t.userProfile?.terms.flatMap(t => t.tags),
        placeholder:t("common.fields.user_profile.name"),
        group: requirementinstanceGroup,
        key: `profile`
    })

    const data = userProfileFilteredData

    const rowValidation = ({ rowData: requirement }) => report[requirement.id].status === "error" && icns.error(report[requirement.id].msg)

    const unfilteredTemplates = requirements.all()

    // eslint-disable-next-line
    const activeTemplates = React.useMemo(() => requirements.allSorted().filter(r => r.lifecycle.state !== 'inactive'), [unfilteredTemplates])


    const eventCountLabel = (ri: RequirementInstance) => {

        const thisEventInstances = eventInstances.allAboutWithTemporals(ri.source)
        const someMissing = thisEventInstances.some(ei => !ei.date)

        return <Label
            icon={eventIcon}
            title={thisEventInstances.length}
            className={someMissing ? 'event-warn' : ''}
            tipPlacement='topRight'
            tip={() => <span>{t("event.no_date_set")}</span>}
            noTip={!someMissing} />
    }

    return (<>
        <VirtualTable id="reqinst" debug data={data} total={sortedData.length} state={liststate} selectAll
            filters={[UserProfileFilter, AudienceFilter, TagFilter]}
            decorations={[<ButtonGroup>{addBtn}{removeSelectedBtn}</ButtonGroup>]}
            filterGroup={requirementinstanceGroup} filterBy={instances.stringify} filterPlaceholder={t("common.components.table.filter_placeholder", { plural: plural.toLowerCase() })}
            onDoubleClick={p => history.push(instances.route(p))}
            refreshFlag={selected}
            actions={p => [editBtn(p), removeBtn(p)]}>

            <Column<RequirementInstance> flexGrow={1} title={t("common.fields.name_multi.name")} dataKey="name" decorations={[rowValidation]}
                dataGetter={r => l(requirements.safeLookup(r.source)!.name)}
                cellRenderer={({ rowData: r }) => <RequirementInstanceLabel tipTitle noLineage noMemo lineage instance={r} />} />

            <Column<RequirementInstance> sortable={false} title={t("common.fields.audience.name")} dataKey="audience" dataGetter={(p: RequirementInstance) =>{
                const audienceTerms = (p.audience?.terms.length ?? 0) === 0
                const audienceListIncludes = (p.audienceList?.includes.length ?? 0) === 0
                const audienceListExcludes = (p.audienceList?.excludes.length ?? 0) === 0
                return (audienceTerms && audienceListIncludes && audienceListExcludes) ? <Label title={ t(tenantPlural) } /> : <Label title={t("common.labels.restricted")} />

            }
            } />

            <Column<RequirementInstance> sortable={false} title={t("common.fields.user_profile.name")} dataKey="userprofile" dataGetter={(p: RequirementInstance) =>

                (p.userProfile?.terms.length ?? 0) > 0 ? <TagList taglist={tagRefsIn(p.userProfile)} /> : <Label noIcon title={t(userPlural)} />
            } />

            {tags.allTagsOf(requirementType).length > 0 &&
                <Column<RequirementInstance> flexGrow={2} sortable={false} title={t("common.fields.tags.name")} dataKey="tags"
                    dataGetter={i => i.tags} cellRenderer={({ rowData: ri }) => <TagList truncateEllipsisLink={instances.route(ri)} taglist={ri.tags} />}
                />
            }

            <Column<RequirementInstance> width={60} sortable={false} title={t(eventPlural)} dataKey="source"
                dataGetter={i => i.source} cellRenderer={({ rowData: ri }) => eventCountLabel(ri)} />

        </VirtualTable>


        {
            pickerVisible &&

            <Picker id='reqinst' title={plural} icon={requirementIcon}
                filterGroup={`${requirementinstanceGroup}-picker`}
                resources={activeTemplates}
                selected={sortedData.map(i => i.source)}
                onSelection={added => Promise.resolve(instances.addAll(added.map(instances.generate as any)))}>

                <Column title={t("common.fields.name_multi.name")} flexGrow={1} dataKey="name" dataGetter={(r: Requirement) => l(r.name)} cellRenderer={cell => <RequirementLabel noLink tipTitle lineage requirement={cell.rowData} />} />
                <Column title={t("common.fields.tags.name")} dataKey="tags" dataGetter={(r: Requirement) => <TagList truncateEllipsisLink={requirements.routeTo(r)} taglist={r.tags} />} flexGrow={1} />

            </Picker>

        }

        { detail &&

            <RequirementInstanceDetail detail={detail} onClose={() => history.push(instances.route())} />}


    </>
    )
}
import { CardlistBox } from "apprise-frontend/components/CardlistBox"
import { useEventInstances } from "emaris-frontend/campaign/event/api"
import { EventInstance, isTemporal } from "emaris-frontend/campaign/event/model"
import { useCurrentCampaign } from "emaris-frontend/campaign/hooks"
import { CampaignInstance } from "emaris-frontend/campaign/model"
import { eventPlural, eventSingular } from "emaris-frontend/event/constants"
import { Event } from "emaris-frontend/event/model"
import produce from "immer"
import * as React from "react"
import { useTranslation } from "react-i18next"
import { EventCard } from "./Card"

type EventListProps = {

    instance: CampaignInstance

    events: EventInstance[]
    onChange: (_:EventInstance[]) => void
}

export const EventList = (props:EventListProps) => {

    const {t} = useTranslation()

    const campaign = useCurrentCampaign()

    const instances = useEventInstances(campaign)

    const {instance,events,onChange} = props

    const remainingTemplates = (ei?:EventInstance)=> {

        // removes the current event so the choice can be changed on new entries.
        const pool = ei ? events.filter(i=>i.id!==ei.id) : events

        // for temporal, list possibilities for the target event; otherwise, show possibility about the instance.
        return isTemporal(ei) ? instances.possibleTemplatesForInstance(ei,pool) :instances.possibleTemplatesForInstance(instance,pool);
    
    }


    const remainingTemporals = (ei:EventInstance) => {

        return instances.possibleTemplatesForInstance(ei,events) 
    }

    const newInstance =  (template:Event,target:string)=> instances.generate(template, target)

     // uses immer to change events and notifies client of the changes. 
    const change = (updater:(_:EventInstance[])=>void) => onChange(produce(events,updater))

    //  helper functions
    const add = (ei: EventInstance) => change(events=>{events.push(ei)})
    const addEvent = () => add(newInstance(remainingTemplates()[0],instance.source))
    const addTemporalFor = (ei:EventInstance) => add(newInstance(remainingTemporals(ei)[0],ei.id))


    const removeWithTemporals = (index:number) =>change(events=>{
       
        const removed = events.splice(index,1)[0]

        var next

        while ((next = events.findIndex(e=>e.target===removed.id)) > -1)
           events.splice(next,1) 
        
    
    })

    const cards = events.map((ei,index) => {
        
            return <EventCard key={`${ei.id??'new'}-${index}`} isNew={!instances.lookup(ei.id)} 
                                                    
                                                    templates = {remainingTemplates(ei)}
                                                    
                                                    instance={ei} 
                                                    context={events}

                                                    onChange={i=>change(events=>{events[index]=i})}
                                                    

                                                    // if we're editing a temporal, the target of relative dates is the temporal's target. 
                                                    // otherwise it's the source of the instance under editing.
                                                    relativeDateTarget={isTemporal(ei) ? ei.target : undefined }

                                                    onAddTemporal={ remainingTemporals(ei).length > 0 ? ()=>addTemporalFor(ei) : undefined }
                                            />})

                        
    return  <CardlistBox className="event-list" id={instance.id} singleLabel={t(eventSingular)} label={t(eventPlural)} 
                           
                        addIf={remainingTemplates().length>0}
                        
                        onAdd={addEvent}

                        onRemove={removeWithTemporals} >

                 {cards}

             </CardlistBox>

}
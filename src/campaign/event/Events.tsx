import { NoSuchRoute } from "apprise-frontend/components/NoSuchRoute";
import { eventType } from "emaris-frontend/event/constants";
import { campaignRoute } from "emaris-frontend/campaign/constants";
import { CampaignValidation } from "emaris-frontend/campaign/validation";
import * as React from "react";
import { Route, Switch } from "react-router-dom";
import { EventInstanceList } from "./List";


type Props = {

    report: ReturnType<CampaignValidation['validateEvents']>
}


export const EventInstances = (props:Props)=> 

            <Switch>
                <Route exact path={`${campaignRoute}/:name/${eventType}`} render={()=><EventInstanceList {...props}/>} />
                <NoSuchRoute /> 
            </Switch>
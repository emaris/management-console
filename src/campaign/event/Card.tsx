
import { Button } from "apprise-frontend/components/Button"
import { SelectBox } from "apprise-frontend/form/SelectBox"
import { SimpleBox } from "apprise-frontend/form/SimpleBox"
import { icns } from "apprise-frontend/icons"
import { useEventInstances } from "emaris-frontend/campaign/event/api"
import { DateBox } from "emaris-frontend/campaign/event/DateBox"
import { EventInstanceLabel } from "emaris-frontend/campaign/event/Label"
import { EventInstance, isTemporal } from "emaris-frontend/campaign/event/model"
import { useCurrentCampaign } from "emaris-frontend/campaign/hooks"
import { eventSingular } from "emaris-frontend/event/constants"
import { EventLabel } from "emaris-frontend/event/Label"
import { Event } from "emaris-frontend/event/model"
import { useTranslation } from "react-i18next"
import "./styles.scss"

type Props = {

    
    templates?: Event[]
    
    instance: EventInstance

    //  the event co-edited along with this one.
    //  passed to DateBox as an overlay to resolve dates relatively to values not yet stored.
    context: EventInstance[]

    relativeDateTarget: string | undefined
    isNew?: boolean

    onAddTemporal?: () => void

    onChange: (_:EventInstance) => void

}

export const EventCard = (props:Props) => {

    const {t} = useTranslation()

    const campaign = useCurrentCampaign()

    const instances = useEventInstances(campaign)

    const {instance,context,isNew,onChange,relativeDateTarget,onAddTemporal,templates=[]} = props

    const report = () => instances.validateInstance(instance,instances.validationProfile())

    const contents =     <div>        
                                    { isNew && templates.length>1 ? 
                                    
                                    <SelectBox<string> standalone

                                        placeholder={`${t("common.buttons.select_one",{singular:t(eventSingular)}).toLowerCase()}...`} 
                                        getlbl={c=><EventLabel noLink event={c}/>}
                                        selectedKey={ instance.source } 
                                        onChange={id=> onChange({...instance,source:id}) }>
                                        
                                        { templates.map(t=>t.id) }

                                    </SelectBox>

                                    :

                                    <SimpleBox light>
                                        <EventInstanceLabel noLink noDate instance={instance} />
                                    </SimpleBox>
                                }

                                <div className="card-date"> 
                    
                                    <DateBox validation={{...report().date,msg:undefined}}
                                            fixedTarget={relativeDateTarget} 
                                            event={instance} 
                                            overlay={context}
                                            showTime
                                            onChange={date=>onChange({...instance,date})} />
                                    
                                </div>

                            </div>

    return <div className={`event-card ${isTemporal(instance) ? 'temporal-card':''} `}>
                <div className={`card-name`}>
                    { contents }
                                
                    {onAddTemporal && 
                                
                                <div className="card-add-related" >
                                        <Button iconLeft icn={icns.add} size="small" onClick={()=>onAddTemporal()}>{t('campaign.buttons.add_related')}</Button>
                                </div>
                    }
                </div>
             </div>
}

import { Placeholder } from "apprise-frontend/components/Placeholder"
import { useAsyncRender } from "apprise-frontend/utils/hooks"
import { useEventInstances } from "emaris-frontend/campaign/event/api"
import * as React from "react"
import { useCurrentCampaign } from "../../lib/emaris-frontend/campaign/hooks"


type Props = React.PropsWithChildren<{}>


export const EventInstanceLoader = (props:Props) => {

    const campaign = useCurrentCampaign()

    const {fetchAll,areReady} = useEventInstances(campaign)

    const [render] = useAsyncRender({when:areReady(),task:fetchAll,content:props.children,placeholder:Placeholder.list})
 
    return render
  
  }
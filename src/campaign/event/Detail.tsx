
import { Button } from "apprise-frontend/components/Button"
import { Drawer } from "apprise-frontend/components/Drawer"
import { useConfig } from 'apprise-frontend/config/state'
import { Form } from "apprise-frontend/form/Form"
import { useFormState } from "apprise-frontend/form/hooks"
import { NoteBox } from "apprise-frontend/form/NoteBox"
import { SimpleBox } from "apprise-frontend/form/SimpleBox"
import { Switch } from "apprise-frontend/form/Switch"
import { VSelectBox } from "apprise-frontend/form/VSelectBox"
import { icns } from "apprise-frontend/icons"
import { useLocale } from "apprise-frontend/model/hooks"
import { Topbar } from "apprise-frontend/scaffold/Topbar"
import { TagBoxset } from "apprise-frontend/tag/TagBoxset"
import { tenantIcon } from "apprise-frontend/tenant/constants"
import { useUsers } from "apprise-frontend/user/api"
import { useCampaigns } from "emaris-frontend/campaign/api"
import { campaignType } from "emaris-frontend/campaign/constants"
import { useEventInstances } from "emaris-frontend/campaign/event/api"
import { DateBox } from "emaris-frontend/campaign/event/DateBox"
import { EventInstanceLabel } from "emaris-frontend/campaign/event/Label"
import { EventInstance, isRelative } from "emaris-frontend/campaign/event/model"
import { useEvents } from "emaris-frontend/event/api"
import { eventType } from "emaris-frontend/event/constants"
import { EventLabel } from "emaris-frontend/event/Label"
import { useProducts } from "emaris-frontend/product/api"
import { productType } from "emaris-frontend/product/constants"
import { useRequirements } from "emaris-frontend/requirement/api"
import { requirementType } from "emaris-frontend/requirement/constants"
import moment from 'moment'
import { useTranslation } from "react-i18next"
import { useCurrentCampaign } from "../../lib/emaris-frontend/campaign/hooks"




type Props = {

    detail: EventInstance
    onClose: () => void

    width?: number
}

export const EventInstanceDetail = (props: Props) => {

    const { t } = useTranslation()
    const { l } = useLocale()

    const { width = 700, detail, onClose } = props

    const campaign = useCurrentCampaign()
    const templates = useEvents()

    const readOnly = campaign.guarded

    const instances = useEventInstances(campaign)

    const config = useConfig()

    const { logged } = useUsers();

    const event = templates.lookup(detail.source)!
    const events = useEvents()
    const campaigns = useCampaigns()
    const products = useProducts()
    const requirements = useRequirements()

    const formstate = useFormState(detail)

    const { edited, dirty, initial, change, reset } = formstate

    const onSave = () => instances.save(edited).then(onClose)

    const report = instances.validateInstance(edited, instances.validationProfile())

    const revert = <Button icn={icns.revert} enabled={dirty} onClick={() => reset()}>{t("common.buttons.revert")}</Button>
    const saveact = <Button icn={icns.save} disabled={false && report.errors() > 0} enabled={dirty} type="primary" onClick={onSave}>{t("common.buttons.save")}</Button>
    const removeact =
        <Button
            key="remove"
            type="danger"
            disabled={readOnly}
            icn={icns.remove}
            onClick={() => instances.remove(edited, onClose, true)}>
            {t("common.buttons.remove")}
        </Button>

    const profile = edited.type && instances.given(edited)

    return <Drawer renderAfterTransition routeId={instances.detailParam()} visible width={width} warnOnClose={dirty} title={`${l(event?.name!)}`} icon={tenantIcon} onClose={onClose}>

        <Topbar autoGroupButtons={false} offset={63}>
            {removeact}
            {saveact}
            {revert}
        </Topbar>

        <Form state={formstate} >

            {profile &&

                <VSelectBox label={t("common.fields.target.name")} validation={report.target}
                    options={instances.possibleTargetsFor(event)}
                    renderOption={id => profile.optionOf(id)}
                    lblTxt={e => {
                        const resolved = instances.lookup(e)!
                        const event = l(events.lookup(resolved.source)!.name)
                        let target = ""

                        switch (resolved.type) {
                            case campaignType: target = l(campaigns.lookup(resolved.target)?.name); break;
                            case requirementType: target = l(requirements.lookup(resolved.target)?.name); break;
                            case productType: target = l(products.lookup(resolved.target)?.name); break;
                            default: target = ""; break;
                        }

                        return `${event} ${target}`
                    }}
                    onChange={change((t, id) => {

                        const currentTarget = t.target

                        t.target = id

                        // if there's a date relative to same target, syncs the change.
                        if (isRelative(t.date) && (t.date.target === currentTarget || !t.date.target))
                            t.date.target = id

                    })}>

                    {[edited.target]}

                </VSelectBox>
            }


            <DateBox label={t("common.fields.date.name")}
                validation={report.date}
                fixedTarget={edited.type === eventType ? edited.target : undefined}
                event={edited}
                showTime
                onChange={change((t, v) => t.date = v)} />


            <TagBoxset edited={edited.tags} type={eventType} validation={report} onChange={change((t, v) => t.tags = v)} />

            <NoteBox label={t("common.fields.note.name")} validation={report.note} autoSize={{ minRows: 4, maxRows: 6 }} onChange={change((t, v) => t.properties.note = t.properties.note ? { ...t.properties.note, [logged.tenant]: v } : { [logged.tenant]: v })} >
                {edited.properties.note}
            </NoteBox>

            <SimpleBox label={t("campaign.fields.source.name")} validation={report.source} >
                <EventLabel event={edited.source} />
            </SimpleBox>

            <Switch label={t("event.fields.relatable.name")} onChange={
                change((u, v) => u.properties.relatable = v ? v : initial.properties?.relatable)
            }
                validation={report.relatable}>
                {edited.properties.relatable}
            </Switch>


            <Switch disabled ={ !config.devmode() && !!initial.notifiedOn && moment(instances.absolute(initial.date)).isBefore(moment.now())} label={t("event.fields.notified.name")} validation={report.notified} onChange={
                change((u, v) => u.notifiedOn = v ? initial.notifiedOn ?? moment().format() : undefined) 
            }>
                {edited.notifiedOn != null}
            </Switch>


            {edited.lineage &&

                <SimpleBox label={t("common.fields.lineage.name")} validation={report.lineage} >
                    <EventInstanceLabel showCampaign instance={edited.lineage} />
                </SimpleBox>
            }

        </Form>

    </Drawer>
}

import { Icon, Tooltip } from "antd";
import { useListState } from "apprise-frontend/components/hooks";
import { Label } from 'apprise-frontend/components/Label';
import { NoSuchRoute } from "apprise-frontend/components/NoSuchRoute";
import { OptionMenu } from 'apprise-frontend/components/OptionMenu';
import { Column, VirtualTable } from "apprise-frontend/components/VirtualTable";
import { SelectBox } from "apprise-frontend/form/SelectBox";
import { icns } from "apprise-frontend/icons";
import { useLocale } from "apprise-frontend/model/hooks";
import { TagList } from "apprise-frontend/tag/Label";
import { TimeLabel } from "apprise-frontend/time/Label";
import { compareDates, group } from "apprise-frontend/utils/common";
import { useFilterState } from 'apprise-frontend/utils/filter';
import { paramsInQuery, updateQuery } from "apprise-frontend/utils/routes";
import { useCampaigns } from "emaris-frontend/campaign/api";
import { campaignEndEvent, campaignIcon, campaignSingular, campaignStartEvent, campaignType } from "emaris-frontend/campaign/constants";
import { useEventInstances } from "emaris-frontend/campaign/event/api";
import { EventInstanceLabel } from "emaris-frontend/campaign/event/Label";
import { absoluteOf, EventInstance, EventInstanceDto, isAbsolute, isRelative } from "emaris-frontend/campaign/event/model";
import { useCurrentCampaign } from "emaris-frontend/campaign/hooks";
import { CampaignValidation } from "emaris-frontend/campaign/validation";
import { useEvents } from "emaris-frontend/event/api";
import { dateIcon, eventIcon, eventPlural, eventSingular, eventType, missingIcon, pendingIcon, pinnedDateIcon, recurringDateIcon, relativeIcon, singleUseDateIcon } from "emaris-frontend/event/constants";
import { EventLabel, EventTypeLabel } from "emaris-frontend/event/Label";
import { Event } from "emaris-frontend/event/model";
import { useProducts } from "emaris-frontend/product/api";
import { productIcon, productSingular, productType } from "emaris-frontend/product/constants";
import { useRequirements } from "emaris-frontend/requirement/api";
import { requirementIcon, requirementSingular, requirementType } from "emaris-frontend/requirement/constants";
import { Button, ButtonGroup } from 'apprise-frontend/components/Button';
import moment from "moment-timezone";
import * as React from "react";
import { useTranslation } from "react-i18next";
import { useHistory, useLocation } from "react-router-dom";
import { usePicker } from "../Picker";
import { EventInstanceDetail } from "./Detail";
import "./styles.scss";



type Props = {

    report: ReturnType<CampaignValidation['validateEvents']>
}


// const noFilter = "All"
const flatView = "flat"
const typedView = "typed"
const calendarView = "calendar"

const viewParam = "event-view"

const beforeStartOption = "beforeStart"
const afterEndOption = "afterEnd"
const pinnedOption = "pinned"
const recurringOption = "recurring"
const singleUseOption = "single"
// const dateOtherOption = "other"
const relativeOption = "relative"
const absoluteOption = "absolute"
const pendingOption = "pending"
const missingOption = "missing"

const pinnedDecoration = t => <Tooltip mouseEnterDelay={.8} title={t("campaign.date.pinned_tip")} placement="topRight"> {pinnedDateIcon}</Tooltip>
const recurringDecoration = t => <Tooltip mouseEnterDelay={.8} title={t("campaign.date.recurring_tip")} placement="topRight">{recurringDateIcon}</Tooltip>
const singleUseDecoration = t => <Tooltip mouseEnterDelay={.8} title={t("campaign.date.single_use_tip")} placement="topRight">{singleUseDateIcon}</Tooltip>
const relativeDecoration = (target) => <Tooltip placement='left' title={() => <EventInstanceLabel instance={target} />}><span className='relative-link'>{icns.link}</span></Tooltip>

export const EventInstanceList = (props: Props) => {

    const campaign = useCurrentCampaign()
    const instances = useEventInstances(campaign)
    const { lookup, detailInRoute } = instances

    const id = detailInRoute()

    if (id && !lookup(id as string))
        return <NoSuchRoute />

    return <InnerEventInstanceList {...props} />
}

const InnerEventInstanceList = (props: Props) => {

    const { t } = useTranslation()
    const { l } = useLocale()

    const history = useHistory()
    const { pathname, search } = useLocation()

    const templates = useEvents()
    const campaign = useCurrentCampaign()

    const readOnly = campaign.guarded

    const { report } = props

    const { Picker, pickerVisible, setPickerVisible } = usePicker()

    const instances = useEventInstances(campaign)

    const liststate = useListState<EventInstance>()

    const { selected, resetSelected, removeOne } = liststate

    const plural = t(eventPlural)

    const { [viewParam]: view = calendarView } = paramsInQuery(search)

    const id = instances.detailInRoute()

    const detail = instances.lookup(id as string)

    const requirements = useRequirements()
    const products = useProducts()
    const campaigns = useCampaigns()

    // const modules = [noFilter, ...templates.registeredModules()] as Module[]

    const addBtn =
        <Button
            icn={icns.add}
            type="primary"
            disabled={readOnly}
            onClick={() => setPickerVisible(true)}>
            {t("common.buttons.add_many", { plural })}
        </Button>

    const removeSelectedBtn =
        <Button
            type="danger"
            disabled={readOnly}
            enabled={selected.length >= 1}
            onClick={() => instances.removeMany(selected, resetSelected)}>
            {t("common.buttons.remove_many", { count: selected.length })}
        </Button>

    const editBtn = (e: EventInstance) =>
        <Button
            key="edit"
            onClick={() => history.push(instances.route(e))}>
            {t("common.buttons.open")}
        </Button>

    const removeBtn = (e: EventInstance) =>
        <Button
            key="remove"
            disabled={readOnly}
            onClick={() => instances.remove(e, removeOne)}>
            {t("common.buttons.remove")}
        </Button>



    // const viewSelectors = [flatView, typedView, calendarView]
    const viewSelectors = [typedView, calendarView]

    const viewSelector = <SelectBox enabledOnReadOnly fieldStyle={{ minWidth: 140 }} standalone onChange={key => history.push(`${pathname}?${updateQuery(search).with(params => params[viewParam] = key)}`)}
        getlbl={s => t(views[s].name)} selectedKey={view as string}>
        {viewSelectors}
    </SelectBox>

    const eventinstanceGroup = `${campaign.id}-${eventType}`    

    const ctx = useFilterState(eventinstanceGroup)

    const defaultSelectedTypes = React.useMemo(() => [campaignType, requirementType, productType, eventType], [])
    const selectedTypes = ctx.get(`type`) ?? defaultSelectedTypes

    const TypeFilters = <OptionMenu selected={selectedTypes} setSelected={ctx.set(`type`)} style={{ width: 180 }} placeholderIcon={icns.filter} placeholder={t("campaign.labels.filter_event_types")}>
        <OptionMenu.Option key={campaignType} value={campaignType} label={<Label icon={campaignIcon} title={t(campaignSingular)} />} />
        <OptionMenu.Option key={requirementType} value={requirementType} label={<Label icon={requirementIcon} title={t(requirementSingular)} />} />
        <OptionMenu.Option key={productType} value={productType} label={<Label icon={productIcon} title={t(productSingular)} />} />
        <OptionMenu.Option key={eventType} value={eventType} label={<Label icon={eventIcon} title={t(eventSingular)} />} />
    </OptionMenu>

    const typeFilter = React.useCallback((i: EventInstance) => i.type && selectedTypes.includes(i.type), [selectedTypes])



    const defaultDateTypes = React.useMemo(()=>[
        beforeStartOption,
        afterEndOption,
        pinnedOption,
        recurringOption,
        singleUseOption,
        absoluteOption,
        relativeOption,
        missingOption,
        pendingOption
    
    ],[])

    const selectedDateTypes = ctx.get(`date`) ?? defaultDateTypes

    const DateFilters = <OptionMenu selected={selectedDateTypes} setSelected={ctx.set(`date`)} style={{ width: 180 }} placeholderIcon={dateIcon} placeholder={t("campaign.labels.filter_event_dates")}>
        <OptionMenu.Option key={beforeStartOption} value={beforeStartOption} label={<Label icon={dateIcon} title={t("campaign.date.before_start")} />} />
        <OptionMenu.Option key={afterEndOption} value={afterEndOption} label={<Label icon={dateIcon} title={t("campaign.date.after_end")} />} />
        <OptionMenu.Divider />
        <OptionMenu.Option key={absoluteOption} value={absoluteOption} label={<Label icon={dateIcon} title={t("campaign.date.absolute")} />} />
        <OptionMenu.Option key={relativeOption} value={relativeOption} label={<Label icon={relativeIcon} title={t("campaign.date.relative")} />} />
        <OptionMenu.Divider />
        <OptionMenu.Option key={missingOption} value={missingOption} label={<Label icon={missingIcon} title={t('event.missing_date.missing')} />} />
        <OptionMenu.Option key={pendingOption} value={pendingOption} label={<Label icon={pendingIcon} title={t('event.missing_date.pending')} />} />
        <OptionMenu.Divider />
        <OptionMenu.Option key={pinnedOption} value={pinnedOption} label={<Label icon={pinnedDateIcon} title={t("campaign.date.pinned")} />} />
        <OptionMenu.Option key={recurringOption} value={recurringOption} label={<Label icon={recurringDateIcon} title={t("campaign.date.recurring")} />} />
        <OptionMenu.Option key={singleUseOption} value={singleUseOption} label={<Label icon={singleUseDateIcon} title={t("campaign.date.single_use")} />} />
    </OptionMenu>

    const start = instances.startDate()
    const end = instances.endDate()

    const dateFilter = React.useCallback((i: EventInstanceDto) => {

        const isPinned = selectedDateTypes.includes(pinnedOption) && i.date?.kind === 'absolute' && i.date.branchType === pinnedOption
        const isSingleUse = selectedDateTypes.includes(singleUseOption) && i.date?.kind === 'absolute' && i.date.branchType === singleUseOption
        const isRelative = selectedDateTypes.includes(relativeOption) && i.date?.kind === 'relative'
        const isAbsolute = selectedDateTypes.includes(absoluteOption) && (i.date === undefined || i.date.kind === 'absolute')
        const isBeforeStart = selectedDateTypes.includes(beforeStartOption) && start && moment(instances.absolute(i.date)).isBefore(start)
        const isAfterEnd = selectedDateTypes.includes(afterEndOption) && end && moment(instances.absolute(i.date)).isAfter(end)

        const isMissing = selectedDateTypes.includes(missingOption) && i.date === undefined
        const isPending = selectedDateTypes.includes(pendingOption) && i.date?.kind === 'relative' && instances.absolute(i.date) === undefined

        const isRecurring = selectedDateTypes.includes(recurringOption) && (i.date?.kind === 'absolute' && (i.date?.branchType === undefined || i.date.branchType === recurringOption))


        return isRecurring || isPinned || isSingleUse || isRelative || isAbsolute || isBeforeStart || isAfterEnd || isMissing || isPending

    }
        // eslint-disable-next-line                            
        , [selectedDateTypes])

    const rowValidation = ({ rowData: event }) => report[event.id]?.status === "error" && icns.error(report[event.id]?.msg)

    const directives = views[view as string]

    const unsortedInstances = instances.all()

    const typeFilteredInstances = React.useMemo(() => unsortedInstances.filter(typeFilter), [typeFilter, unsortedInstances])

    const dateFilteredInstances = React.useMemo(() => typeFilteredInstances.filter(dateFilter), [dateFilter, typeFilteredInstances])

    // all views have a default order based on dates, and this is preserved inside groups.
    const sortedInstances: EventInstanceDto[] = React.useMemo(

        () => dateFilteredInstances.sort((e1, e2) => instances.dateComparator(e1, e2, false))

        // eslint-disable-next-line
        , [dateFilteredInstances])


    const groups = React.useMemo(() =>

        group(sortedInstances).by(i => directives.groupBy(i, t, instances.absolute), directives.orderBy ?? ((i1, i2) => i1.name.localeCompare(i2.name)))

        // eslint-disable-next-line
        , [sortedInstances, directives])


    const grouped = React.useMemo(

        () => groups.length === 1 ? [{ title: groups[0].key, id: groups[0].key.name }, ...groups[0].group] : groups.flatMap(({ key, group }) => [{ id: key.name, title: key }, ...group])

        , [groups])

    const unfilteredTemplates = templates.all()

    // eslint-disable-next-line
    const suitableTemplates = React.useMemo(() => templates.allSorted().filter(r => r.lifecycle.state !== 'inactive').filter(instances.isPossibleTemplate), [unfilteredTemplates])

    const decorationsFor = (e: EventInstance) => {

        let filters = [] as JSX.Element[]

        if (isAbsolute(e.date)) {
            if (e.date.branchType === 'pinned')
                filters.push(pinnedDecoration(t))
            else if (e.date.branchType === 'single')
                filters.push(singleUseDecoration(t))
            else if (e.date.branchType === 'recurring' || e.date.branchType === undefined)
                filters.push(recurringDecoration(t))
        }
        else if (isRelative(e.date))
            filters.push(relativeDecoration(e.date.target))

        return filters
    }

    const lookupTarget = (target:string | undefined):string => {
        if (target === undefined) return ''

        const requirement = requirements.lookup(target)
        if (requirement !== undefined) return l(requirement.name)

        const product = products.lookup(target)
        if (product !== undefined) return l(product.name)

        const campaign = campaigns.lookup(target)
        if (campaign !== undefined) return l(campaign.name)

        return ''
    }

    return (<>

        <VirtualTable<any> key={view as string} rowKey="id" data={grouped} total={unsortedInstances.length + groups.length} state={liststate}

            filterGroup={eventinstanceGroup}
            filterWith={(f, i) => {
                let target = ''

                switch (i.type)  {

                    case campaignType : {
                        const c = campaigns.lookup(i.target)
                        target = c ? l(c.name) : ''
                        break
                    }
                    case productType : {
                        const p = products.lookup(i.target)
                        target = p ? l(p.name) : ''
                        break
                    }
                    case requirementType: {
                        const r = requirements.lookup(i.target)
                        target = r ? l(r.name) : ''
                        break
                    }
                    case eventType : {
                        const e = instances.lookup(i.target)
                        const lookedUpTarget = lookupTarget(e?.target)
                        const source = e ? templates.lookup(e.source) : undefined
                        target = source ? l(source.name) : ''
                        target = target + lookedUpTarget
                        break
                    }
                    default: {
                        target = ''
                        break
                    }
                }
                return i.title || instances.stringify(i).toLocaleLowerCase().includes(f.toLocaleLowerCase()) || target.toLowerCase().includes(f.toLowerCase())
            }}
            filterPlaceholder={t("common.components.table.filter_placeholder", { plural: plural.toLowerCase() })}
            filters={[TypeFilters, DateFilters]}

            

            countFilter={t => !!t?.campaign}

            decorations={[viewSelector, <ButtonGroup>{addBtn}{removeSelectedBtn}</ButtonGroup>]}

            rowClassName={({ rowData: e }) => (e.source === campaignStartEvent && absoluteOf(e.date)) ? 'start-row' : (e.source === campaignEndEvent && absoluteOf(e.date)) ? 'end-row' : ''}

            customProperties={({ rowData }) => ({ title: rowData.title })}
            customRowRenderer={InstanceRow}

            refreshFlag={selected}

            actions={e => [editBtn(e), removeBtn(e)]}>

            <Column<EventInstance> sortable={view === flatView} flexGrow={1} title={t("common.fields.name_multi.name")} decorations={[rowValidation]} dataKey="t.name"
                dataGetter={e => l(templates.lookup(e.source)?.name)}
                cellRenderer={({ rowData: e }) =>
                    <EventInstanceLabel noLineage noMemo noDate instance={e} />} />

            <Column<EventInstance> sortable={view === flatView} flexGrow={1} title={t("common.fields.target.name")} dataKey="t.target" dataGetter={e => instances.given(e).name()} cellRenderer={({ rowData: ei }) => instances.given(ei).labelOf(ei.target, { noDate: true })} />

            <Column<EventInstance> sortable={view === flatView} width={150} title={t("common.fields.date.name")} comparator={compareDates} dataKey="t.date"
                dataGetter={t => instances.absolute(t.date)}
                cellRenderer={({ rowData: e }) => <TimeLabel noMemo format="short" nullTime={e.date?.kind === 'relative' ? t('event.missing_date.pending') : t('event.missing_date.missing')} value={instances.absolute(e.date)} icon={icns.calendar} decorations={decorationsFor(e)} />
                } />

            <Column<EventInstance> sortable={view === flatView} width={90} title={t("common.fields.notified.name")} comparator={compareDates} dataKey="t.notified"
                dataGetter={t => t.notifiedOn}
                cellRenderer={({ rowData: e }) => {
                    if (e.notifiedOn) {

                        return <div className='notified-check'>
                                    <Tooltip title={<TimeLabel value={e.notifiedOn} noTip />}>
                                        <Icon type="check" className='notified-check-icon'/>
                                    </Tooltip>
                                </div>
                    
                    
                    } 
                    return undefined
                }}/>

        </VirtualTable>

        {pickerVisible &&

            <Picker id='events' title={plural} icon={eventIcon} noContext
                filterGroup={`${eventinstanceGroup}-picker`}
                resources={suitableTemplates}
                selected={[]}
                onSelection={ async added => {

                    const generated =added.map(a => instances.generate(a as Event))
                    
                    await instances.addAll(generated)

                    if (generated.length===1)    
                       return () => history.push(instances.route(generated[0]))   
                   
                   }}>

                <Column<Event> title={t("common.fields.name_multi.name")} flexGrow={1} dataKey="t.name.en" dataGetter={t => l(t.name)} cellRenderer={cell => <EventLabel noLink event={cell.rowData} />} />
                <Column<Event> width={150} title={t("common.fields.type.name")} dataKey="t.type" dataGetter={t => t.type} cellRenderer={cell => <EventTypeLabel type={cell.cellData} />} />
                <Column<Event> title={t("common.fields.tags.name")} dataKey="t.tags" dataGetter={t => <TagList taglist={t.tags} />} flexGrow={1} />

            </Picker>

        }

        {detail &&

            <EventInstanceDetail detail={detail} onClose={() => history.push(instances.route())} />

        }


    </>
    )
}

const InstanceRow = props => {

    const { title, ...rest } = props
    const { t } = useTranslation()

    if (!title)
        return <div {...rest} />

    const labelTitle = (title.year && title.month) ? moment().year(title.year).month(title.month).locale(moment.locale()).format(t("time.default_format_month_year")) : t(title.name)
    
    return <div {...rest} className={`${rest.className} event-table-title`}>
        <Label icon={title.icon} title={labelTitle} />
    </div>

}



const views = {

    [typedView]: { name: "campaign.labels.event_view.typed", groupBy: i => types[i.type ?? 'domain'] },

    [calendarView]: {
        name: "campaign.labels.event_view.calendar",
        orderBy: (t1, t2) => t1.year - t2.year || t1.month - t2.month,
        groupBy: (i, t, absolute) => {

            const date = absolute(i.date)

            if (!date)
                return { month: 0, year: 0, name: t('common.labels.unplanned'), icon: icns.calendar }

            const d = moment(new Date(date))

            return { month: d.month(), year: d.year(), name: `${t(`time.months.${d.month() + 1}`)} ${d.year()}`, icon: icns.calendar }

        }
    }
}

const types = {

    [requirementType]: { name: requirementSingular, icon: requirementIcon },
    [productType]: { name: productSingular, icon: productIcon },
    [campaignType]: { name: campaignSingular, icon: campaignIcon },
    [eventType]: { name: eventSingular, icon: eventIcon },

    0: { name: "common.labels.unplanned", icon: icns.calendar },
    1: { name: "time.months.1", icon: icns.calendar },
    2: { name: "time.months.2", icon: icns.calendar },
    3: { name: "time.months.3", icon: icns.calendar },
    4: { name: "time.months.4", icon: icns.calendar },
    5: { name: "time.months.5", icon: icns.calendar },
    6: { name: "time.months.6", icon: icns.calendar },
    7: { name: "time.months.7", icon: icns.calendar },
    8: { name: "time.months.8", icon: icns.calendar },
    9: { name: "time.months.9", icon: icns.calendar },
    10: { name: "time.months.10", icon: icns.calendar },
    11: { name: "time.months.11", icon: icns.calendar },
    12: { name: "time.months.12", icon: icns.calendar }

}
import { useSystem } from 'apprise-frontend/system/api'
import { useContextFilter } from 'apprise-frontend/system/filter'
import { useTagFilter } from 'apprise-frontend/tag/filter'
import { paramsInQuery, updateQuery } from 'apprise-frontend/utils/routes'
import { Tagged } from 'apprise-frontend/tag/model'
import * as React from 'react'
import { useTranslation } from 'react-i18next'
import { useHistory, useLocation } from 'react-router-dom'
import { Button } from 'apprise-frontend/components/Button'
import { Drawer } from 'apprise-frontend/components/Drawer'
import { useListState } from 'apprise-frontend/components/hooks'
import { Column, TableProps, VirtualTable } from 'apprise-frontend/components/VirtualTable'
import { childrenIn } from 'apprise-frontend/utils/children'


type ProxyProps<T extends Object> = React.PropsWithChildren< Partial<Omit<TableProps<T>,'data'|'filterGroup'>> & {


    title: string
    resourceIdProp?: string
    icon: JSX.Element
    width?: number

    resources: T[]
    selected: string[]

    filterGroup: string


    noContext?: boolean

    onSelection: (_: T[]) => any
   

}>

type Props<T extends Object> = ProxyProps<T> & {

    visible: boolean
    onClose:  () => void

}

const pickerParam="select-drawer"


export const usePicker = <T extends Tagged> () => {

    const { pathname, search } = useLocation()
    const history = useHistory()

    const pickerVisible = !!paramsInQuery(search)[pickerParam]

    // eslint-disable-next-line
    const setPickerVisible= React.useCallback((visible:boolean) => history.push(`${pathname}?${updateQuery(search).with(p=>p[pickerParam]= visible ? 'true' : null)}`),[pathname,search])

    const ProxyPicker = React.useCallback(( $: ProxyProps<T>) =>   <Picker visible={pickerVisible} onClose={()=> setPickerVisible(false)} {...$} /> , [pickerVisible,setPickerVisible])
    
    return {Picker:ProxyPicker, pickerVisible, setPickerVisible }
}



export const Picker = <T extends Tagged>(props: Props<T>) => {

    const { t } = useTranslation()
    
    const { resourceIdProp = "id", 
    
        title, 
        width = 800, 
        icon, 
        visible, 
        resources, 
        onSelection, 
        onClose,
        filterGroup, 
        decorations=[],
        filters=[],
        selected = [], 
        noContext, 
        ...rest} = props

    const selection = useListState<T>()

    const system = useSystem()

   const {ContextSelector, contextFilteredData } = useContextFilter({
        data:resources,
        group: filterGroup,
        style:{width: 170 },
        disabled: selection.selected.length > 0
    })
  
    const selectionFilteredData = React.useMemo(()=> contextFilteredData.filter(r => !selected.includes(r[resourceIdProp])),[contextFilteredData,selected,resourceIdProp])

    const { TagFilter, tagFilteredData } = useTagFilter({
        filtered: selectionFilteredData,
        key: `tags`,
        group: filterGroup

    })

    const columns = childrenIn(props).type(Column).toArray()

    const select = async () => {

        var result = await onSelection(selection.selected)
        
        onClose()
        
        if (typeof result === 'function') 
            result()
    }

    const selectBtn = <Button type="primary" key="add-selected" enabled={selection.selected.length > 0} onClick={select}>
        {t("common.buttons.add_selected", { count: selection.selected.length })}
    </Button>

    const contextBtn = system.crossContext() ? ContextSelector : null

    return <Drawer renderAfterTransition bodyStyle={{marginTop:25}}  routeId={pickerParam} visible={visible} width={width} title={t("common.buttons.select_many", { "plural": title })} icon={icon} onClose={onClose}>
        <VirtualTable {...rest}  
                key={resourceIdProp} data={tagFilteredData} total={resources.length} rowKey={resourceIdProp} 
                filterGroup={filterGroup} filterPlaceholder={t("common.components.table.filter_placeholder",{plural:title.toLowerCase()})}
                filters={[TagFilter, ...filters]} 
                state={selection} decorations={[noContext || contextBtn, selectBtn, ...decorations ]} 
                selectAll>
                {columns}
        </VirtualTable>
    </Drawer>

}



import { ButtonGroup } from "apprise-frontend/components/Button";
import { useListState } from "apprise-frontend/components/hooks";
import { NoSuchRoute } from "apprise-frontend/components/NoSuchRoute";
import { Column, VirtualTable } from "apprise-frontend/components/VirtualTable";
import { icns } from "apprise-frontend/icons";
import { useLocale } from "apprise-frontend/model/hooks";
import { useTags } from "apprise-frontend/tag/api";
import { useTagFilter } from 'apprise-frontend/tag/filter';
import { TagList } from "apprise-frontend/tag/Label";
import { useTenants } from "apprise-frontend/tenant/api";
import { tenantIcon, tenantPlural, tenantType } from "apprise-frontend/tenant/constants";
import { TenantLabel } from "apprise-frontend/tenant/Label";
import { Tenant } from "apprise-frontend/tenant/model";
import { useCurrentCampaign } from "emaris-frontend/campaign/hooks";
import { usePartyInstances } from "emaris-frontend/campaign/party/api";
import { PartyInstanceLabel } from "emaris-frontend/campaign/party/Label";
import { PartyInstance } from "emaris-frontend/campaign/party/model";
import { CampaignValidation } from "emaris-frontend/campaign/validation";
import { Button } from 'apprise-frontend/components/Button';
import * as React from "react";
import { useTranslation } from "react-i18next";
import { useHistory } from "react-router-dom";
import { usePicker } from "../Picker";
import { PartyInstanceDetail } from "./Detail";


type Props = {

    report: ReturnType<CampaignValidation['validateParties']>
}

export const PartyInstanceList = (props: Props) => {

    const campaign = useCurrentCampaign()

    const parties = usePartyInstances(campaign)

    const source = parties.detailInRoute()

    const detail = parties.lookup(source)

    return source && !detail ?  <NoSuchRoute /> : <InnerPartyInstanceList {...props} detail={detail!} /> 
}


const InnerPartyInstanceList = (props: Props & { detail:PartyInstance }) => {

    const { t } = useTranslation()
    const { l } = useLocale()

    const history = useHistory()

    const tags = useTags()
    const tenants = useTenants()
    const campaign = useCurrentCampaign()

    const readOnly = campaign.guarded

    const { detail, report } = props

    const {Picker, setPickerVisible, pickerVisible} = usePicker()

    const parties = usePartyInstances(campaign)

    const liststate = useListState<PartyInstance>()

    const { selected, resetSelected, removeOne} = liststate

     const plural = t(tenantPlural)
        
    const unfilteredData = parties.all()

    // eslint-disable-next-line
    const sortedData = React.useMemo(() => parties.allSorted(), [unfilteredData])

    const partyinstanceGroup = `${campaign.id}-${tenantType}`

    const { TagFilter, tagFilteredData } = useTagFilter({
        filtered: sortedData,
        group: partyinstanceGroup,
        key:`tags`
    })

    const data = tagFilteredData

    const editBtn = (p: PartyInstance) =>
        <Button
            key="edit"
            onClick={() => history.push(parties.route(p))}>
            {t("common.buttons.open")}
        </Button>

    const removeBtn = (p: PartyInstance) =>
        <Button
            key="remove"
            type="danger"
            disabled={readOnly}
            onClick={() => parties.remove(p, removeOne) }>
            {t("common.buttons.remove")}
        </Button>

    const addBtn =
        <Button
            icn={icns.add}
            type="primary"
            disabled={readOnly}
            onClick={()=>setPickerVisible(true)}>
            {t("common.buttons.add_many", { plural })}
        </Button>

    const removeSelectedBtn =
        <Button
             enabled={selected.length >= 1}
            disabled={readOnly}
            onClick={() => parties.removeMany(selected, resetSelected)}>
            {t("common.buttons.remove_many", { count: selected.length })}
        </Button>


    const rowValidation = ({ rowData: party }) => report[party.id].status === "error" && icns.error(report[party.id].msg)    

    const unfilteredTemplates = tenants.all()

    // eslint-disable-next-line
    const activeTemplates = React.useMemo(()=>tenants.allSorted().filter(r => r.lifecycle.state !== 'inactive'),[unfilteredTemplates])

      return (<>
        <VirtualTable<PartyInstance> data={data} total={unfilteredData.length} state={liststate} selectAll

            filterGroup={partyinstanceGroup} filterBy={parties.stringify} filterPlaceholder={t("common.components.table.filter_placeholder", { plural: plural.toLowerCase() })}
            
            filters={[TagFilter]}
           
            decorations={[<ButtonGroup>{addBtn}{removeSelectedBtn}</ButtonGroup>]}
            
            onDoubleClick={p => history.push(parties.route(p))}

            refreshFlag={selected}
            
            actions={p => [editBtn(p), removeBtn(p)]}>

            <Column<PartyInstance> title={t("common.fields.name_multi.name")} decorations={[rowValidation]}
                dataKey="name" dataGetter={p => <PartyInstanceLabel noLineage noMemo instance={p} />} />

            {tags.allTagsOf(tenantType).length > 0 &&
                <Column flexGrow={1} sortable={false} title={t("common.fields.tags.name")} dataKey="t.tags" dataGetter={(pi: PartyInstance) => <TagList truncateEllipsisLink={parties.route(pi)} taglist={pi.tags} />} />
            }
        </VirtualTable>


        {
            
            pickerVisible && 
            
            <Picker id='partyinst' title={plural} icon={tenantIcon} noContext
                filterGroup={`${partyinstanceGroup}-picker`}
                resources={activeTemplates}
                selected={sortedData.map(i => i.source)}
                onSelection={added => Promise.resolve(parties.addAll(added.map(parties.generate as any)))}>

                <Column title={t("common.fields.name_multi.name")} flexGrow={1} dataKey="t.name.en" dataGetter={(t: Tenant) => l(t.name)} cellRenderer={cell => <TenantLabel noLink tenant={cell.rowData} />} />
                <Column title={t("common.fields.tags.name")} dataKey="t.tags" dataGetter={(t: Tenant) => <TagList truncateEllipsisLink={tenants.routeTo(t)} taglist={t.tags} />} flexGrow={1} />

            </Picker>

        }

        { detail && 
        
            <PartyInstanceDetail detail={detail} onClose={() => history.push(parties.route())} />
          
        }
        


    </>
    )
}
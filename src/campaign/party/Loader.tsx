
import { Placeholder } from "apprise-frontend/components/Placeholder"
import { useAsyncRender } from "apprise-frontend/utils/hooks"
import { usePartyInstances } from "emaris-frontend/campaign/party/api"
import * as React from "react"
import { useCurrentCampaign } from "../../lib/emaris-frontend/campaign/hooks"

type Props = React.PropsWithChildren<{}>


export const PartyInstanceLoader = (props:Props) => {

    const campaign = useCurrentCampaign()

    const {fetchAll,areReady} = usePartyInstances(campaign)

    const [render] = useAsyncRender({when:areReady(),task:fetchAll,content:props.children,placeholder:Placeholder.list})

    return render
  
  }



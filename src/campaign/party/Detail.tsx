
import { Button } from "apprise-frontend/components/Button"
import { Drawer } from "apprise-frontend/components/Drawer"
import { useConfig } from "apprise-frontend/config/state"
import { useFormState } from "apprise-frontend/form/hooks"
import { icns } from "apprise-frontend/icons"
import { useLocale } from "apprise-frontend/model/hooks"
import { Tab } from "apprise-frontend/scaffold/Tab"
import { Topbar } from "apprise-frontend/scaffold/Topbar"
import { useTenants } from "apprise-frontend/tenant/api"
import { tenantIcon } from "apprise-frontend/tenant/constants"
import { paramsInQuery } from "apprise-frontend/utils/routes"
import { ValidationReport } from "apprise-frontend/utils/validation"
import { usePartyInstances } from "emaris-frontend/campaign/party/api"
import { PartyInstance } from "emaris-frontend/campaign/party/model"
import { PartySettings } from "emaris-frontend/campaign/party/Settings"
import { submissionType } from "emaris-frontend/campaign/submission/constants"
import { useDashboards } from "emaris-frontend/dashboard/api"
import * as React from "react"
import { useTranslation } from "react-i18next"
import { useHistory, useLocation } from "react-router-dom"
import { useCurrentCampaign } from "../../lib/emaris-frontend/campaign/hooks"
import { PartyGeneralForm } from "./GeneralForm"
import { PartyProfile } from "./PartyProfile"




type Props =  {

    detail:PartyInstance
    onClose:()=>void

    width?: number
}


export const PartyInstanceDetail = (props:Props) => {

    const {t} = useTranslation()
    const {l} = useLocale()
    const {search} = useLocation()
    const history = useHistory()

    const {width=650,detail,onClose} = props

    const campaign = useCurrentCampaign()
    const {safeLookup} = useTenants()
    const dashboard = useDashboards()
    const config = useConfig()
    const {save,validateInstance,detailParam} = usePartyInstances(campaign)
    
    const tenant = safeLookup(detail.source)

    const formstate  = useFormState(detail)
    const {edited, dirty, reset} = formstate

    const liveBtn = <Button enabledOnReadOnly iconLeft icn={icns.edit} onClick={()=>history.push(dashboard.given(campaign).routeToParty(edited))}>{t("campaign.labels.campaign_view.live")}</Button>


    const onSave = ()=>save(edited).then(onClose)

    const revert = <Button icn={icns.revert} enabled={dirty} onClick={()=>reset()}>{t("common.buttons.revert")}</Button>
    const saveact = <Button icn={icns.save} enabled={dirty} type="primary" onClick={onSave}>{t("common.buttons.save")}</Button> 

    const {tab='info'} = paramsInQuery(search)

    const report = validateInstance(edited)

    return  <Drawer readonly={!edited.source} renderAfterTransition routeId={detailParam()} visible width={width} warnOnClose={dirty} title={t("common.buttons.edit_one", {singular:l(tenant?.name!)})} icon={tenantIcon} onClose={onClose}>
                
                <Topbar offset={63}>
                    {saveact}
                    {revert}
                    {config.get().routedTypes?.includes(submissionType) && liveBtn}

                    <Tab default id='info' icon={icns.form} name={t("common.labels.general")} />
                    <Tab  id="preferences" icon={icns.settings}  name={t("common.labels.preferences")}  />
                    <Tab  id="profile" icon={icns.profile}  name={t("campaign.labels.profile")}  />
    
                </Topbar>

                { tab==='info' &&  <PartyGeneralForm state={formstate} report={report} />  }
                { tab==='preferences' && <PartySettings state={formstate} report={report as ValidationReport} /> }
                { tab==='profile' &&  <PartyProfile state={formstate} /> }

              
            </Drawer>
}

import { Form } from "apprise-frontend/form/Form"
import { FormState } from "apprise-frontend/form/hooks"
import { NoteBox } from "apprise-frontend/form/NoteBox"
import { SimpleBox } from "apprise-frontend/form/SimpleBox"
import { TagBoxset } from "apprise-frontend/tag/TagBoxset"
import { tenantType } from "apprise-frontend/tenant/constants"
import { TenantLabel } from "apprise-frontend/tenant/Label"
import { useUsers } from "apprise-frontend/user/api"
import { PartyInstanceLabel } from "emaris-frontend/campaign/party/Label"
import { PartyInstance } from "emaris-frontend/campaign/party/model"
import { partyinstvalidationapi } from "emaris-frontend/campaign/party/validation"
import * as React from "react"
import { useTranslation } from "react-i18next"



type Props = {

    state: FormState<PartyInstance>
    report: ReturnType<ReturnType<ReturnType<typeof partyinstvalidationapi>>['validateInstance']>

}

export const PartyGeneralForm = (props: Props) => {

    const { t } = useTranslation()

    const { state, report } = props;

    const { edited, change } = state

    const { logged } = useUsers()


    return <Form state={state} >


        <TagBoxset edited={edited.tags} type={tenantType} validation={report} onChange={change((t, v) => t.tags = v)} />


        <NoteBox enabledOnReadOnly label={t("common.fields.note.name")} validation={report.note} autoSize={{ minRows: 4, maxRows: 6 }} onChange={change((t, v) => t.properties.note = t.properties.note ? { ...t.properties.note, [logged.tenant]: v } : { [logged.tenant]: v })} >
            {edited.properties.note}
        </NoteBox>

        <SimpleBox label={t("campaign.fields.source.name")} validation={report.source} >
            <TenantLabel tenant={edited.source} />
        </SimpleBox>

        {edited.lineage &&


            <SimpleBox label={t("common.fields.lineage.name")} validation={report.lineage} >
                <PartyInstanceLabel showCampaign instance={edited.lineage} />
            </SimpleBox>

        }


    </Form>


}
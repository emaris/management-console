import { Drawer } from "apprise-frontend/components/Drawer"
import { FormState } from "apprise-frontend/form/hooks"
import { useLayout } from 'apprise-frontend/layout/LayoutProvider'
import { paramIcon } from "apprise-frontend/layout/parameters/constants"
import { ParameterPanel } from "apprise-frontend/layout/parameters/Panel"
import { deepequals } from "apprise-frontend/utils/common"
import { useCampaigns } from "emaris-frontend/campaign/api"
import { PartyInstance } from "emaris-frontend/campaign/party/model"
import { useRequirementInstances } from "emaris-frontend/campaign/requirement/api"
import { predefinedParameterIds } from "emaris-frontend/layout/parameters/constants"
import { Product } from "emaris-frontend/product/model"
import { Requirement } from "emaris-frontend/requirement/model"
import * as React from "react"
import { useTranslation } from "react-i18next"
import { reqParam } from "./PartyProfile"


type RequirementProps = {

    target: Requirement
    onClose: ()=>void
 
    state: FormState<PartyInstance> 
}

export const RequirementOverlaySection = (props:RequirementProps) => {

    const {t} = useTranslation()
    
    const {target,onClose,state} = props

    const {edited,change} = state

    const plural = t('layout.parameters.plural')

    const {allParameters} = useLayout()

    const overlays = edited.properties.parameterOverlays.requirements[target.id] ?? {}

    // const parameters = allParameters().filter(p=>!predefinedParameterIds.includes(p.id) && !predefinedRequirementParameterIds.includes(p.id))
    const parameters = allParameters().filter(p=>!predefinedParameterIds.includes(p.id))

    const campaign = useCampaigns().safeLookup(edited.campaign)

    const allRequirementInstances = useRequirementInstances(campaign).allForParty(edited).filter(ri => ri.source === target.id)

    const requirementInstanceOverlays = allRequirementInstances.length === 1 ? allRequirementInstances[0].properties.parameterOverlay ?? {} : {}
  
    const overlayed = parameters.map(p=>{
        
        const value = overlays[p.name]?.value ? overlays[p.name].value : requirementInstanceOverlays[p.name] ? requirementInstanceOverlays[p.name].value : p.value 
        return {...p,value }
    })

    const onChange =  change( (t,[i,v]) => {

        const overlays = t.properties.parameterOverlays.requirements
        const parameter = parameters[i]
        const exists = overlays[target.id]

        //eslint-disable-next-line
        const unchanged = deepequals(v,parameter.value) || v == parameter.value        // the lax equality here is intended to capture equality up to type conversions.

        if ( unchanged) {
            if ( overlays[target.id] &&  overlays[target.id][parameter.name])
                delete overlays[target.id][parameter.name]
            return
        }

        if (!exists)
            overlays[target.id]= {}       
        overlays[target.id][parameter.name]={value: v, original: parameter} 
    })

    return <Drawer routeId={reqParam} visible={true} width={400} warnOnClose={false}  title={plural} icon={paramIcon} onClose={onClose}>
                <div style={{height:"100%",display:"flex",flexDirection:"column"}}>
                    <ParameterPanel forInstance parameters={overlayed} onChange={(index,value)=> onChange([index,value]) }  />
                </div>
         </Drawer>
        
}


type ProductProps = {

    target: Product
    onClose: ()=>void
 
    state: FormState<PartyInstance> 
}

export const ProductOverlaySection = (props:ProductProps) => {

    const {t} = useTranslation()
    
    const {target,onClose,state} = props

    const {edited,change} = state

    const plural = t('layout.parameters.plural')

    const {allParameters} = useLayout()

    const overlays = edited.properties.parameterOverlays.products[target.id] ?? {}

    // const parameters = allParameters().filter(p=>!predefinedParameterIds.includes(p.id) && !predefinedRequirementParameterIds.includes(p.id))
    const parameters = allParameters().filter(p=>!predefinedParameterIds.includes(p.id))

    const overlayed = parameters.map(p=>{
        const value = overlays[p.name]?.value ? overlays[p.name].value : p.value 
        return {...p,value }
    })

    const onChange =  change( (t,[i,v]) => {

        const overlays = t.properties.parameterOverlays.products
        const parameter = parameters[i]
        const exists = overlays[target.id]

        //eslint-disable-next-line
        const unchanged = deepequals(v,parameter.value) || v == parameter.value        // the lax equality here is intended to capture equality up to type conversions.
      
        if (unchanged) {
            if ( overlays[target.id] &&  overlays[target.id][parameter.name])
                delete overlays[target.id][parameter.name]
            return
        }

        if (!exists)
            overlays[target.id]= {}       
       
        overlays[target.id][parameter.name]={value: v, original: parameter} 
    })

    return <Drawer routeId={reqParam} visible={true} width={400} warnOnClose={false}  title={plural} icon={paramIcon} onClose={onClose}>
                <div style={{height:"100%",display:"flex",flexDirection:"column"}}>
                    <ParameterPanel forInstance parameters={overlayed} onChange={(index,value)=> onChange([index,value]) }  />
                </div>
         </Drawer>
        
}
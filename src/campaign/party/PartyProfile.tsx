

import { Button } from "apprise-frontend/components/Button"
import { DrawerAnchor } from "apprise-frontend/components/Drawer"
import { Column, VirtualTable } from "apprise-frontend/components/VirtualTable"
import { FormState } from "apprise-frontend/form/hooks"
import { icns } from "apprise-frontend/icons"
import { LayoutProvider } from "apprise-frontend/layout/LayoutProvider"
import { instanceFrom } from "apprise-frontend/layout/model"
import { TocEntry, TocLayout } from "apprise-frontend/scaffold/TocLayout"
import { BytestreamedContext } from 'apprise-frontend/stream/BytestreamedHelper'
import { paramsInQuery, updateQuery } from "apprise-frontend/utils/routes"
import { useCurrentCampaign } from "emaris-frontend/campaign/hooks"
import { PartyInstance } from "emaris-frontend/campaign/party/model"
import { useProductInstances } from "emaris-frontend/campaign/product/api"
import { ProductInstanceLabel } from "emaris-frontend/campaign/product/Label"
import { ProductInstance } from "emaris-frontend/campaign/product/model"
import { useRequirementInstances } from "emaris-frontend/campaign/requirement/api"
import { RequirementInstanceLabel } from "emaris-frontend/campaign/requirement/Label"
import { RequirementInstance } from "emaris-frontend/campaign/requirement/model"
import { useProducts } from "emaris-frontend/product/api"
import { productPlural, productType } from "emaris-frontend/product/constants"
import { ProductDetailLoader } from 'emaris-frontend/product/DetailLoader'
import { useRequirements } from "emaris-frontend/requirement/api"
import { requirementPlural, requirementType } from "emaris-frontend/requirement/constants"
import { RequirementDetailLoader } from 'emaris-frontend/requirement/DetailLoader'
import * as React from "react"
import { useTranslation } from "react-i18next"
import { useHistory, useLocation } from "react-router-dom"
import { ProductOverlaySection, RequirementOverlaySection } from "./ParamOverlayForm"
import "./styles.scss"

type Props = {

    state: FormState<PartyInstance>
}

// offset for anchors to hanf off the top of the page
const anchorOffset = 160

export const PartyProfile = (props: Props) => {

    const { t } = useTranslation()

    return <TocLayout className="party-profile" offsetTop={anchorOffset + 10}>

        <TocEntry id={requirementType} title={t(requirementPlural)} />
        <TocEntry id={productType} title={t(productPlural)} />

        <div className="profile-instances">

            <BytestreamedContext>
                <RequirementSection {...props} />
                <ProductSession {...props} />
            </BytestreamedContext>

        </div>

    </TocLayout>
}


export const reqParam = "reqparameter"


const RequirementSection = (props: Props) => {

    const { t } = useTranslation()

    const history = useHistory()
    const { pathname, search } = useLocation()

    const campaign = useCurrentCampaign()
    const requirements = useRequirements()

    const { [reqParam]: reqparameter } = paramsInQuery(search)

    const target = requirements.lookup(reqparameter as string)

    const editRequirements = (r: RequirementInstance) => history.push(`${pathname}?${updateQuery(search).with(params => params[reqParam] = r.source)}`)
    const closeRequirements = () => history.push(`${pathname}?${updateQuery(search).with(params => params[reqParam] = null)}`)


    const instances = useRequirementInstances(campaign)
    const all = instances.allSorted()

    const plural = t('layout.parameters.plural')

    const editParams = (r: RequirementInstance) => <Button icn={icns.edit} key="edit" onClick={() => editRequirements(r)} > {t("common.buttons.edit_many", { plural })}</Button>

    return <DrawerAnchor id={requirementType} className={`instance-section`} offset={anchorOffset}>

        <h1 className=".section-title">{t(requirementPlural)}</h1>

        <VirtualTable height={2000} rowHeight={40} data={all} selectable={false} rowKey="source" filtered={all.length > 15}
            onDoubleClick={r => editRequirements(r)}
            actions={p => [editParams(p)]}>

            <Column<RequirementInstance> flexGrow={1} title={t("common.fields.name_multi.name")} dataKey="name" dataGetter={p => <RequirementInstanceLabel lineage instance={p} />} />

        </VirtualTable>



        {target &&
            <RequirementDetailLoader id={target.id} >{loaded =>

                <LayoutProvider for={props.state.edited.id} layoutConfig={{ type: requirementType }} layout={instanceFrom(loaded?.properties.layout)} onChange={() => {/* no real change intended here, only overlays. */ }}>
                    <RequirementOverlaySection key={loaded?.id} {...props} target={target} onClose={closeRequirements} />
                </LayoutProvider>

            }</RequirementDetailLoader>
        }


    </DrawerAnchor>


}




const ProductSession = (props: Props) => {

    const { t } = useTranslation()

    const history = useHistory()
    const { pathname, search } = useLocation()

    const campaign = useCurrentCampaign()
    const products = useProducts()

    const { prodparameter } = paramsInQuery(search)

    const target = products.lookup(prodparameter as string)

    const editParameters = (r: RequirementInstance) => history.push(`${pathname}?${updateQuery(search).with(params => params.prodparameter = r.source)}`)
    const close = () => history.push(`${pathname}?${updateQuery(search).with(params => params.prodparameter = null)}`)

    const instances = useProductInstances(campaign)
    const all = instances.allSorted()

    const plural = t('layout.parameters.plural')

    const editParams = (r: ProductInstance) => <Button icn={icns.edit} key="edit" onClick={() => editParameters(r)} > {t("common.buttons.edit_many", { plural })}</Button>

    return <DrawerAnchor id={productType} className={`instance-section`} offset={anchorOffset}>

        <h1 className=".section-title">{t(productPlural)}</h1>

        <VirtualTable height={2000} rowHeight={40} data={all} selectable={false} rowKey="source" filtered={all.length > 15}
            onDoubleClick={r => editParameters(r)}
            actions={p => [editParams(p)]}>

            <Column<ProductInstance> flexGrow={1} title={t("common.fields.name_multi.name")} dataKey="name" dataGetter={p => <ProductInstanceLabel link instance={p} />} />

        </VirtualTable>


        {target &&

            <ProductDetailLoader id={target.id} >{loaded =>

                <LayoutProvider for={props.state.edited.id} layoutConfig={{ type: productType }} layout={instanceFrom(loaded.properties.layout)} onChange={() => {/* no real change intended here, only overlays. */ }}>
                    <ProductOverlaySection key={loaded.id} {...props} target={target} onClose={close} />
                </LayoutProvider>
            }</ProductDetailLoader>
        }

    </DrawerAnchor>


}
import { NoteBox } from 'apprise-frontend/form/NoteBox'
import { ReadOnlyContext } from 'apprise-frontend/scaffold/ReadOnly'
import { useTags } from 'apprise-frontend/tag/api'
import { TenantBox } from 'apprise-frontend/tenant/TenantBox'
import { useUsers } from 'apprise-frontend/user/api'
import { campaignmodule } from 'emaris-frontend/campaign/module'
import { CampaignSettingsFields } from 'emaris-frontend/campaign/Settings'
import { Form } from 'apprise-frontend/form/Form'
import { FormState } from 'apprise-frontend/form/hooks'
import { MultiBox } from 'apprise-frontend/form/MultiBox'
import { contextCategory } from 'apprise-frontend/system/constants'
import { TagBoxset } from 'apprise-frontend/tag/TagBoxset'
import { campaignType } from 'emaris-frontend/campaign/constants'
import { Campaign } from 'emaris-frontend/campaign/model'
import { partition } from "lodash"
import * as React from 'react'
import { useTranslation } from 'react-i18next'
import { systemType } from 'apprise-frontend/system/constants'
import { CampaignValidation } from 'emaris-frontend/campaign/validation'

export type CampaignFormProps = FormState<Campaign> & {

    isNew: boolean
    report: ReturnType<CampaignValidation['validateProfile']>
}
    

/*
Many lines are commented because the lineage has been removed from the forms
and I don't want to loose the previous state until the behaviours are consolidated.
*/

export const ProfileForm = (props: CampaignFormProps) =>  {
    
    const {t} = useTranslation()
        
    // const {contextOf,defaultContext} = useSystem()
    const {lookupTag,lookupCategory} = useTags()
    // const campaigns = useCampaigns()

    const {logged} = useUsers()

    // const {isNew,edited, initial,change,report} = props;
    const {edited, change,report} = props;

    const readonly = React.useContext(ReadOnlyContext)
    const [facets,other] = partition(edited.tags,t=>lookupTag(t).type===systemType)
   
    const context = lookupCategory(contextCategory)

    // const singular = campaignmodule.nameSingular.toLowerCase()

    // const otherCampaigns = campaigns.all().filter(c=>c.id!==edited.id)
    
    return <Form state={props} sidebar> 

            <MultiBox id="campaign-name" label={t("common.fields.name_multi.name")} validation={report.name} onChange={change( (t,v)=> t.name=v) }>
                {edited.name}
            </MultiBox>

            <MultiBox id="campaign-description" label={t("common.fields.description_multi.name")} validation={report.description} onChange={change( (t,v)=> t.description=v) }>
                {edited.description}
            </MultiBox>

           {/* {isNew || !initial.lineage ||  

                <ContextAwareSelectBox  label={t("common.fields.lineage.name",{singular})}  validation={report.lineage}
                        currentContext = {contextOf(edited)} 
                        singleContext={!defaultContext()}
                        options={otherCampaigns}  
                        onChange={change((t,v)=>t.lineage=v.id)}
                        renderOption={c=><CampaignLabel noLink campaign={c} />} 
                        lblTxt={c=>l(c.name)}
                        optionId={c=>c.id}>
                        
                    {[campaigns.lookup(edited.lineage)]}

                </ContextAwareSelectBox>
            
            } */}

            <TagBoxset edited={edited.tags} type={campaignType} validation={report} onChange={ change( (t,v) => t.tags=v) } />

            <CampaignSettingsFields state={props} report={report} />            

            <TenantBox 
                label={t("campaign.fields.stat_exclude_list.name")}
                validation={report.statExcludeList}
                resource={edited.properties.statExcludeList ?? []}
                module={campaignmodule}
                editable={!readonly}
                allowNoTenant={false}
                onChange={change( (t,v) => t.properties.statExcludeList=v)}
                />

            <TagBoxset edited={facets} categories={[context]} validation={report} onChange={change( (t,v) =>t.tags=[...v,...other])}  />

        
            <NoteBox label={t("common.fields.note.name")} validation={report.note} autoSize={{minRows:4,maxRows:6}} onChange={change( (t,v)=> t.properties.note = t.properties.note ? {...t.properties.note, [logged.tenant]: v} : {[logged.tenant]: v} ) } >
                    {edited.properties.note} 
            </NoteBox> 



    </Form>

}


import { Icon, Tooltip } from "antd";
import { buildinfo } from 'apprise-frontend/buildinfo';
import { Button } from "apprise-frontend/components/Button";
import { Drawer, DrawerProps, useRoutableDrawer } from "apprise-frontend/components/Drawer";
import { IdProperty } from "apprise-frontend/components/IdProperty";
import { NoSuchRoute } from "apprise-frontend/components/NoSuchRoute";
import { Placeholder } from "apprise-frontend/components/Placeholder";
import { RouteGuard } from "apprise-frontend/components/RouteGuard";
import { SideList } from "apprise-frontend/components/SiderList";
import { Paragraph, Text } from "apprise-frontend/components/Typography";
import { useConfig } from "apprise-frontend/config/state";
import { Form } from "apprise-frontend/form/Form";
import { FormState, useFormState } from "apprise-frontend/form/hooks";
import { SliderBox } from "apprise-frontend/form/SliderBox";
import { iamPlural } from "apprise-frontend/iam/constants";
import { any, specialise } from "apprise-frontend/iam/model";
import { icns } from "apprise-frontend/icons";
import { useLocale } from "apprise-frontend/model/hooks";
import { LifecycleSummary } from "apprise-frontend/model/lifecycle";
import { PushGuard } from 'apprise-frontend/push/PushGuard';
import { Page } from "apprise-frontend/scaffold/Page";
import { Subtitle, Titlebar } from "apprise-frontend/scaffold/PageHeader";
import { Sidebar } from "apprise-frontend/scaffold/Sidebar";
import { Tab } from "apprise-frontend/scaffold/Tab";
import { Topbar } from "apprise-frontend/scaffold/Topbar";
import { TagList } from "apprise-frontend/tag/Label";
import { tenantIcon, tenantPlural, tenantType } from "apprise-frontend/tenant/constants";
import { TimeZoneContext } from "apprise-frontend/time/hooks";
import { useUsers } from "apprise-frontend/user/api";
import { through } from "apprise-frontend/utils/common";
import { useFeedback } from 'apprise-frontend/utils/feedback';
import { useAsyncRender } from "apprise-frontend/utils/hooks";
import { parentIn } from "apprise-frontend/utils/routes";
import { withReport } from "apprise-frontend/utils/validation";
import { useCampaigns } from "emaris-frontend/campaign/api";
import { useCampaignPermissions } from "emaris-frontend/campaign/CampaignPermissions";
import { campaignPlural, campaignRoute, campaignSingular } from "emaris-frontend/campaign/constants";
import { useEventInstances } from "emaris-frontend/campaign/event/api";
import { EventInstanceLabel } from "emaris-frontend/campaign/event/Label";
import { AbsoluteDate } from "emaris-frontend/campaign/event/model";
import { ArchiveLabel, CampaignLabel, MutedLabel, SuspendedLabel } from "emaris-frontend/campaign/Label";
import { Campaign } from "emaris-frontend/campaign/model";
import { campaignmodule } from "emaris-frontend/campaign/module";
import { usePartyInstances } from "emaris-frontend/campaign/party/api";
import { useProductInstances } from "emaris-frontend/campaign/product/api";
import { useRequirementInstances } from "emaris-frontend/campaign/requirement/api";
import { submissionType } from "emaris-frontend/campaign/submission/constants";
import { useDashboards } from "emaris-frontend/dashboard/api";
import { useEvents } from "emaris-frontend/event/api";
import { eventIcon, eventPlural, eventType } from "emaris-frontend/event/constants";
import { productIcon, productPlural, productType } from "emaris-frontend/product/constants";
import { requirementIcon, requirementPlural, requirementType } from "emaris-frontend/requirement/constants";
import moment from "moment-timezone";
import * as React from "react";
import { Trans, useTranslation } from "react-i18next";
import { BsSafe2Fill } from "react-icons/bs";
import { HiPause, HiPlay } from 'react-icons/hi';
import { RiNotificationLine, RiNotificationOffLine } from 'react-icons/ri';
import { useHistory, useLocation, useParams } from "react-router";
import { CurrentCampaignContext } from "../lib/emaris-frontend/campaign/hooks";
import { EventInstances } from "./event/Events";
import { ProfileForm } from "./GeneralForm";
import { campaignGroup } from './List';
import { useCampaignLiveGuard } from './LiveGuard';
import { Parties } from "./party/Parties";
import { ProductInstances } from "./product/Products";
import { RequirementInstances } from "./requirement/Requirements";

export const CampaignDetail = () => {

    const { pathname } = useLocation();

    const { id } = useParams<{ id: string }>()

    const campaigns = useCampaigns()

    const current = campaigns.lookup(id)

    if (!current)
        return <NoSuchRoute backTo={parentIn(pathname)} />

    return <PushGuard>
        <CampaignDetailLoader key={current.id} detail={current} />
    </PushGuard>

}


export const NewCampaignDetail = () => {

    const events = useEvents()

    const [fetched, setFetched] = React.useState<boolean>(false)

    //New campaign loads events to use them on save time to import managed campaign events
    const [render] = useAsyncRender({
        when: fetched,
        task: () => events.fetchAll().then(_ => setFetched(true)),
        content: <InnerCampaignDetail isNew={true} />,
        placeholder: Placeholder.page

    })

    return render

}


const CampaignDetailLoader = (props: { detail: Campaign }) => {

    const { detail } = props

    const campaigns = useCampaigns()

    const initialValue = campaigns.isLoaded(detail.id) ? detail : undefined

    const [fetched, setFetched] = React.useState<Campaign | undefined>(initialValue)


    const [render] = useAsyncRender({
        when: !!fetched && fetched.id === detail.id,
        task: () => campaigns.fetchOne(detail).then(setFetched),

        content: () => <CurrentCampaignContext campaign={fetched!}>
            <InnerCampaignDetail isNew={false} detail={fetched!} />
        </CurrentCampaignContext>,

        placeholder: Placeholder.page

    })


    return render

}


const InnerCampaignDetail = (props: { isNew: boolean, detail?: Campaign }) => {

    const { l } = useLocale()
    const { t } = useTranslation()

    const history = useHistory()
    const { search } = useLocation()
    const { type } = useParams<{ type: string }>()


    const [Permissions, showPermissions] = useCampaignPermissions()

    const { logged } = useUsers()
    const config = useConfig()
    const campaigns = useCampaigns()

    const context = React.useRef(campaigns.next())

    // clean up context
    React.useEffect(() => campaigns.resetNext()

        //eslint-disable-next-line
        , [])

    const { isNew, detail = context.current.source } = props

    const dashboard = useDashboards()

    const { actions: { manage } } = campaigns

    const formstate = useFormState<Campaign>(detail);

    const { edited, initial, reset, dirty } = formstate

    const { liveGuard, liveGuarded } = useCampaignLiveGuard({
        campaign: edited
    })

    const singular = t(campaignmodule.nameSingular)
    const name = l(edited.name)

    const [drawerVisible, setDrawerVisible] = React.useState(false)

    const { Drawer: SuspendDrawer, open: openSuspend, close: closeSuspend } = useRoutableDrawer({ id: 'suspend' })
    const { Drawer: ArchiveDrawer, open: openArchive, close: closeArchive } = useRoutableDrawer({ id: 'archive' })
    const { Drawer: MuteDrawer, open: openMute, close: closeMute } = useRoutableDrawer({ id: 'mute' })

    // -------------- authz
    const manageIt = specialise(manage, detail.id)

    // these reports can be costly to compute at each render, so we memoise them on a per-instance basis.
    // based on whether the underlying instances lists have changed or not. 

    // Unforutnately, these lists are indirect dependencies of the report and the linter thinks are spurious. 
    // Conversely, it think the apparent dependencies are important. I disable it here, as I cannot change
    // api design to accomodate it.

    const partyinstances = usePartyInstances(edited).all()
    const reqinstances = useRequirementInstances(edited).all()
    const prodinstances = useProductInstances(edited).all()
    const eventinstances = useEventInstances(edited).all()




    //eslint-disable-next-line
    const profileReport = React.useMemo(() => campaigns.validateProfile(edited, initial), [edited])
    //eslint-disable-next-line
    const partiesReport = React.useMemo(() => partyinstances && campaigns.validateParties(edited), [partyinstances])
    //eslint-disable-next-line
    const productsReport = React.useMemo(() => prodinstances && campaigns.validateProducts(edited), [prodinstances])
    //eslint-disable-next-line
    const requirementsReport = React.useMemo(() => reqinstances && campaigns.validateRequirements(edited), [reqinstances])
    //eslint-disable-next-line
    const eventsReport = React.useMemo(() => eventinstances && campaigns.validateEvents(edited), [eventinstances])

    // -------------- error report
    const report = {
        profile: profileReport,
        parties: isNew ? withReport({}) : partiesReport,
        products: isNew ? withReport({}) : productsReport,
        requirements: isNew ? withReport({}) : requirementsReport,
        events: isNew ? withReport({}) : eventsReport,
    }

    const totalErrors = Object.keys(report).reduce((total, key) => total += report[key].errors(), 0)


    // -------------- actions
    const onSave = (campaign: Campaign) => campaigns.save(campaign, context.current)
        // reset to saved as initial state (or will appear dirty)
        .then(through(saved => reset(saved, false)))
        .then(through(saved => history.push(`${campaigns.routeTo(saved)}${search}`)))

    const onClone = (campaign: Campaign) => {
        campaigns.setNext({ source: campaigns.clone(campaign), lineage: campaign, type: 'clone' })
        history.push(`${campaignRoute}/new`)
    }

    const onBranch = (campaign: Campaign, offset: number) => {
        campaigns.setNext({ source: campaigns.branch(campaign), lineage: campaign, type: 'branch', timeOffset: offset })
        history.push(`${campaignRoute}/new`)
    }

    const suspended = !!campaigns.isSuspendSubmissions(edited)
    const suspendedStatus = suspended ? 'off' : 'on'


    const onSuspendChange = () => {

        campaigns.setSuspendSubmissions(detail, !suspended)

        onSave(detail).then(closeSuspend)

    }

    const muted = !!campaigns.isMuted(edited)
    const mutedStatus = muted ? 'off' : 'on'

    const onMutedChange = () => {

        campaigns.setMuted(detail, !muted)

        onSave(detail).then(closeMute)

    }

    const archive = () => {

        campaigns.setArchived(detail)

        onSave(detail).then(closeArchive)
    }




    const readOnly = liveGuarded || !logged.can(manageIt) || campaigns.isArchived(edited)

    // -------------- buttons
    const saveBtn = <Button
        icn={icns.save}
        enabled={dirty}
        disabled={report.profile.errors() > 0}
        dot={report.profile.errors() > 0}
        onClick={() => onSave(edited)}>
        {t("common.buttons.save")}
    </Button>

    const cantremove = isNew || dirty
    const removeBtn = <Button
        enabledOnReadOnly={buildinfo.development}
        icn={icns.remove}
        disabled={cantremove}
        onClick={() => { campaigns.remove(detail.id, () => history.push(campaignRoute), !buildinfo.development) }}>
        {t("common.buttons.remove")}
    </Button>

    const revertBtn = <Button
        icn={icns.revert}
        enabled={dirty}
        type="danger"
        onClick={() => reset()}>
        {t("common.buttons.revert")}
    </Button>

    const cantclone = isNew || !logged.can(manage)
    const clone = <Button
        enabledOnReadOnly={!campaigns.isArchived(edited)} // overrides reules unless it's archived
        disabled={cantclone}
        icn={icns.clone}
        onClick={() => onClone(edited)}>
        {t("common.buttons.clone")}
    </Button>

    const cantbranch = isNew || !logged.can(manage)
    const branch = <Button
        enabledOnReadOnly={!campaigns.isArchived(edited)}   // overrides reules unless it's archived
        disabled={cantbranch}
        icn={icns.branch}
        onClick={() => setDrawerVisible(true)}>
        {t("campaign.buttons.branch")}
    </Button>

    const liveBtn = <Button
        enabledOnReadOnly
        enabled={campaigns.isStarted(edited)}
        iconLeft
        icn={icns.edit}
        onClick={() => history.push(dashboard.routeTo(edited))}>
        {t("campaign.labels.campaign_view.live")}
    </Button>


    const rightsBtn = <Button
        icn={icns.permissions}
        enabledOnReadOnly
        disabled={isNew || dirty}
        onClick={showPermissions}>
        {t(iamPlural)}
    </Button>

    const addCampaignBtn = <Button
        type="primary"
        enabledOnReadOnly
        icn={icns.add}
        enabled={logged.can(manage)}
        onClick={() => campaigns.setNext({ ...campaigns.next(), type: 'new' })}
        linkTo={`${campaignRoute}/new`} >
        {t("common.buttons.add_one", { singular })}
    </Button>

    const cantsuspend = isNew || dirty || campaigns.isArchived(edited)
    const suspendBtn = <Button
        type="danger"
        enabledOnReadOnly
        icn={icns.pause}
        disabled={cantsuspend}
        onClick={openSuspend}>
        {t(`campaign.suspend.suspend_btn_${suspendedStatus}`)}
    </Button>

    const muteBtn = <Button
        type="danger"
        enabledOnReadOnly
        disabled={isNew || dirty}
        icn={icns.muted}
        onClick={openMute}>
        {t(`campaign.mute.mute_btn_${mutedStatus}`)}
    </Button>



    const cantarchive = dirty || !campaigns.canArchive(edited)
    const archiveBtn = <Button
        icn={icns.archive}
        disabled={cantarchive}
        onClick={openArchive}>
        {t("campaign.archive.archive_btn")}
    </Button>

    const onTabChange = key => history.push(`${campaigns.routeTo(detail)}${key === 'info' ? '' : `/${key}`}${search}`)

    const tabcompo = (() => {

        switch (type) {
            case tenantType: return <Parties report={report.parties} />
            case requirementType: return <RequirementInstances report={report.requirements} />
            case productType: return <ProductInstances report={report.products} />
            case eventType: return <EventInstances report={report.events} />
            default: return <ProfileForm isNew={isNew} report={report.profile} {...formstate} />
        }
    })()

    const primaryBtn = !isNew && !dirty && campaigns.isEnded(edited) && campaigns.canArchive(edited) ? archiveBtn : saveBtn

    return <Page readOnly={readOnly}>

        <Sidebar>
            {React.cloneElement(primaryBtn, { type: 'primary' })}
            {primaryBtn === archiveBtn && saveBtn}
            {revertBtn}
            {clone}
            {branch}
            {removeBtn}
            {rightsBtn}

            <br />

            {muteBtn}
            {suspendBtn}
            

            <br />

            <IdProperty id={detail.id} />

            <br />

            <LifecycleSummary {...edited.lifecycle} />

            <br />

            <div className="sidebar-property">
                <Paragraph>
                    {totalErrors > 0 ?

                        <Text type="danger">{t("common.validation.total_error_count", { totalErrors })}</Text>
                        :
                        <Text smaller className="emphasis">{t("common.validation.zero_error_count")}</Text>

                    }
                </Paragraph>
            </div>

            {(logged.can(manage) && !isNew) &&
                <>
                    <div style={{ marginTop: "25px" }}>{addCampaignBtn}</div>
                </>
            }
            <SideList data={campaigns.all()}
                filterGroup={campaignGroup} filterBy={campaigns.stringify} filterPlaceholder={t("common.components.table.filter_placeholder", { plural: t(campaignPlural.toLowerCase()).toLowerCase() })}
                renderData={c => l(c.name)}
                render={c => <CampaignLabel noMemo selected={c.id === edited.id} campaign={c} />} />


        </Sidebar>

        <Topbar onTabChange={onTabChange} activeTab={type}>

            <Titlebar title={name || (name === undefined ? `<${t('common.labels.new')}>` : "")}>
                {edited.lineage && <Subtitle className="lineage-subtitle">
                    <div className='subtitle-label'>
                        <span >{t("common.fields.lineage.name")}:&nbsp;&nbsp;</span><CampaignLabel targetMode='lineage' noIcon campaign={edited} />
                    </div>
                </Subtitle>}
                <div style={{ display: "flex", alignItems: "center" }}>
                    <CampaignLabel mode='tag' statusOnly campaign={edited} />
                    <ArchiveLabel campaign={edited} />
                    <SuspendedLabel campaign={edited} />
                    <MutedLabel campaign={edited} />
                    <TagList taglist={edited.tags} />
                </div>
            </Titlebar>


            <Tab default id="info" icon={icns.form} name={t("common.labels.general")} badge={report.profile.errors() > 0} />
            <Tab id={tenantType} disabled={isNew} icon={tenantIcon} name={t(tenantPlural)} badge={report.parties.errors() > 0} />
            <Tab id={requirementType} disabled={isNew} icon={requirementIcon} name={t(requirementPlural)} badge={report.requirements.errors() > 0} />
            <Tab id={productType} disabled={isNew} icon={productIcon} name={t(productPlural)} badge={report.products.errors() > 0} />
            <Tab id={eventType} disabled={isNew} icon={eventIcon} name={t(eventPlural)} badge={report.events.errors() > 0} />

            {liveGuard}


            {React.cloneElement(primaryBtn, { type: 'primary' })}
            {primaryBtn === archiveBtn ? saveBtn : campaigns.canArchive(edited) && archiveBtn}
            {revertBtn}
            {clone}
            {branch}
            {removeBtn}
            {rightsBtn}
            {config.get().routedTypes?.includes(submissionType) && liveBtn}
            {muteBtn}
            {suspendBtn}

        </Topbar>

        <Permissions resourceCentric resourceRange={[edited]} filter={{ resources: [edited.id, any] }} />

        <TimeZoneContext.Provider value={campaigns.timeZone(edited)}>
            {tabcompo}
        </TimeZoneContext.Provider>

        <RouteGuard when={dirty} onOk={() => reset(edited, false)} />

        <BranchDrawer enabledOnReadOnly readonly={cantbranch} visible={drawerVisible} setVisible={setDrawerVisible} onBranch={onBranch} edited={edited} formstate={formstate} />

        <SuspendDrawer enabledOnReadOnly readonly={cantsuspend} width={400} className='suspend-drawer' icon={icns.pause} title={t('campaign.suspend.suspend_title')}>
            <SuspendDrawerPanel status={suspendedStatus} onConfirm={onSuspendChange} />
        </SuspendDrawer>

        <ArchiveDrawer readonly={cantarchive} className='archive-drawer' icon={icns.archive} title={t('campaign.archive.archive_title')}>
            <ArchiveDrawerPanel onConfirm={archive} ended={campaigns.isEnded(edited)} />
        </ArchiveDrawer>

        <MuteDrawer enabledOnReadOnly width={400} className='mute-drawer' icon={icns.muted} title={t('campaign.mute.mute_title')}>
            <MuteDrawerPanel status={mutedStatus} onConfirm={onMutedChange} />
        </MuteDrawer>
    </Page>

}

type BranchDrawerProps = Partial<DrawerProps> & {
    setVisible: (_: boolean) => void
    onBranch: (_: any, __: any) => void
    edited: Campaign
    formstate: FormState<Campaign>
}

const BranchDrawer = (props: BranchDrawerProps) => {

    const { t } = useTranslation()

    const { visible, setVisible, edited, onBranch, formstate } = props


    const [branchOffset, setBranchOffset] = React.useState(1)

    const instances = useEventInstances(edited)
    const allEventInstances = instances.all()

    //eslint-disable-next-line
    const firstRecurringEvent = React.useMemo(() => allEventInstances && allEventInstances.sort(instances.dateComparator).find(i => i.date?.kind === 'absolute' && (i.date?.branchType === 'recurring' || i.date?.branchType === undefined)), [allEventInstances])
    //eslint-disable-next-line
    const firstRecurringDate = React.useMemo(() => firstRecurringEvent ? +moment((firstRecurringEvent.date as AbsoluteDate).value).format("YYYY") : undefined, [firstRecurringEvent])
    //eslint-disable-next-line
    const branchStartYear = React.useMemo(() => firstRecurringDate || new Date().getFullYear(), [firstRecurringDate])

    return <Drawer placement="right" icon={icns.branch} enabledOnReadOnly={props.enabledOnReadOnly} visible={visible} onClose={() => setVisible(false)} title={t("campaign.branch.name", { singular: t(campaignSingular) })} width={800} className="branch-drawer">
        <Topbar autoGroupButtons={false}>
            <Button type="default" key={1} onClick={() => setVisible(false)}>{t("common.buttons.cancel")}</Button>
            <Button type="primary" key={2} onClick={() => onBranch(edited, branchOffset)}>{t("campaign.branch.button")}</Button>
        </Topbar>

        <div className="branch-info">

            {<Trans i18nKey="campaign.branch.blurb" />}
            {firstRecurringEvent && <><br /><br />
                <Trans
                    i18nKey="campaign.branch.action"
                    components={[<Tooltip title={<EventInstanceLabel noLineage noMemo noDate instance={firstRecurringEvent} />}>{firstRecurringDate}</Tooltip>]}
                    values={{ firstRecurringDate }}
                />
            </>}

        </div>

        <Form state={formstate}>
            <div className="slider">
                <SliderBox
                    label={t("campaign.branch.field.name")}
                    validation={{ msg: t("campaign.branch.field.msg", { singular: t(campaignSingular) }) }}
                    defaultValue={firstRecurringDate ? branchOffset : 0}
                    min={0}
                    max={10}
                    disabled={firstRecurringDate === undefined}
                    marks={Array(11).fill(0).map((_, i) => i).reduce((acc, cur) => ({ ...acc, [cur]: branchStartYear + cur }), {})}
                    showValues={false}
                    tooltipVisible={false}
                    onChange={v => setBranchOffset(v as number)} />
            </div>
        </Form>

    </Drawer>
}

const SuspendDrawerPanel = (props: {

    status: 'off' | 'on'
    onConfirm: () => any

}) => {

    const { status, onConfirm } = props

    const { t } = useTranslation()

    return <div className={`suspend-drawer-panel drawer-${status}`}>

        <Paragraph style={{ fontSize: 16 }} >{t(`campaign.suspend.suspend_intro_${status}`)}</Paragraph>

        <Icon component={status === 'on' ? HiPause : HiPlay} className='suspend-drawer-icon' />

        <Button type='primary' onClick={onConfirm}>{t(`campaign.suspend.suspend_confirm_${status}`)}</Button>

    </div>

}


const ArchiveDrawerPanel = (props: {
    onConfirm: () => any
    ended: boolean
}) => {

    const { onConfirm, ended } = props

    const { t } = useTranslation()
    const { askConsent } = useFeedback()

    const confirm = () => askConsent({

        title: t('campaign.archive.archive_confirm_title'),
        content: t('campaign.archive.archive_confirm_text'),
        okText: t('campaign.archive.archive_confirm_btn'),
        noValediction: true,
        okChallenge: buildinfo.development ? undefined : t('campaign.archive.archive_confirm_challenge'),
        onOk: onConfirm

    })

    return <div className="archive-drawer-panel">

        <Paragraph style={{ fontSize: 16 }} >{t(`campaign.archive.archive_intro`)}</Paragraph>

        <br />

        <Paragraph strong style={{ fontSize: 16 }}>{t(`campaign.archive.archive_warn`)}</Paragraph>

        <Icon style={{ color: "darkorange" }} component={BsSafe2Fill} className={'archive-drawer-icon'} />


        <Button type='primary' disabled={!ended} onClick={confirm}>{t(`campaign.archive.archive_action_btn`)}</Button>

        {ended || <Paragraph type='warning' strong style={{ marginTop: 30 }}>{t(`campaign.archive.archive_warn_end_1`)}</Paragraph>}
        {ended || <Paragraph strong style={{ marginTop: 30 }}>{t(`campaign.archive.archive_warn_end_2`)}</Paragraph>}

    </div>

}


const MuteDrawerPanel = (props: {

    status: 'off' | 'on'
    onConfirm: () => any

}) => {

    const { status, onConfirm } = props

    const { t } = useTranslation()

    return <div className={`mute-drawer-panel drawer-${status}`}>

        <Paragraph style={{ fontSize: 16 }} >{t(`campaign.mute.mute_intro_${status}`)}</Paragraph>

        <Icon component={status === 'on' ? RiNotificationOffLine : RiNotificationLine} className='mute-drawer-icon' />

        <Button type='primary' onClick={onConfirm}>{t(`campaign.mute.mute_confirm_${status}`)}</Button>

    </div>

}
import { useLayout } from 'apprise-frontend/layout/LayoutProvider'
import {  Parameter } from 'apprise-frontend/layout/parameters/model'
import { ParameterPanel } from 'apprise-frontend/layout/parameters/Panel'
import { isMultilang, Multilang } from 'apprise-frontend/model/multilang'
import { deepequals } from 'apprise-frontend/utils/common'
import { AssetInstance } from 'emaris-frontend/campaign/model'
import { predefinedParameterIds } from 'emaris-frontend/layout/parameters/constants'
import { isDateYear } from 'emaris-frontend/layout/parameters/DateYear'


type Props = {

    instance: AssetInstance
    excludes: (_: Parameter<any>) => boolean

    onChange: (_: AssetInstance) => any

}



export const AssetOverlayForm = (props: Props) => {

    const { instance, onChange, excludes } = props

    const { allParameters } = useLayout()

    const overlays = instance.properties.parameterOverlay ?? {}

    const parameters = allParameters().filter(p => !predefinedParameterIds.includes(p.id) && !excludes(p))

    const overlayed = parameters.map(p => ({ ...p, value: overlays[p.name] ? overlays[p.name].value : p.value }))

    const onOverride = (index: number, newval: any) => {

        const parameter = parameters[index]

        let isBlank = false

        if (isMultilang(newval)) isBlank = !Object.values(newval as Multilang).some(v => v.length)
        else if (typeof newval === 'string') isBlank = !newval.length
        else if (isDateYear(newval)) isBlank = newval?.date === undefined
        else isBlank = newval === undefined

        //eslint-disable-next-line
        const unchanged = (deepequals(newval, parameter.value)) || (newval == parameter.value)        // the lax equality here is intended to capture equality up to type conversions.

        const { parameterOverlay = {}, ...props } = instance.properties

        const { [parameter.name]: oldval, ...otherparams } = parameterOverlay

        const newOverlay = (isBlank || unchanged) ? otherparams : { ...otherparams, [parameter.name]: {value: newval, original: parameter} }

        onChange({
            ...instance, properties: deepequals(newOverlay, {}) ? props : { ...props, parameterOverlay: newOverlay }

        })

    }


    return <ParameterPanel parameters={overlayed} forInstance onChange={onOverride} />

}

import { buildinfo } from "apprise-frontend/buildinfo"
import { Button } from "apprise-frontend/components/Button"
import { useListState } from "apprise-frontend/components/hooks"
import { Column, VirtualTable } from "apprise-frontend/components/VirtualTable"
import { iamPlural } from "apprise-frontend/iam/constants"
import { specialise } from "apprise-frontend/iam/model"
import { icns } from "apprise-frontend/icons"
import { useLocale } from "apprise-frontend/model/hooks"
import { Page } from "apprise-frontend/scaffold/Page"
import { Titlebar } from "apprise-frontend/scaffold/PageHeader"
import { Sidebar } from "apprise-frontend/scaffold/Sidebar"
import { Topbar } from "apprise-frontend/scaffold/Topbar"
import { useContextFilter } from 'apprise-frontend/system/filter'
import { useTags } from "apprise-frontend/tag/api"
import { TagList } from "apprise-frontend/tag/Label"
import { useUsers } from "apprise-frontend/user/api"
import { useCampaigns } from "emaris-frontend/campaign/api"
import { useCampaignPermissions } from "emaris-frontend/campaign/CampaignPermissions"
import { campaignType } from "emaris-frontend/campaign/constants"
import { ArchiveLabel, CampaignLabel, SuspendedLabel } from "emaris-frontend/campaign/Label"
import { Campaign } from "emaris-frontend/campaign/model"
import { campaignmodule } from "emaris-frontend/campaign/module"
import { useTranslation } from "react-i18next"
import { useLocation } from "react-router"


export const campaignGroup = campaignType


export const CampaignList = () => {

    const { l } = useLocale()
    const { t } = useTranslation()

    const [Permissions, showPermissions] = useCampaignPermissions()

    const { pathname } = useLocation()
    //const history = useHistory()

    const [singular, plural] = [t(campaignmodule.nameSingular), t(campaignmodule.namePlural)]

    const tags = useTags()
    const { logged } = useUsers()
    const campaigns = useCampaigns()

    const { actions: { manage }, validationErrors } = campaigns

    const liststate = useListState<Campaign>()

    // const open = (c: Campaign) => <Button key={1} icn={icns.open} linkTo={campaigns.routeTo(c)}>{t("common.buttons.open")}</Button>
    // const remove = (campaign: Campaign) =>
    //     <Button
    //         key={2}
    //         icn={icns.remove}
    //         enabled={logged.can(manage)}
    //         disabled={campaigns.isArchived(campaign)}
    //         onClick={() =>
    //             campaigns.remove(
    //                 campaign.id,
    //                 () => liststate.setSelected(liststate.selected.filter(l => l.id !== campaign.id)),
    //                 !buildinfo.development
    //             )
    //         } >{t("common.buttons.remove")}
    //     </Button>

    // const clone = (campaign: Campaign) => <Button key={3} disabled={campaigns.isArchived(campaign)} 
    //      icn={icns.clone} onClick={() => {
    //         campaigns.setNext({ source: campaigns.clone(campaign), lineage: campaign, type: 'clone' })
    //         history.push(`${pathname}/new`)
    //     }} >{t("common.buttons.clone")}</Button>

    const removeAll = <Button type="danger" disabled={liststate.selected.length < 1}
        onClick={() => campaigns.removeAll(liststate.selected.map(l => l.id), () => liststate.setSelected([]))} >
        <span style={{ fontVariantCaps: "all-small-caps" }}>(DEV)</span> {t("common.labels.remove_all", { count: liststate.selected.length })}
    </Button>

    const rights = <Button enabled={liststate.selected.length > 0} icn={icns.permissions} style={{ marginTop: 30 }} onClick={showPermissions}>{t(iamPlural)}</Button>

    const add = <Button type="primary" icn={icns.add} enabled={logged.can(manage)} onClick={() => campaigns.setNext({ ...campaigns.next() })} linkTo={`${pathname}/new`} >{t("common.buttons.add_one", { singular })}</ Button>

    const canManage = (r: Campaign) => logged.can(specialise(campaigns.actions.manage, r.id))
    const readonly = ({ rowData }) => canManage(rowData) || icns.readonly(t("common.labels.readonly"))


    const unfilteredData = campaigns.allSorted()

    const { ContextSelector, contextFilteredData } = useContextFilter({
        data: unfilteredData,
        group: campaignGroup
    })

    const rowValidation = ({ rowData: campaign }) => {

       if (campaigns.isLoaded(campaign.id)) {
            const errors = validationErrors(campaign)
            return errors === 0 || icns.error(t("common.feedback.issues", { count: errors }))
        }

        //campaigns.fetchOne(campaign.id)

        return true

    }

    const data = contextFilteredData

    return (
        <Page>

            <Sidebar>
                {add}
                {rights}

                {buildinfo.development &&
                    <>
                        <br />
                        {removeAll}
                    </>
                }
            </Sidebar>

            <Topbar>

                <Titlebar title={plural} />

                {add}
                {rights}

            </Topbar>


            <VirtualTable data={data} rowKey="id" state={liststate} 
                filterGroup={campaignGroup} filterBy={campaigns.stringify} filterPlaceholder={t("common.components.table.filter_placeholder", { plural: plural.toLowerCase() })}
                filters={[ContextSelector]}
                sortBy={[['name', 'asc']]}
                //actions={c => [open(c), remove(c), clone(c)]}
                 >

                <Column  flexGrow={2} title={t("common.fields.name_multi.name")} decorations={[readonly, rowValidation]}
                    dataKey="name" dataGetter={(c: Campaign) => l(c.name)} cellRenderer={r => <CampaignLabel campaign={r.rowData} />} />

                <Column width={150} title={t("campaign.fields.status.name")} dataKey="status" dataGetter={(c: Campaign) => campaigns.status(c).label}

                    cellRenderer={r => campaigns.isArchived(r.rowData) ? <ArchiveLabel campaign={r.rowData} /> : campaigns.isSuspendSubmissions(r.rowData) ? <SuspendedLabel campaign={r.rowData} /> : <CampaignLabel mode='tag' statusOnly campaign={r.rowData} />} />

                <Column flexGrow={1} sortable={false} title={t("common.fields.lineage.name")} dataKey="lineage" dataGetter={(c: Campaign) => <CampaignLabel targetMode='lineage' campaign={c} />} />

                {tags.allTagsOf(campaignType).length > 0 &&
                    <Column sortable={false} flexGrow={1} title={t("common.fields.tags.name")} dataKey="tags" dataGetter={(c: Campaign) => <TagList taglist={c.tags} />} />
                }

            </VirtualTable>


            <Permissions resourceRange={liststate.selected} />

        </Page>
    )

}
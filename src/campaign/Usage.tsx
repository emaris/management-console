
import { useCampaigns } from 'emaris-frontend/campaign/api';
import { Campaign } from 'emaris-frontend/campaign/model';
import { createContext, PropsWithChildren, useContext, useMemo } from 'react';


type UsageData = {

    instances: Record<string, Campaign[]>

}

const CampaignUsageContext = createContext<UsageData>(undefined!)


export const CampaignUsage = (props: PropsWithChildren<{


}>) => {

    const { children } = props
    const campaigns = useCampaigns()

    const preloaded = campaigns.arePreloaded()
    const instances = campaigns.allInstances()


    const instanceUsage: UsageData['instances'] = useMemo(() => {

        if (!preloaded)
            return []

        const usage =  campaigns.all().filter(campaigns.isRunning).reduce((acc, campaign) => {

            const { parties, requirements, products, events } = instances[campaign.id] ?? {}  // may not have been  (fully)loaded yet (backend tells us is running)

            let usage =  [...parties?.all ?? [], ...requirements?.all ?? [], ...products?.all ?? [], ...events?.all ?? [] ].reduce((acc, instance) => ({

                ...acc,
                [instance.source]: [...acc[instance.source] ?? [], campaign]

            }), acc)

            usage = { ...usage ?? {}, ...[...events?.all ?? []].reduce((acc, instance) => ( instance.source ? {  // protect from dirty data without a source.

                ...acc,
                [instance.source]: acc[instance.source]?.includes(campaign) ? acc[instance.source] : [...acc[instance.source] ?? [], campaign]

            } : acc ), acc)}

            return usage


        }, {})

        return usage

    
        
    // eslint-disable-next-line
    }, [preloaded, instances])


    const usage: UsageData = useMemo(() => {


        const self =  {

            instances: instanceUsage

        }

        return self
    
    }, [instanceUsage])

    return <CampaignUsageContext.Provider value={usage}>
        {children}
    </CampaignUsageContext.Provider>

}

export type Usage = ReturnType<typeof useCampaignUsage>

export const useCampaignUsage = () => {


    const usage = useContext(CampaignUsageContext)
    const campaigns = useCampaigns()

    const self =  {

        ...usage

        ,

        // special default value further protects from corrupt usage map (undefined is indexed)
        usedBy: (target: string | { id: string } = '__missing') => usage.instances[ typeof target === 'string' ? target : target?.id] ?? []

        ,

        isInUse: (target: string | { id: string } ) => campaigns.liveGuard() && self.usedBy(target).length > 0 
    

    }

    return self
}
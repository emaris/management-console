
import { CampaignDetail, NewCampaignDetail } from "../campaign/Detail"
import { Placeholder } from "apprise-frontend/components/Placeholder"
import { campaignRoute } from "emaris-frontend/campaign/constants"
import { CampaignLoader } from "emaris-frontend/campaign/Loader"
import * as React from "react"
import { Route, Switch } from "react-router"
import { CampaignList } from "./List"


export const Campaigns =  () => 

            <CampaignLoader placeholder={Placeholder.page}>
                <Switch>
                    <Route exact path={campaignRoute} component={CampaignList} />
                    <Route path={`${campaignRoute}/new`} component={NewCampaignDetail} />
                    <Route path={`${campaignRoute}/:id/:type`} component={CampaignDetail} />
                    <Route path={`${campaignRoute}/:id`} component={CampaignDetail} />
                </Switch>
            </CampaignLoader>


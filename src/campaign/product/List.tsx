import { useListState } from "apprise-frontend/components/hooks";
import { Label } from "apprise-frontend/components/Label";
import { NoSuchRoute } from "apprise-frontend/components/NoSuchRoute";
import { Column, VirtualTable } from "apprise-frontend/components/VirtualTable";
import { icns } from "apprise-frontend/icons";
import { useLocale } from "apprise-frontend/model/hooks";
import { useTags } from "apprise-frontend/tag/api";
import { useTagFilter, useTagHolderFilter } from 'apprise-frontend/tag/filter';
import { TagList } from "apprise-frontend/tag/Label";
import { tagRefsIn } from "apprise-frontend/tag/model";
import { tenantPlural } from "apprise-frontend/tenant/constants";
import { userPlural } from 'apprise-frontend/user/constants';
import { useEventInstances } from 'emaris-frontend/campaign/event/api';
import { useCurrentCampaign } from "emaris-frontend/campaign/hooks";
import { useProductInstances } from "emaris-frontend/campaign/product/api";
import { ProductInstanceLabel } from "emaris-frontend/campaign/product/Label";
import { ProductInstance } from "emaris-frontend/campaign/product/model";
import { CampaignValidation } from "emaris-frontend/campaign/validation";
import { eventIcon, eventPlural } from 'emaris-frontend/event/constants';
import { useProducts } from "emaris-frontend/product/api";
import { productIcon, productPlural, productType } from "emaris-frontend/product/constants";
import { ProductLabel } from "emaris-frontend/product/Label";
import { Product } from "emaris-frontend/product/model";
import { Button, ButtonGroup } from 'apprise-frontend/components/Button';
import * as React from "react";
import { useTranslation } from "react-i18next";
import { useHistory } from "react-router-dom";
import { usePicker } from "../Picker";
import { ProductInstanceDetail } from "./Detail";


type Props = {

    report: ReturnType<CampaignValidation['validateProducts']>
}

export const ProductInstanceList = (props: Props) => {

    const campaign = useCurrentCampaign()

    const instances = useProductInstances(campaign)

    const instanceId = instances.detailInRoute()

    const detail = instances.lookup(instanceId)

    return instanceId && !detail ? <NoSuchRoute /> : <InnerList {...props} detail={detail!} />
}


const InnerList = (props: Props & { detail: ProductInstance }) => {


    const { t } = useTranslation()
    const { l } = useLocale()

    const history = useHistory()

    const tags = useTags()
    const products = useProducts()

    const campaign = useCurrentCampaign()
    const eventInstances = useEventInstances(campaign)

    const readOnly = campaign.guarded

    const { detail, report } = props

    const { Picker, pickerVisible, setPickerVisible } = usePicker()

    const instances = useProductInstances(campaign)

    const liststate = useListState<ProductInstance>()

    const { selected, resetSelected, removeOne } = liststate

    const plural = t(productPlural)

    const editBtn = (p: ProductInstance) =>
        <Button
            key="edit"
            onClick={() => history.push(instances.route(p))} >
            {t("common.buttons.open")}
        </Button>

    const removeBtn = (p: ProductInstance) =>
        <Button
            key="remove"
            disabled={readOnly}
            onClick={() => instances.remove(p, removeOne)} >
            {t("common.buttons.remove")}
        </Button>

    const addBtn = <Button
        icn={icns.add}
        type="primary"
        disabled={readOnly}
        onClick={() => setPickerVisible(true)}>
        {t("common.buttons.add_many", { plural })}
    </Button>

    const removeSelectedBtn =
        <Button
            type="danger"
            enabled={selected.length >= 1}
            disabled={readOnly}
            onClick={() => instances.removeMany(selected, resetSelected)}>
            {t("common.buttons.remove_many", { count: selected.length })}
        </Button>

    const unfilteredData = instances.all()

    // eslint-disable-next-line
    const sortedData = React.useMemo(() => instances.allSorted(), [unfilteredData])

    const productinstanceGroup = `${campaign.id}-${productType}`


    const { TagFilter, tagFilteredData } = useTagFilter({
        filtered: sortedData,
        group: productinstanceGroup,
        key: `tags`
    })

    const { TagFilter: AudienceFilter, tagFilteredData: audienceFilteredData } = useTagHolderFilter({
        filtered: tagFilteredData,
        tagged: unfilteredData,
        tagsOf: t => products.safeLookup(t.source).audience.terms.flatMap(t => t.tags),
        placeholder: t("common.fields.audience.name"),
        group: productinstanceGroup,
        key: `audience`
    })

    const { TagFilter: UserProfileFilter, tagFilteredData: userProfileFilteredData } = useTagHolderFilter({
        filtered: audienceFilteredData,
        tagged: unfilteredData,
        tagsOf: t => t.userProfile?.terms.flatMap(t => t.tags),
        placeholder: t("common.fields.user_profile.name"),
        group: productinstanceGroup,
        key: `profile`
    })

    const data = userProfileFilteredData

    const rowValidation = ({ rowData: product }) => report[product.id].status === "error" && icns.error(report[product.id].msg)


    const unfilteredTemplates = products.all()

    // eslint-disable-next-line
    const activeTemplates = React.useMemo(() => products.allSorted().filter(r => r.lifecycle.state !== 'inactive'), [unfilteredTemplates])


    const eventCountLabel = (ri: ProductInstance) => {

        const thisEventInstances = eventInstances.allAboutWithTemporals(ri.source)
        const someMissing = thisEventInstances.some(ei => !ei.date)

        return <Label
            icon={eventIcon}
            title={thisEventInstances.length}
            className={someMissing ? 'event-warn' : ''}
            tipPlacement='topRight'
            tip={() => <span>{t("event.no_date_set")}</span>}
            noTip={!someMissing} />
    }

    return (<>
        <VirtualTable data={data} total={sortedData.length} state={liststate} selectAll
            filterGroup={productinstanceGroup}
            filters={[UserProfileFilter, AudienceFilter, TagFilter]}

            filterBy={instances.stringify} filterPlaceholder={t("common.components.table.filter_placeholder", { plural: plural.toLowerCase() })}
            decorations={[<ButtonGroup>{addBtn}{removeSelectedBtn}</ButtonGroup>]}
            onDoubleClick={p => history.push(instances.route(p))}
            refreshFlag={selected}
            actions={p => [editBtn(p), removeBtn(p)]}>

            <Column<ProductInstance> flexGrow={1} title={t("common.fields.name_multi.name")} decorations={[rowValidation]}
                dataKey="name" dataGetter={p => <ProductInstanceLabel tipTitle noLineage noMemo link instance={p} />} />


            <Column<ProductInstance> sortable={false} title={t("common.fields.audience.name")} dataKey="audience" dataGetter={(p: ProductInstance) => {
                const audienceTerms = (p.audience?.terms.length ?? 0) === 0
                const audienceListIncludes = (p.audienceList?.includes.length ?? 0) === 0
                const audienceListExcludes = (p.audienceList?.excludes.length ?? 0) === 0
                return (audienceTerms && audienceListIncludes && audienceListExcludes) ? <Label title={t(tenantPlural)} /> : <Label title={t("common.labels.restricted")} />

            }
            } />

            <Column<ProductInstance> sortable={false} title={t("common.fields.user_profile.name")} dataKey="userprofile" dataGetter={(p: ProductInstance) =>

                (p.userProfile?.terms.length ?? 0) > 0 ? <TagList taglist={tagRefsIn(p.userProfile)} /> : <Label noIcon title={t(userPlural)} />
            } />

            {tags.allTagsOf(productType).length > 0 &&
                <Column<ProductInstance> sortable={false} flexGrow={2} title={t("common.fields.tags.name")} dataKey="t.tags" dataGetter={(pi: ProductInstance) => <TagList truncateEllipsisLink={instances.route(pi)} taglist={pi.tags} />} />
            }

            <Column<ProductInstance> ìwidth={60} sortable={false} title={t(eventPlural)} dataKey="source"
                dataGetter={i => i.source} cellRenderer={({ rowData: ri }) => eventCountLabel(ri)}
            />

        </VirtualTable>


        {pickerVisible &&

            <Picker id='prodinst' title={plural} icon={productIcon}
                filterGroup={`$requirementinstanceGroup}-picker`}
                resources={activeTemplates}
                selected={sortedData.map(i => i.source)}
                onSelection={added => Promise.resolve(instances.addAll(added.map(instances.generate as any)))}>

                <Column flexGrow={1} title={t("common.fields.name_multi.name")} dataKey="t.name.en" dataGetter={(t: Product) => l(t.name)} cellRenderer={cell => <ProductLabel noLink tipTitle product={cell.rowData} />} />
                <Column flexGrow={2} title={t("common.fields.tags.name")} dataKey="t.tags" dataGetter={(p: Product) => <TagList truncateEllipsisLink={products.routeTo(p)} taglist={p.tags} />} />

            </Picker>

        }

        {detail &&

            <ProductInstanceDetail detail={detail} onClose={() => history.push(instances.route())} />

        }


    </>
    )
}

import { Button } from "apprise-frontend/components/Button"
import { Drawer } from "apprise-frontend/components/Drawer"
import { useConfig } from "apprise-frontend/config/state"
import { Form } from "apprise-frontend/form/Form"
import { FormState, useFormState } from "apprise-frontend/form/hooks"
import { NoteBox } from "apprise-frontend/form/NoteBox"
import { SimpleBox } from "apprise-frontend/form/SimpleBox"
import { Switch } from "apprise-frontend/form/Switch"
import { icns } from "apprise-frontend/icons"
import { LayoutProvider } from 'apprise-frontend/layout/LayoutProvider'
import { instanceFrom } from 'apprise-frontend/layout/model'
import { paramIcon } from 'apprise-frontend/layout/parameters/constants'
import { useLocale } from "apprise-frontend/model/hooks"
import { Tab } from "apprise-frontend/scaffold/Tab"
import { Topbar } from "apprise-frontend/scaffold/Topbar"
import { BytestreamedContext } from 'apprise-frontend/stream/BytestreamedHelper'
import { TagBoxset } from "apprise-frontend/tag/TagBoxset"
import { TagRefBox } from "apprise-frontend/tag/TagRefBox"
import { AudienceList } from "apprise-frontend/tenant/AudienceList"
import { tenantIcon, tenantType } from "apprise-frontend/tenant/constants"
import { useUsers } from "apprise-frontend/user/api"
import { userType } from 'apprise-frontend/user/constants'
import { through } from "apprise-frontend/utils/common"
import { paramsInQuery } from "apprise-frontend/utils/routes"
import { useEventInstances } from "emaris-frontend/campaign/event/api"
import { EventInstance } from "emaris-frontend/campaign/event/model"
import { useProductInstances } from "emaris-frontend/campaign/product/api"
import { ProductInstanceLabel } from "emaris-frontend/campaign/product/Label"
import { ProductInstance } from "emaris-frontend/campaign/product/model"
import { submissionType } from "emaris-frontend/campaign/submission/constants"
import { useDashboards } from "emaris-frontend/dashboard/api"
import { predefinedProductParameterIds } from 'emaris-frontend/layout/parameters/constants'
import { useProducts } from "emaris-frontend/product/api"
import { productType } from "emaris-frontend/product/constants"
import { ProductDetailLoader } from 'emaris-frontend/product/DetailLoader'
import { ProductLabel } from "emaris-frontend/product/Label"
import { requirementIcon, requirementPlural } from "emaris-frontend/requirement/constants"
import * as React from "react"
import { useTranslation } from "react-i18next"
import { useHistory, useLocation } from "react-router-dom"
import { useCurrentCampaign } from "emaris-frontend/campaign/hooks"
import { ProductRequirementList } from "#product/RequirementList"
import { AssetOverlayForm } from "#campaign/AssetOverlayForm"
import { EventList } from "#campaign/event/EventList"




type Props = {

    detail: ProductInstance
    onClose: () => void

    width?: number
}

export const ProductInstanceDetail = (props: Props) => {

    const { detail } = props

    return <ProductDetailLoader id={detail.source}>{

        () => <InnerProductInstanceDetail {...props} />

    }</ProductDetailLoader>
}

const InnerProductInstanceDetail = (props: Props) => {

    const { t } = useTranslation()
    const { l } = useLocale()
    const history = useHistory()
    const { search } = useLocation()

    const sources = useProducts()
    const config = useConfig()
    const dashboard = useDashboards()

    const campaign = useCurrentCampaign()

    const { detail, width = 650, onClose } = props

    const products = useProductInstances(campaign)
    const events = useEventInstances(campaign)

    // existing events about this detail, including temporals.
    // eslint-disable-next-line
    const initialRelatedEvents = events.allAboutWithTemporals(detail.source)

    type FormData = { instance: ProductInstance, relatedEvents: EventInstance[] }

    const formstate = useFormState<FormData>({ instance: detail, relatedEvents: initialRelatedEvents })

    const source = sources.safeLookup(detail.source)

    const { edited, initial, dirty, change, reset, softReset } = formstate

    // resyncs on external changes (push events)
    // eslint-disable-next-line
    React.useEffect(() => softReset({ instance, relatedEvents: initialRelatedEvents }), [events.all()])


    const { instance, relatedEvents } = formstate.edited

    const liveBtn = <Button enabledOnReadOnly iconLeft icn={icns.edit} onClick={() => history.push(dashboard.given(campaign).routeToAsset(edited.instance))}>{t("campaign.labels.campaign_view.live")}</Button>


    const onSave = () => events.replaceAllAbout(relatedEvents, detail.source)
        .then(through(_ => products.save(instance, initial.instance)))
        .then(relatedEvents => softReset({ ...edited, relatedEvents }))

    const revert = <Button icn={icns.revert} enabled={dirty} onClick={() => reset()}>{t("common.buttons.revert")}</Button>
    const saveact = <Button type="primary" icn={icns.save} enabled={dirty} onClick={onSave}>{t("common.buttons.save")}</Button>

    const pluralRequirements = t(requirementPlural)


    const { tab } = paramsInQuery(search)

    const profileReport = products.validateInstanceProfile(instance)
    const requirementReport = { errors: () => 0 }

    const report = { ...profileReport, ...requirementReport }

    const tabcompo = (() => {

        switch (tab) {

            case pluralRequirements: return <ProductRequirementList product={source} report={report} />

            case 'overlay': return (
                <BytestreamedContext>
                    <LayoutProvider for={edited.instance.id} layoutConfig={{ type: instance.instanceType }} layout={instanceFrom(source.properties.layout)} onChange={() => {/* no real change intended here, only overlays. */ }}>
                        <AssetOverlayForm
                            instance={edited.instance}
                            excludes={p => predefinedProductParameterIds.includes(p.id)}
                            onChange={instance => change((t, v) => t.instance = v)(instance)} />
                    </LayoutProvider>
                </BytestreamedContext>

            )



            default: return <GeneralForm report={report} {...formstate} />
        }
    })()

    return <Drawer readonly={!edited.instance.source} renderAfterTransition routeId={products.detailParam()} visible width={width} warnOnClose={dirty} title={t("common.buttons.edit_one", { singular: l(source?.name!) })} icon={tenantIcon} onClose={onClose}>

        <div style={{ height: "100%", display: "flex", flexDirection: "column" }} >

            <Topbar offset={63}>

                <Tab default id={"info"} icon={icns.form} name={t("common.labels.general")} badge={profileReport.errors() > 0} />
                <Tab id={'overlay'} icon={paramIcon} name={t('layout.parameters.plural')} />
                <Tab id={pluralRequirements} icon={requirementIcon} name={pluralRequirements} badge={requirementReport.errors() > 0} />

                {saveact}
                {revert}
                {config.get().routedTypes?.includes(submissionType) && liveBtn}

            </Topbar>

            {/* tabcompo can stretch 100% if it wants to (e.g. a table) */}
            <div style={{ flexGrow: 1 }}>

                {tabcompo}

            </div>

        </div>


    </Drawer>
}


type FormProps = FormState<{ instance: ProductInstance, relatedEvents: EventInstance[] }> & {

    report: any
}


const GeneralForm = (props: FormProps) => {

    const { edited, change, report, dirty } = props

    const { instance, relatedEvents } = edited

    const { t } = useTranslation()

    const { logged } = useUsers();

    const campaign = useCurrentCampaign()

    const events = useEventInstances(campaign)
    const instances = useProductInstances(campaign)

    const onScreenEvents = dirty ? relatedEvents : [...relatedEvents].sort(events.temporalAwareDateComparatorOver(relatedEvents))

    const errors = React.useMemo(() => instances.validateAssetDependencies(edited.instance)

        //eslint-disable-next-line
        , [edited.instance])

    return <Form state={props} >

        {Object.values(errors).length > 0 &&

            <div className='missing-dependencies-list'>
                <div className='list-title'>{icns.error()}&nbsp;{t('campaign.feedback.missing_dependencies_errors')}</div>
                <ul>
                    {Object.values(errors).map((error, id) =>

                        <li key={id}>{error.msg}</li>

                    )}
                </ul>
            </div>

        }

        <EventList instance={instance} events={onScreenEvents} onChange={change((e, newevents) => { e.relatedEvents = newevents })} />

        <TagRefBox mode="multi" label={t("common.fields.audience.name")} validation={report.audience} expression={edited.instance.audience} type={tenantType} onChange={change((t, v) => t.instance.audience = v)} />

        <AudienceList label={t("common.fields.audience_list.name")} validation={report.audienceList} onChange={change((t, v) => t.instance.audienceList = v)}>{edited.instance.audienceList}</AudienceList>

        <TagRefBox mode="multi" label={t("common.fields.user_profile.name")} validation={report.userProfile} expression={edited.instance.userProfile} type={userType} onChange={change((t, v) => t.instance.userProfile = v)} />


        <TagBoxset edited={instance.tags} type={productType} validation={report} onChange={change((t, v) => t.instance.tags = v)} />

        <NoteBox enabledOnReadOnly label={t("common.fields.note.name")} validation={report.note} autoSize={{ minRows: 4, maxRows: 6 }} onChange={change((t, v) => t.instance.properties.note = t.instance.properties.note ? { ...t.instance.properties.note, [logged.tenant]: v } : { [logged.tenant]: v })} >
            {instance.properties.note}
        </NoteBox>

        <SimpleBox label={t("campaign.fields.source.name")} validation={report.source} >
            <ProductLabel product={instance.source} />
        </SimpleBox>

        {instance.lineage &&

            <SimpleBox label={t("common.fields.lineage.name")} validation={report.lineage} >
                <ProductInstanceLabel showCampaign instance={instance.lineage} />
            </SimpleBox>

        }

        <Switch label={t("common.fields.editable.name")} onChange={change((u, v) => u.instance.properties.editable = v)} validation={report.editable}>
            {edited.instance.properties.editable !== undefined ? edited.instance.properties.editable : true}
        </Switch>

        <Switch label={t("common.fields.versionable.name")} onChange={change((u, v) => u.instance.properties.versionable = v)} validation={report.versionable}>
            {edited.instance.properties.versionable !== undefined ? edited.instance.properties.versionable : true}
        </Switch>

        {
            campaign.properties.complianceScale &&
            <Switch label={t("common.fields.assessed.name")} onChange={change((u, v) => u.instance.properties.assessed = v)} validation={report.assessed}>
                {edited.instance.properties.assessed !== undefined ? edited.instance.properties.assessed : true}
            </Switch>
        }

    </Form>
}

import { Placeholder } from "apprise-frontend/components/Placeholder"
import { useAsyncRender } from "apprise-frontend/utils/hooks"
import { useProductInstances } from "emaris-frontend/campaign/product/api"
import * as React from "react"
import { useCurrentCampaign } from "../../lib/emaris-frontend/campaign/hooks"


type Props = React.PropsWithChildren<{}>


export const ProductInstanceLoader = (props:Props) => {

    const campaign = useCurrentCampaign()

    const {fetchAll,areReady} = useProductInstances(campaign)

    const [render] = useAsyncRender({when:areReady(),task:fetchAll,content:props.children,placeholder:Placeholder.list})

    return render
  
  }
import { NoSuchRoute } from "apprise-frontend/components/NoSuchRoute";
import { campaignRoute } from "emaris-frontend/campaign/constants";
import { CampaignValidation } from "emaris-frontend/campaign/validation";
import { productType } from "emaris-frontend/product/constants";
import * as React from "react";
import { Route, Switch } from "react-router-dom";
import { ProductInstanceList } from "./List";


type Props = {

    report: ReturnType<CampaignValidation['validateProducts']>
}

export const ProductInstances = (props:Props)=> {

    return   <Switch>
                <Route exact path={`${campaignRoute}/:name/${productType}`} render={()=><ProductInstanceList {...props} />} />
                <NoSuchRoute /> 
            </Switch>
                

}
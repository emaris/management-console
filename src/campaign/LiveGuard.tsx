import { Icon } from 'antd'
import { Button } from 'apprise-frontend/components/Button'
import { useRoutableDrawer } from 'apprise-frontend/components/Drawer'
import { Paragraph, Text } from 'apprise-frontend/components/Typography'
import { useCampaigns } from 'emaris-frontend/campaign/api'
import { CampaignLabel } from 'emaris-frontend/campaign/Label'
import { Campaign } from 'emaris-frontend/campaign/model'
import { useDashboards } from 'emaris-frontend/dashboard/api'
import React, { useState } from 'react'
import { useTranslation } from 'react-i18next'
import { AiFillLock, AiFillUnlock } from 'react-icons/ai'
import { FaExpeditedssl } from 'react-icons/fa'
import { useHistory } from 'react-router-dom'
import { useCampaignUsage } from './Usage'


export const useLiveAssetGuard = (props: {

    id: string
    disabled: boolean
    singular: string
    link: (_: Campaign) => string

}) => {

    const { id, link, singular: s, disabled } = props

    const singular = s.toLowerCase()

    const { t } = useTranslation()

    const history = useHistory()

    const usage = useCampaignUsage()

    
    const [liveGuarded, liveGuardedSet] = useState(true)
    const { Drawer, open, close } = useRoutableDrawer({ id: 'liveguard' })

    const clickGuard = () => liveGuarded ? open() : liveGuardedSet(true)

    const unlock = () => {

        liveGuardedSet(false)
        close()
    }

    const status = liveGuarded ? 'guarded' : 'unguarded'

    const icon = liveGuarded ? <Icon component={AiFillLock} /> : <Icon component={AiFillUnlock} />

    const inUse = usage.isInUse(id)

    const liveGuard = inUse && <div className={`liveguard liveguard-${status} ${disabled ? `liveguard-disabled` : ''}`} >

        <Button disabled={disabled} tooltip={t(`campaign.liveguard.tip_${status}`)} className='liveguard-label' icn={icon} enabledOnReadOnly noborder type='ghost' onClick={clickGuard}>
            {t('campaign.liveguard.label')}
        </Button>

        <Drawer width={500} className='liveguard-drawer' onClose={close}
            icon={<Icon style={{ color: "red" }} component={AiFillLock} />} title={t("campaign.liveguard.title")}>

            <div className="liveguard-dialog-panel">

                <Paragraph style={{ fontSize: 16 }} >{t("campaign.liveguard.warning1", { singular })}</Paragraph>

                <FaExpeditedssl className='liveguard-dialog-icon' />

                <Paragraph style={{ marginTop: 30 }}><Text italics>{t("campaign.liveguard.usage1", { singular })}</Text></Paragraph>

                <Paragraph style={{ marginTop: 20 }}><Text italics>{t("campaign.liveguard.usage2", { singular })}</Text></Paragraph>

                <Paragraph style={{ marginTop: 20 }}><Text italics>{t("campaign.liveguard.warning2", { singular })}</Text></Paragraph>

                <Button enabledOnReadOnly style={{ marginTop: 30 }} type="danger" onClick={unlock}>{t("campaign.liveguard.btn", { singular })}</Button>

                <div className="liveguard-dialog-section">

                    <div className="liveguard-dialog-section-title">
                        {t('campaign.liveguard.campaigns')}
                    </div>

                    <Paragraph style={{ fontSize: 16 }}><Text italics>{t("campaign.liveguard.campaigns_explainer", { singular })}</Text></Paragraph>

                    <div className='liveguard-campaigns'>
                        {
                            usage.usedBy(id).map(c =>

                                <div key={c.id} className='liveguard-campaign' >
                                    <CampaignLabel campaign={c} noDecorations noOptions noLink />
                                    <Button enabledOnReadOnly size='small' type='primary' className='liveguardcampaign-open' onClick={() => history.push(link(c))}>
                                        {t('campaign.liveguard.campaign_open',{singular})}
                                    </Button>
                                </div>
                            )
                        }
                    </div>
                </div>

            </div>

        </Drawer>

    </div>





    return { liveGuarded: inUse && liveGuarded, liveGuard, usage }
}

export const useCampaignLiveGuard = (props: {

    campaign: Campaign

}) => {

    const { campaign } = props

    const { t } = useTranslation()

    const singular = t('campaign.module.name_singular').toLowerCase()

    const history = useHistory()

    const campaigns = useCampaigns()

    const dashboards = useDashboards()

    const inUse = campaigns.liveGuard() && campaigns.isRunning(campaign)

    const [liveGuarded, liveGuardedSet] = useState(true)
    const { Drawer, open, close } = useRoutableDrawer({ id: 'liveguard' })

    const clickGuard = () => liveGuarded ? open() : liveGuardedSet(true)

    const unlock = () => {

        liveGuardedSet(false)
        close()
    }

    const status = liveGuarded ? 'guarded' : 'unguarded'

    const icon = liveGuarded ? <Icon component={AiFillLock} /> : <Icon component={AiFillUnlock} />

    const liveGuard = inUse && <div className={`liveguard liveguard-${status}`} >

        <Button tooltip={t(`campaign.liveguard.campaign_tip_${status}`)} className='liveguard-label' icn={icon} enabledOnReadOnly noborder type='ghost' onClick={clickGuard}>
            {t('campaign.liveguard.label')}
        </Button>

        <Drawer width={500} className='liveguard-drawer' onClose={close}
            icon={<Icon style={{ color: "red" }} component={AiFillLock} />} title={t("campaign.liveguard.title")}>

            <div className="liveguard-dialog-panel">

                <Paragraph style={{ fontSize: 16 }} >{t("campaign.liveguard.campaign_warning", { singular })}</Paragraph>

                <FaExpeditedssl className='liveguard-dialog-icon' />

                <Paragraph style={{ marginTop: 30 }}><Text italics>{t("campaign.liveguard.campaign_usage", { singular })}</Text></Paragraph>

                <div className='liveguard-campaign-btns'>
                    <Button enabledOnReadOnly type="danger" onClick={unlock}>{t("campaign.liveguard.btn", { singular })}</Button>
                    <Button enabledOnReadOnly type='primary' className='liveguardcampaign-open' onClick={() => history.push(dashboards.given(campaign).routeToCampaign())}>
                                        {t('campaign.liveguard.campaign_open', {singular})}
                    </Button>
                </div>

            </div>

        </Drawer>

    </div>


    return { liveGuarded: inUse && liveGuarded, liveGuard }
}


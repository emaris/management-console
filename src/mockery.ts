
import { mockPermissions } from 'apprise-frontend/utils/mocks/iam';
import { mockLoggedUser } from 'apprise-frontend/utils/mocks/logged';
import { mockRegistry } from "apprise-frontend/utils/mocks/mocks";
import { mockSettings } from 'apprise-frontend/utils/mocks/settings';
import { mockTags } from 'apprise-frontend/utils/mocks/tags';
import { mockTenants } from 'apprise-frontend/utils/mocks/tenants';
import { mockThemes } from 'apprise-frontend/utils/mocks/themes';
import { mockUsers } from 'apprise-frontend/utils/mocks/users';
import { mockMessages } from "apprise-messages/mocks";
import MockAdapter from "axios-mock-adapter";
import { mockCampaigns } from 'emaris-frontend/utils/mocks/campaigns';
import { mockEvents } from 'emaris-frontend/utils/mocks/events';
import { mockEventInstances, mockInstances, mockPartyInstances, mockProductInstances, mockRequirementInstances } from 'emaris-frontend/utils/mocks/instances';
import { mockInfo } from 'emaris-frontend/utils/mocks/mocks';
import { mockProducts } from 'emaris-frontend/utils/mocks/products';
import { mockRequirements } from 'emaris-frontend/utils/mocks/requirements';
import { mockSubmissions } from 'emaris-frontend/utils/mocks/submissions';



export const mockery = (mock: MockAdapter) => {

  // mockConfig(mock, {

  //   routedTypes: [requirementType, campaignType, productType, eventType, submissionType],
  //   mode: "dev",
  //   name: "mc",
  //   services: {
  //     admin: { "prefix": "admin", label: "Admin Backend" },
  //     domain: { "prefix": "domain", label: "Domain Backend", default: true }
  //   },
  //   intl: {
  //     languages: ["en", "fr"],
  //     required: ["en", "fr"]
  //   },
  //   fileLimitSize: 10

  // })
  
  //mockEverything(mock)

  mock.onAny().passThrough()

}


//eslint-disable-next-line
const mockEverything = (mock:MockAdapter) => {


  mockInfo(mock)

  mockSettings(mock)

  mockLoggedUser(mock)

  mockTags(mock)

  mockTenants(mock)

  mockUsers(mock)

  mockPermissions(mock)
  
  mockRequirements(mock)

  mockProducts(mock)

  mockThemes(mock)

  mockEvents(mock)

  mockCampaigns(mock)

  mockInstances(mock)

  mockRegistry.register("instances")

  mockPartyInstances(mock)

  mockRequirementInstances(mock)
  mockProductInstances(mock)

  mockEventInstances(mock)

  mockSubmissions(mock)

  mockMessages (mock)


  mock.onAny().passThrough()
}